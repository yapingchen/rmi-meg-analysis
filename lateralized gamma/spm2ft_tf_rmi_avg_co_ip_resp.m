%%% gp_shr_co
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf\']);
    eval(['load tf_s' aa '_gp_shr_co_comb.mat']);
end
cd F:\2018_RMI_gamma\coip;
cfg = [];
cfg.keepindividual = 'yes' ;
tf_gp_shr_co_comb_avg = ft_freqgrandaverage(cfg, tf_s02_gp_shr_co_comb, tf_s03_gp_shr_co_comb, tf_s04_gp_shr_co_comb, tf_s05_gp_shr_co_comb, tf_s06_gp_shr_co_comb, tf_s08_gp_shr_co_comb, tf_s09_gp_shr_co_comb, tf_s10_gp_shr_co_comb, tf_s11_gp_shr_co_comb, tf_s12_gp_shr_co_comb, tf_s13_gp_shr_co_comb, tf_s14_gp_shr_co_comb, tf_s15_gp_shr_co_comb, tf_s16_gp_shr_co_comb, tf_s17_gp_shr_co_comb, tf_s18_gp_shr_co_comb, tf_s19_gp_shr_co_comb, tf_s20_gp_shr_co_comb)
save tf_gp_shr_co_comb_avg tf_gp_shr_co_comb_avg


%%
%%% gp_shr_ip
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf\']);
    eval(['load tf_s' aa '_gp_shr_ip_comb.mat']);
end
cd F:\2018_RMI_gamma\coip;
cfg = [];
cfg.keepindividual = 'yes' ;
tf_gp_shr_ip_comb_avg = ft_freqgrandaverage(cfg, tf_s02_gp_shr_ip_comb, tf_s03_gp_shr_ip_comb, tf_s04_gp_shr_ip_comb, tf_s05_gp_shr_ip_comb, tf_s06_gp_shr_ip_comb, tf_s08_gp_shr_ip_comb, tf_s09_gp_shr_ip_comb, tf_s10_gp_shr_ip_comb, tf_s11_gp_shr_ip_comb, tf_s12_gp_shr_ip_comb, tf_s13_gp_shr_ip_comb, tf_s14_gp_shr_ip_comb, tf_s15_gp_shr_ip_comb, tf_s16_gp_shr_ip_comb, tf_s17_gp_shr_ip_comb, tf_s18_gp_shr_ip_comb, tf_s19_gp_shr_ip_comb, tf_s20_gp_shr_ip_comb)
save tf_gp_shr_ip_comb_avg tf_gp_shr_ip_comb_avg

% %%
%%% gp_unshr_co
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf\']);
    eval(['load tf_s' aa '_gp_unshr_co_comb.mat']);
end
cd F:\2018_RMI_gamma\coip;
cfg = [];
cfg.keepindividual = 'yes' ;
tf_gp_unshr_co_comb_avg = ft_freqgrandaverage(cfg, tf_s02_gp_unshr_co_comb, tf_s03_gp_unshr_co_comb, tf_s04_gp_unshr_co_comb, tf_s05_gp_unshr_co_comb, tf_s06_gp_unshr_co_comb, tf_s08_gp_unshr_co_comb, tf_s09_gp_unshr_co_comb, tf_s10_gp_unshr_co_comb, tf_s11_gp_unshr_co_comb, tf_s12_gp_unshr_co_comb, tf_s13_gp_unshr_co_comb, tf_s14_gp_unshr_co_comb, tf_s15_gp_unshr_co_comb, tf_s16_gp_unshr_co_comb, tf_s17_gp_unshr_co_comb, tf_s18_gp_unshr_co_comb, tf_s19_gp_unshr_co_comb, tf_s20_gp_unshr_co_comb)
save tf_gp_unshr_co_comb_avg tf_gp_unshr_co_comb_avg


% %%
%%% gp_unshr_ip
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf\']);
    eval(['load tf_s' aa '_gp_unshr_ip_comb.mat']);
end
cd F:\2018_RMI_gamma\coip;
cfg = [];
cfg.keepindividual = 'yes' ;
tf_gp_unshr_ip_comb_avg = ft_freqgrandaverage(cfg, tf_s02_gp_unshr_ip_comb, tf_s03_gp_unshr_ip_comb, tf_s04_gp_unshr_ip_comb, tf_s05_gp_unshr_ip_comb, tf_s06_gp_unshr_ip_comb, tf_s08_gp_unshr_ip_comb, tf_s09_gp_unshr_ip_comb, tf_s10_gp_unshr_ip_comb, tf_s11_gp_unshr_ip_comb, tf_s12_gp_unshr_ip_comb, tf_s13_gp_unshr_ip_comb, tf_s14_gp_unshr_ip_comb, tf_s15_gp_unshr_ip_comb, tf_s16_gp_unshr_ip_comb, tf_s17_gp_unshr_ip_comb, tf_s18_gp_unshr_ip_comb, tf_s19_gp_unshr_ip_comb, tf_s20_gp_unshr_ip_comb)
save tf_gp_unshr_ip_comb_avg tf_gp_unshr_ip_comb_avg

% %%
%%% fr_shr_co
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf\']);
    eval(['load tf_s' aa '_fr_shr_co_comb.mat']);
end
cd F:\2018_RMI_gamma\coip;
cfg = [];
cfg.keepindividual = 'yes' ;
tf_fr_shr_co_comb_avg = ft_freqgrandaverage(cfg, tf_s02_fr_shr_co_comb, tf_s03_fr_shr_co_comb, tf_s04_fr_shr_co_comb, tf_s05_fr_shr_co_comb, tf_s06_fr_shr_co_comb, tf_s08_fr_shr_co_comb, tf_s09_fr_shr_co_comb, tf_s10_fr_shr_co_comb, tf_s11_fr_shr_co_comb, tf_s12_fr_shr_co_comb, tf_s13_fr_shr_co_comb, tf_s14_fr_shr_co_comb, tf_s15_fr_shr_co_comb, tf_s16_fr_shr_co_comb, tf_s17_fr_shr_co_comb, tf_s18_fr_shr_co_comb, tf_s19_fr_shr_co_comb, tf_s20_fr_shr_co_comb)
save tf_fr_shr_co_comb_avg tf_fr_shr_co_comb_avg

% %%
%%% fr_shr_ip
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf\']);
    eval(['load tf_s' aa '_fr_shr_ip_comb.mat']);
end
cd F:\2018_RMI_gamma\coip;
cfg = [];
cfg.keepindividual = 'yes' ;
tf_fr_shr_ip_comb_avg = ft_freqgrandaverage(cfg, tf_s02_fr_shr_ip_comb, tf_s03_fr_shr_ip_comb, tf_s04_fr_shr_ip_comb, tf_s05_fr_shr_ip_comb, tf_s06_fr_shr_ip_comb, tf_s08_fr_shr_ip_comb, tf_s09_fr_shr_ip_comb, tf_s10_fr_shr_ip_comb, tf_s11_fr_shr_ip_comb, tf_s12_fr_shr_ip_comb, tf_s13_fr_shr_ip_comb, tf_s14_fr_shr_ip_comb, tf_s15_fr_shr_ip_comb, tf_s16_fr_shr_ip_comb, tf_s17_fr_shr_ip_comb, tf_s18_fr_shr_ip_comb, tf_s19_fr_shr_ip_comb, tf_s20_fr_shr_ip_comb)
save tf_fr_shr_ip_comb_avg tf_fr_shr_ip_comb_avg

% %%
%%% fr_unshr_co
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf\']);
    eval(['load tf_s' aa '_fr_unshr_co_comb.mat']);
end
cd F:\2018_RMI_gamma\coip;
cfg = [];
cfg.keepindividual = 'yes' ;
tf_fr_unshr_co_comb_avg = ft_freqgrandaverage(cfg, tf_s02_fr_unshr_co_comb, tf_s03_fr_unshr_co_comb, tf_s04_fr_unshr_co_comb, tf_s05_fr_unshr_co_comb, tf_s06_fr_unshr_co_comb, tf_s08_fr_unshr_co_comb, tf_s09_fr_unshr_co_comb, tf_s10_fr_unshr_co_comb, tf_s11_fr_unshr_co_comb, tf_s12_fr_unshr_co_comb, tf_s13_fr_unshr_co_comb, tf_s14_fr_unshr_co_comb, tf_s15_fr_unshr_co_comb, tf_s16_fr_unshr_co_comb, tf_s17_fr_unshr_co_comb, tf_s18_fr_unshr_co_comb, tf_s19_fr_unshr_co_comb, tf_s20_fr_unshr_co_comb)
save tf_fr_unshr_co_comb_avg tf_fr_unshr_co_comb_avg

% %%
%%% fr_unshr_ip
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf\']);
    eval(['load tf_s' aa '_fr_unshr_ip_comb.mat']);
end
cd F:\2018_RMI_gamma\coip;
cfg = [];
cfg.keepindividual = 'yes' ;
tf_fr_unshr_ip_comb_avg = ft_freqgrandaverage(cfg, tf_s02_fr_unshr_ip_comb, tf_s03_fr_unshr_ip_comb, tf_s04_fr_unshr_ip_comb, tf_s05_fr_unshr_ip_comb, tf_s06_fr_unshr_ip_comb, tf_s08_fr_unshr_ip_comb, tf_s09_fr_unshr_ip_comb, tf_s10_fr_unshr_ip_comb, tf_s11_fr_unshr_ip_comb, tf_s12_fr_unshr_ip_comb, tf_s13_fr_unshr_ip_comb, tf_s14_fr_unshr_ip_comb, tf_s15_fr_unshr_ip_comb, tf_s16_fr_unshr_ip_comb, tf_s17_fr_unshr_ip_comb, tf_s18_fr_unshr_ip_comb, tf_s19_fr_unshr_ip_comb, tf_s20_fr_unshr_ip_comb)
save tf_fr_unshr_ip_comb_avg tf_fr_unshr_ip_comb_avg

% %%
%%% sep_co
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf\']);
    eval(['load tf_s' aa '_sep_co_comb.mat']);
end
cd F:\2018_RMI_gamma\coip;
cfg = [];
cfg.keepindividual = 'yes' ;
tf_sep_co_comb_avg = ft_freqgrandaverage(cfg, tf_s02_sep_co_comb, tf_s03_sep_co_comb, tf_s04_sep_co_comb, tf_s05_sep_co_comb, tf_s06_sep_co_comb, tf_s08_sep_co_comb, tf_s09_sep_co_comb, tf_s10_sep_co_comb, tf_s11_sep_co_comb, tf_s12_sep_co_comb, tf_s13_sep_co_comb, tf_s14_sep_co_comb, tf_s15_sep_co_comb, tf_s16_sep_co_comb, tf_s17_sep_co_comb, tf_s18_sep_co_comb, tf_s19_sep_co_comb, tf_s20_sep_co_comb)
save tf_sep_co_comb_avg tf_sep_co_comb_avg

% %%
%%% sep_ip
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf\']);
    eval(['load tf_s' aa '_sep_ip_comb.mat']);
end
cd F:\2018_RMI_gamma\coip;
cfg = [];
cfg.keepindividual = 'yes' ;
tf_sep_ip_comb_avg = ft_freqgrandaverage(cfg, tf_s02_sep_ip_comb, tf_s03_sep_ip_comb, tf_s04_sep_ip_comb, tf_s05_sep_ip_comb, tf_s06_sep_ip_comb, tf_s08_sep_ip_comb, tf_s09_sep_ip_comb, tf_s10_sep_ip_comb, tf_s11_sep_ip_comb, tf_s12_sep_ip_comb, tf_s13_sep_ip_comb, tf_s14_sep_ip_comb, tf_s15_sep_ip_comb, tf_s16_sep_ip_comb, tf_s17_sep_ip_comb, tf_s18_sep_ip_comb, tf_s19_sep_ip_comb, tf_s20_sep_ip_comb)
save tf_sep_ip_comb_avg tf_sep_ip_comb_avg