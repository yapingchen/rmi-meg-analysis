close all
clear all


%% gp
%% 
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf']);
    eval(['load tf_s' aa '_gp_shr_co_comb.mat']);
    eval(['load tf_s' aa '_gp_shr_ip_comb.mat']);
end

cd F:\2018_RMI_gamma\coip;
tfdiff_s02_gp_shr_co_ip = tf_s02_gp_shr_co_comb;
tfdiff_s02_gp_shr_co_ip.powspctrm = tf_s02_gp_shr_co_comb.powspctrm - tf_s02_gp_shr_ip_comb.powspctrm;
save tfdiff_s02_gp_shr_co_ip tfdiff_s02_gp_shr_co_ip

tfdiff_s03_gp_shr_co_ip = tf_s03_gp_shr_co_comb;
tfdiff_s03_gp_shr_co_ip.powspctrm = tf_s03_gp_shr_co_comb.powspctrm - tf_s03_gp_shr_ip_comb.powspctrm;
save tfdiff_s03_gp_shr_co_ip tfdiff_s03_gp_shr_co_ip

tfdiff_s04_gp_shr_co_ip = tf_s04_gp_shr_co_comb;
tfdiff_s04_gp_shr_co_ip.powspctrm = tf_s04_gp_shr_co_comb.powspctrm - tf_s04_gp_shr_ip_comb.powspctrm;
save tfdiff_s04_gp_shr_co_ip tfdiff_s04_gp_shr_co_ip

tfdiff_s05_gp_shr_co_ip = tf_s05_gp_shr_co_comb;
tfdiff_s05_gp_shr_co_ip.powspctrm = tf_s05_gp_shr_co_comb.powspctrm - tf_s05_gp_shr_ip_comb.powspctrm;
save tfdiff_s05_gp_shr_co_ip tfdiff_s05_gp_shr_co_ip

tfdiff_s06_gp_shr_co_ip = tf_s06_gp_shr_co_comb;
tfdiff_s06_gp_shr_co_ip.powspctrm = tf_s06_gp_shr_co_comb.powspctrm - tf_s06_gp_shr_ip_comb.powspctrm;
save tfdiff_s06_gp_shr_co_ip tfdiff_s06_gp_shr_co_ip

tfdiff_s08_gp_shr_co_ip = tf_s08_gp_shr_co_comb;
tfdiff_s08_gp_shr_co_ip.powspctrm = tf_s08_gp_shr_co_comb.powspctrm - tf_s08_gp_shr_ip_comb.powspctrm;
save tfdiff_s08_gp_shr_co_ip tfdiff_s08_gp_shr_co_ip

tfdiff_s09_gp_shr_co_ip = tf_s09_gp_shr_co_comb;
tfdiff_s09_gp_shr_co_ip.powspctrm = tf_s09_gp_shr_co_comb.powspctrm - tf_s09_gp_shr_ip_comb.powspctrm;
save tfdiff_s09_gp_shr_co_ip tfdiff_s09_gp_shr_co_ip

tfdiff_s10_gp_shr_co_ip = tf_s10_gp_shr_co_comb;
tfdiff_s10_gp_shr_co_ip.powspctrm = tf_s10_gp_shr_co_comb.powspctrm - tf_s10_gp_shr_ip_comb.powspctrm;
save tfdiff_s10_gp_shr_co_ip tfdiff_s10_gp_shr_co_ip

tfdiff_s11_gp_shr_co_ip = tf_s11_gp_shr_co_comb;
tfdiff_s11_gp_shr_co_ip.powspctrm = tf_s11_gp_shr_co_comb.powspctrm - tf_s11_gp_shr_ip_comb.powspctrm;
save tfdiff_s11_gp_shr_co_ip tfdiff_s11_gp_shr_co_ip

tfdiff_s12_gp_shr_co_ip = tf_s12_gp_shr_co_comb;
tfdiff_s12_gp_shr_co_ip.powspctrm = tf_s12_gp_shr_co_comb.powspctrm - tf_s12_gp_shr_ip_comb.powspctrm;
save tfdiff_s12_gp_shr_co_ip tfdiff_s12_gp_shr_co_ip

tfdiff_s13_gp_shr_co_ip = tf_s13_gp_shr_co_comb;
tfdiff_s13_gp_shr_co_ip.powspctrm = tf_s13_gp_shr_co_comb.powspctrm - tf_s13_gp_shr_ip_comb.powspctrm;
save tfdiff_s13_gp_shr_co_ip tfdiff_s13_gp_shr_co_ip

tfdiff_s14_gp_shr_co_ip = tf_s14_gp_shr_co_comb;
tfdiff_s14_gp_shr_co_ip.powspctrm = tf_s14_gp_shr_co_comb.powspctrm - tf_s14_gp_shr_ip_comb.powspctrm;
save tfdiff_s14_gp_shr_co_ip tfdiff_s14_gp_shr_co_ip

tfdiff_s15_gp_shr_co_ip = tf_s15_gp_shr_co_comb;
tfdiff_s15_gp_shr_co_ip.powspctrm = tf_s15_gp_shr_co_comb.powspctrm - tf_s15_gp_shr_ip_comb.powspctrm;
save tfdiff_s15_gp_shr_co_ip tfdiff_s15_gp_shr_co_ip

tfdiff_s16_gp_shr_co_ip = tf_s16_gp_shr_co_comb;
tfdiff_s16_gp_shr_co_ip.powspctrm = tf_s16_gp_shr_co_comb.powspctrm - tf_s16_gp_shr_ip_comb.powspctrm;
save tfdiff_s16_gp_shr_co_ip tfdiff_s16_gp_shr_co_ip

tfdiff_s17_gp_shr_co_ip = tf_s17_gp_shr_co_comb;
tfdiff_s17_gp_shr_co_ip.powspctrm = tf_s17_gp_shr_co_comb.powspctrm - tf_s17_gp_shr_ip_comb.powspctrm;
save tfdiff_s17_gp_shr_co_ip tfdiff_s17_gp_shr_co_ip

tfdiff_s18_gp_shr_co_ip = tf_s18_gp_shr_co_comb;
tfdiff_s18_gp_shr_co_ip.powspctrm = tf_s18_gp_shr_co_comb.powspctrm - tf_s18_gp_shr_ip_comb.powspctrm;
save tfdiff_s18_gp_shr_co_ip tfdiff_s18_gp_shr_co_ip

tfdiff_s19_gp_shr_co_ip = tf_s19_gp_shr_co_comb;
tfdiff_s19_gp_shr_co_ip.powspctrm = tf_s19_gp_shr_co_comb.powspctrm - tf_s19_gp_shr_ip_comb.powspctrm;
save tfdiff_s19_gp_shr_co_ip tfdiff_s19_gp_shr_co_ip

tfdiff_s20_gp_shr_co_ip = tf_s20_gp_shr_co_comb;
tfdiff_s20_gp_shr_co_ip.powspctrm = tf_s20_gp_shr_co_comb.powspctrm - tf_s20_gp_shr_ip_comb.powspctrm;
save tfdiff_s20_gp_shr_co_ip tfdiff_s20_gp_shr_co_ip

cfg = [];
cfg.keepindividual = 'yes' ;
tfdiff_gp_shr_co_ip_avg = ft_freqgrandaverage(cfg, tfdiff_s02_gp_shr_co_ip, tfdiff_s03_gp_shr_co_ip, tfdiff_s04_gp_shr_co_ip, tfdiff_s05_gp_shr_co_ip, tfdiff_s06_gp_shr_co_ip, tfdiff_s08_gp_shr_co_ip, tfdiff_s09_gp_shr_co_ip, tfdiff_s10_gp_shr_co_ip, tfdiff_s11_gp_shr_co_ip, tfdiff_s12_gp_shr_co_ip, tfdiff_s13_gp_shr_co_ip, tfdiff_s14_gp_shr_co_ip, tfdiff_s15_gp_shr_co_ip, tfdiff_s16_gp_shr_co_ip, tfdiff_s17_gp_shr_co_ip, tfdiff_s18_gp_shr_co_ip, tfdiff_s19_gp_shr_co_ip, tfdiff_s20_gp_shr_co_ip)
save tfdiff_gp_shr_co_ip_avg tfdiff_gp_shr_co_ip_avg


%%


%% 
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf']);
    eval(['load tf_s' aa '_gp_unshr_co_comb.mat']);
    eval(['load tf_s' aa '_gp_unshr_ip_comb.mat']);
end

cd F:\2018_RMI_gamma\coip

tfdiff_s02_gp_unshr_co_ip = tf_s02_gp_unshr_co_comb;
tfdiff_s02_gp_unshr_co_ip.powspctrm = tf_s02_gp_unshr_co_comb.powspctrm - tf_s02_gp_unshr_ip_comb.powspctrm;
save tfdiff_s02_gp_unshr_co_ip tfdiff_s02_gp_unshr_co_ip

tfdiff_s03_gp_unshr_co_ip = tf_s03_gp_unshr_co_comb;
tfdiff_s03_gp_unshr_co_ip.powspctrm = tf_s03_gp_unshr_co_comb.powspctrm - tf_s03_gp_unshr_ip_comb.powspctrm;
save tfdiff_s03_gp_unshr_co_ip tfdiff_s03_gp_unshr_co_ip

tfdiff_s04_gp_unshr_co_ip = tf_s04_gp_unshr_co_comb;
tfdiff_s04_gp_unshr_co_ip.powspctrm = tf_s04_gp_unshr_co_comb.powspctrm - tf_s04_gp_unshr_ip_comb.powspctrm;
save tfdiff_s04_gp_unshr_co_ip tfdiff_s04_gp_unshr_co_ip

tfdiff_s05_gp_unshr_co_ip = tf_s05_gp_unshr_co_comb;
tfdiff_s05_gp_unshr_co_ip.powspctrm = tf_s05_gp_unshr_co_comb.powspctrm - tf_s05_gp_unshr_ip_comb.powspctrm;
save tfdiff_s05_gp_unshr_co_ip tfdiff_s05_gp_unshr_co_ip

tfdiff_s06_gp_unshr_co_ip = tf_s06_gp_unshr_co_comb;
tfdiff_s06_gp_unshr_co_ip.powspctrm = tf_s06_gp_unshr_co_comb.powspctrm - tf_s06_gp_unshr_ip_comb.powspctrm;
save tfdiff_s06_gp_unshr_co_ip tfdiff_s06_gp_unshr_co_ip

tfdiff_s08_gp_unshr_co_ip = tf_s08_gp_unshr_co_comb;
tfdiff_s08_gp_unshr_co_ip.powspctrm = tf_s08_gp_unshr_co_comb.powspctrm - tf_s08_gp_unshr_ip_comb.powspctrm;
save tfdiff_s08_gp_unshr_co_ip tfdiff_s08_gp_unshr_co_ip

tfdiff_s09_gp_unshr_co_ip = tf_s09_gp_unshr_co_comb;
tfdiff_s09_gp_unshr_co_ip.powspctrm = tf_s09_gp_unshr_co_comb.powspctrm - tf_s09_gp_unshr_ip_comb.powspctrm;
save tfdiff_s09_gp_unshr_co_ip tfdiff_s09_gp_unshr_co_ip

tfdiff_s10_gp_unshr_co_ip = tf_s10_gp_unshr_co_comb;
tfdiff_s10_gp_unshr_co_ip.powspctrm = tf_s10_gp_unshr_co_comb.powspctrm - tf_s10_gp_unshr_ip_comb.powspctrm;
save tfdiff_s10_gp_unshr_co_ip tfdiff_s10_gp_unshr_co_ip

tfdiff_s11_gp_unshr_co_ip = tf_s11_gp_unshr_co_comb;
tfdiff_s11_gp_unshr_co_ip.powspctrm = tf_s11_gp_unshr_co_comb.powspctrm - tf_s11_gp_unshr_ip_comb.powspctrm;
save tfdiff_s11_gp_unshr_co_ip tfdiff_s11_gp_unshr_co_ip

tfdiff_s12_gp_unshr_co_ip = tf_s12_gp_unshr_co_comb;
tfdiff_s12_gp_unshr_co_ip.powspctrm = tf_s12_gp_unshr_co_comb.powspctrm - tf_s12_gp_unshr_ip_comb.powspctrm;
save tfdiff_s12_gp_unshr_co_ip tfdiff_s12_gp_unshr_co_ip

tfdiff_s13_gp_unshr_co_ip = tf_s13_gp_unshr_co_comb;
tfdiff_s13_gp_unshr_co_ip.powspctrm = tf_s13_gp_unshr_co_comb.powspctrm - tf_s13_gp_unshr_ip_comb.powspctrm;
save tfdiff_s13_gp_unshr_co_ip tfdiff_s13_gp_unshr_co_ip

tfdiff_s14_gp_unshr_co_ip = tf_s14_gp_unshr_co_comb;
tfdiff_s14_gp_unshr_co_ip.powspctrm = tf_s14_gp_unshr_co_comb.powspctrm - tf_s14_gp_unshr_ip_comb.powspctrm;
save tfdiff_s14_gp_unshr_co_ip tfdiff_s14_gp_unshr_co_ip

tfdiff_s15_gp_unshr_co_ip = tf_s15_gp_unshr_co_comb;
tfdiff_s15_gp_unshr_co_ip.powspctrm = tf_s15_gp_unshr_co_comb.powspctrm - tf_s15_gp_unshr_ip_comb.powspctrm;
save tfdiff_s15_gp_unshr_co_ip tfdiff_s15_gp_unshr_co_ip

tfdiff_s16_gp_unshr_co_ip = tf_s16_gp_unshr_co_comb;
tfdiff_s16_gp_unshr_co_ip.powspctrm = tf_s16_gp_unshr_co_comb.powspctrm - tf_s16_gp_unshr_ip_comb.powspctrm;
save tfdiff_s16_gp_unshr_co_ip tfdiff_s16_gp_unshr_co_ip

tfdiff_s17_gp_unshr_co_ip = tf_s17_gp_unshr_co_comb;
tfdiff_s17_gp_unshr_co_ip.powspctrm = tf_s17_gp_unshr_co_comb.powspctrm - tf_s17_gp_unshr_ip_comb.powspctrm;
save tfdiff_s17_gp_unshr_co_ip tfdiff_s17_gp_unshr_co_ip

tfdiff_s18_gp_unshr_co_ip = tf_s18_gp_unshr_co_comb;
tfdiff_s18_gp_unshr_co_ip.powspctrm = tf_s18_gp_unshr_co_comb.powspctrm - tf_s18_gp_unshr_ip_comb.powspctrm;
save tfdiff_s18_gp_unshr_co_ip tfdiff_s18_gp_unshr_co_ip

tfdiff_s19_gp_unshr_co_ip = tf_s19_gp_unshr_co_comb;
tfdiff_s19_gp_unshr_co_ip.powspctrm = tf_s19_gp_unshr_co_comb.powspctrm - tf_s19_gp_unshr_ip_comb.powspctrm;
save tfdiff_s19_gp_unshr_co_ip tfdiff_s19_gp_unshr_co_ip

tfdiff_s20_gp_unshr_co_ip = tf_s20_gp_unshr_co_comb;
tfdiff_s20_gp_unshr_co_ip.powspctrm = tf_s20_gp_unshr_co_comb.powspctrm - tf_s20_gp_unshr_ip_comb.powspctrm;
save tfdiff_s20_gp_unshr_co_ip tfdiff_s20_gp_unshr_co_ip

cfg = [];
cfg.keepindividual = 'yes' ;
tfdiff_gp_unshr_co_ip_avg = ft_freqgrandaverage(cfg, tfdiff_s02_gp_unshr_co_ip, tfdiff_s03_gp_unshr_co_ip, tfdiff_s04_gp_unshr_co_ip, tfdiff_s05_gp_unshr_co_ip, tfdiff_s06_gp_unshr_co_ip, tfdiff_s08_gp_unshr_co_ip, tfdiff_s09_gp_unshr_co_ip, tfdiff_s10_gp_unshr_co_ip, tfdiff_s11_gp_unshr_co_ip, tfdiff_s12_gp_unshr_co_ip, tfdiff_s13_gp_unshr_co_ip, tfdiff_s14_gp_unshr_co_ip, tfdiff_s15_gp_unshr_co_ip, tfdiff_s16_gp_unshr_co_ip, tfdiff_s17_gp_unshr_co_ip, tfdiff_s18_gp_unshr_co_ip, tfdiff_s19_gp_unshr_co_ip, tfdiff_s20_gp_unshr_co_ip)
save tfdiff_gp_unshr_co_ip_avg tfdiff_gp_unshr_co_ip_avg

%%

%% fr
%% 
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf']);
    eval(['load tf_s' aa '_fr_shr_co_comb.mat']);
    eval(['load tf_s' aa '_fr_shr_ip_comb.mat']);
end

cd F:\2018_RMI_gamma\coip

tfdiff_s02_fr_shr_co_ip = tf_s02_fr_shr_co_comb;
tfdiff_s02_fr_shr_co_ip.powspctrm = tf_s02_fr_shr_co_comb.powspctrm - tf_s02_fr_shr_ip_comb.powspctrm;
save tfdiff_s02_fr_shr_co_ip tfdiff_s02_fr_shr_co_ip

tfdiff_s03_fr_shr_co_ip = tf_s03_fr_shr_co_comb;
tfdiff_s03_fr_shr_co_ip.powspctrm = tf_s03_fr_shr_co_comb.powspctrm - tf_s03_fr_shr_ip_comb.powspctrm;
save tfdiff_s03_fr_shr_co_ip tfdiff_s03_fr_shr_co_ip

tfdiff_s04_fr_shr_co_ip = tf_s04_fr_shr_co_comb;
tfdiff_s04_fr_shr_co_ip.powspctrm = tf_s04_fr_shr_co_comb.powspctrm - tf_s04_fr_shr_ip_comb.powspctrm;
save tfdiff_s04_fr_shr_co_ip tfdiff_s04_fr_shr_co_ip

tfdiff_s05_fr_shr_co_ip = tf_s05_fr_shr_co_comb;
tfdiff_s05_fr_shr_co_ip.powspctrm = tf_s05_fr_shr_co_comb.powspctrm - tf_s05_fr_shr_ip_comb.powspctrm;
save tfdiff_s05_fr_shr_co_ip tfdiff_s05_fr_shr_co_ip

tfdiff_s06_fr_shr_co_ip = tf_s06_fr_shr_co_comb;
tfdiff_s06_fr_shr_co_ip.powspctrm = tf_s06_fr_shr_co_comb.powspctrm - tf_s06_fr_shr_ip_comb.powspctrm;
save tfdiff_s06_fr_shr_co_ip tfdiff_s06_fr_shr_co_ip

tfdiff_s08_fr_shr_co_ip = tf_s08_fr_shr_co_comb;
tfdiff_s08_fr_shr_co_ip.powspctrm = tf_s08_fr_shr_co_comb.powspctrm - tf_s08_fr_shr_ip_comb.powspctrm;
save tfdiff_s08_fr_shr_co_ip tfdiff_s08_fr_shr_co_ip

tfdiff_s09_fr_shr_co_ip = tf_s09_fr_shr_co_comb;
tfdiff_s09_fr_shr_co_ip.powspctrm = tf_s09_fr_shr_co_comb.powspctrm - tf_s09_fr_shr_ip_comb.powspctrm;
save tfdiff_s09_fr_shr_co_ip tfdiff_s09_fr_shr_co_ip

tfdiff_s10_fr_shr_co_ip = tf_s10_fr_shr_co_comb;
tfdiff_s10_fr_shr_co_ip.powspctrm = tf_s10_fr_shr_co_comb.powspctrm - tf_s10_fr_shr_ip_comb.powspctrm;
save tfdiff_s10_fr_shr_co_ip tfdiff_s10_fr_shr_co_ip

tfdiff_s11_fr_shr_co_ip = tf_s11_fr_shr_co_comb;
tfdiff_s11_fr_shr_co_ip.powspctrm = tf_s11_fr_shr_co_comb.powspctrm - tf_s11_fr_shr_ip_comb.powspctrm;
save tfdiff_s11_fr_shr_co_ip tfdiff_s11_fr_shr_co_ip

tfdiff_s12_fr_shr_co_ip = tf_s12_fr_shr_co_comb;
tfdiff_s12_fr_shr_co_ip.powspctrm = tf_s12_fr_shr_co_comb.powspctrm - tf_s12_fr_shr_ip_comb.powspctrm;
save tfdiff_s12_fr_shr_co_ip tfdiff_s12_fr_shr_co_ip

tfdiff_s13_fr_shr_co_ip = tf_s13_fr_shr_co_comb;
tfdiff_s13_fr_shr_co_ip.powspctrm = tf_s13_fr_shr_co_comb.powspctrm - tf_s13_fr_shr_ip_comb.powspctrm;
save tfdiff_s13_fr_shr_co_ip tfdiff_s13_fr_shr_co_ip

tfdiff_s14_fr_shr_co_ip = tf_s14_fr_shr_co_comb;
tfdiff_s14_fr_shr_co_ip.powspctrm = tf_s14_fr_shr_co_comb.powspctrm - tf_s14_fr_shr_ip_comb.powspctrm;
save tfdiff_s14_fr_shr_co_ip tfdiff_s14_fr_shr_co_ip

tfdiff_s15_fr_shr_co_ip = tf_s15_fr_shr_co_comb;
tfdiff_s15_fr_shr_co_ip.powspctrm = tf_s15_fr_shr_co_comb.powspctrm - tf_s15_fr_shr_ip_comb.powspctrm;
save tfdiff_s15_fr_shr_co_ip tfdiff_s15_fr_shr_co_ip

tfdiff_s16_fr_shr_co_ip = tf_s16_fr_shr_co_comb;
tfdiff_s16_fr_shr_co_ip.powspctrm = tf_s16_fr_shr_co_comb.powspctrm - tf_s16_fr_shr_ip_comb.powspctrm;
save tfdiff_s16_fr_shr_co_ip tfdiff_s16_fr_shr_co_ip

tfdiff_s17_fr_shr_co_ip = tf_s17_fr_shr_co_comb;
tfdiff_s17_fr_shr_co_ip.powspctrm = tf_s17_fr_shr_co_comb.powspctrm - tf_s17_fr_shr_ip_comb.powspctrm;
save tfdiff_s17_fr_shr_co_ip tfdiff_s17_fr_shr_co_ip

tfdiff_s18_fr_shr_co_ip = tf_s18_fr_shr_co_comb;
tfdiff_s18_fr_shr_co_ip.powspctrm = tf_s18_fr_shr_co_comb.powspctrm - tf_s18_fr_shr_ip_comb.powspctrm;
save tfdiff_s18_fr_shr_co_ip tfdiff_s18_fr_shr_co_ip

tfdiff_s19_fr_shr_co_ip = tf_s19_fr_shr_co_comb;
tfdiff_s19_fr_shr_co_ip.powspctrm = tf_s19_fr_shr_co_comb.powspctrm - tf_s19_fr_shr_ip_comb.powspctrm;
save tfdiff_s19_fr_shr_co_ip tfdiff_s19_fr_shr_co_ip

tfdiff_s20_fr_shr_co_ip = tf_s20_fr_shr_co_comb;
tfdiff_s20_fr_shr_co_ip.powspctrm = tf_s20_fr_shr_co_comb.powspctrm - tf_s20_fr_shr_ip_comb.powspctrm;
save tfdiff_s20_fr_shr_co_ip tfdiff_s20_fr_shr_co_ip

cfg = [];
cfg.keepindividual = 'yes' ;
tfdiff_fr_shr_co_ip_avg = ft_freqgrandaverage(cfg, tfdiff_s02_fr_shr_co_ip, tfdiff_s03_fr_shr_co_ip, tfdiff_s04_fr_shr_co_ip, tfdiff_s05_fr_shr_co_ip, tfdiff_s06_fr_shr_co_ip, tfdiff_s08_fr_shr_co_ip, tfdiff_s09_fr_shr_co_ip, tfdiff_s10_fr_shr_co_ip, tfdiff_s11_fr_shr_co_ip, tfdiff_s12_fr_shr_co_ip, tfdiff_s13_fr_shr_co_ip, tfdiff_s14_fr_shr_co_ip, tfdiff_s15_fr_shr_co_ip, tfdiff_s16_fr_shr_co_ip, tfdiff_s17_fr_shr_co_ip, tfdiff_s18_fr_shr_co_ip, tfdiff_s19_fr_shr_co_ip, tfdiff_s20_fr_shr_co_ip)
save tfdiff_fr_shr_co_ip_avg tfdiff_fr_shr_co_ip_avg


%%




%% 
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf']);
    eval(['load tf_s' aa '_fr_unshr_co_comb.mat']);
    eval(['load tf_s' aa '_fr_unshr_ip_comb.mat']);
end

cd F:\2018_RMI_gamma\coip

tfdiff_s02_fr_unshr_co_ip = tf_s02_fr_unshr_co_comb;
tfdiff_s02_fr_unshr_co_ip.powspctrm = tf_s02_fr_unshr_co_comb.powspctrm - tf_s02_fr_unshr_ip_comb.powspctrm;
save tfdiff_s02_fr_unshr_co_ip tfdiff_s02_fr_unshr_co_ip

tfdiff_s03_fr_unshr_co_ip = tf_s03_fr_unshr_co_comb;
tfdiff_s03_fr_unshr_co_ip.powspctrm = tf_s03_fr_unshr_co_comb.powspctrm - tf_s03_fr_unshr_ip_comb.powspctrm;
save tfdiff_s03_fr_unshr_co_ip tfdiff_s03_fr_unshr_co_ip

tfdiff_s04_fr_unshr_co_ip = tf_s04_fr_unshr_co_comb;
tfdiff_s04_fr_unshr_co_ip.powspctrm = tf_s04_fr_unshr_co_comb.powspctrm - tf_s04_fr_unshr_ip_comb.powspctrm;
save tfdiff_s04_fr_unshr_co_ip tfdiff_s04_fr_unshr_co_ip

tfdiff_s05_fr_unshr_co_ip = tf_s05_fr_unshr_co_comb;
tfdiff_s05_fr_unshr_co_ip.powspctrm = tf_s05_fr_unshr_co_comb.powspctrm - tf_s05_fr_unshr_ip_comb.powspctrm;
save tfdiff_s05_fr_unshr_co_ip tfdiff_s05_fr_unshr_co_ip

tfdiff_s06_fr_unshr_co_ip = tf_s06_fr_unshr_co_comb;
tfdiff_s06_fr_unshr_co_ip.powspctrm = tf_s06_fr_unshr_co_comb.powspctrm - tf_s06_fr_unshr_ip_comb.powspctrm;
save tfdiff_s06_fr_unshr_co_ip tfdiff_s06_fr_unshr_co_ip

tfdiff_s08_fr_unshr_co_ip = tf_s08_fr_unshr_co_comb;
tfdiff_s08_fr_unshr_co_ip.powspctrm = tf_s08_fr_unshr_co_comb.powspctrm - tf_s08_fr_unshr_ip_comb.powspctrm;
save tfdiff_s08_fr_unshr_co_ip tfdiff_s08_fr_unshr_co_ip

tfdiff_s09_fr_unshr_co_ip = tf_s09_fr_unshr_co_comb;
tfdiff_s09_fr_unshr_co_ip.powspctrm = tf_s09_fr_unshr_co_comb.powspctrm - tf_s09_fr_unshr_ip_comb.powspctrm;
save tfdiff_s09_fr_unshr_co_ip tfdiff_s09_fr_unshr_co_ip

tfdiff_s10_fr_unshr_co_ip = tf_s10_fr_unshr_co_comb;
tfdiff_s10_fr_unshr_co_ip.powspctrm = tf_s10_fr_unshr_co_comb.powspctrm - tf_s10_fr_unshr_ip_comb.powspctrm;
save tfdiff_s10_fr_unshr_co_ip tfdiff_s10_fr_unshr_co_ip

tfdiff_s11_fr_unshr_co_ip = tf_s11_fr_unshr_co_comb;
tfdiff_s11_fr_unshr_co_ip.powspctrm = tf_s11_fr_unshr_co_comb.powspctrm - tf_s11_fr_unshr_ip_comb.powspctrm;
save tfdiff_s11_fr_unshr_co_ip tfdiff_s11_fr_unshr_co_ip

tfdiff_s12_fr_unshr_co_ip = tf_s12_fr_unshr_co_comb;
tfdiff_s12_fr_unshr_co_ip.powspctrm = tf_s12_fr_unshr_co_comb.powspctrm - tf_s12_fr_unshr_ip_comb.powspctrm;
save tfdiff_s12_fr_unshr_co_ip tfdiff_s12_fr_unshr_co_ip

tfdiff_s13_fr_unshr_co_ip = tf_s13_fr_unshr_co_comb;
tfdiff_s13_fr_unshr_co_ip.powspctrm = tf_s13_fr_unshr_co_comb.powspctrm - tf_s13_fr_unshr_ip_comb.powspctrm;
save tfdiff_s13_fr_unshr_co_ip tfdiff_s13_fr_unshr_co_ip

tfdiff_s14_fr_unshr_co_ip = tf_s14_fr_unshr_co_comb;
tfdiff_s14_fr_unshr_co_ip.powspctrm = tf_s14_fr_unshr_co_comb.powspctrm - tf_s14_fr_unshr_ip_comb.powspctrm;
save tfdiff_s14_fr_unshr_co_ip tfdiff_s14_fr_unshr_co_ip

tfdiff_s15_fr_unshr_co_ip = tf_s15_fr_unshr_co_comb;
tfdiff_s15_fr_unshr_co_ip.powspctrm = tf_s15_fr_unshr_co_comb.powspctrm - tf_s15_fr_unshr_ip_comb.powspctrm;
save tfdiff_s15_fr_unshr_co_ip tfdiff_s15_fr_unshr_co_ip

tfdiff_s16_fr_unshr_co_ip = tf_s16_fr_unshr_co_comb;
tfdiff_s16_fr_unshr_co_ip.powspctrm = tf_s16_fr_unshr_co_comb.powspctrm - tf_s16_fr_unshr_ip_comb.powspctrm;
save tfdiff_s16_fr_unshr_co_ip tfdiff_s16_fr_unshr_co_ip

tfdiff_s17_fr_unshr_co_ip = tf_s17_fr_unshr_co_comb;
tfdiff_s17_fr_unshr_co_ip.powspctrm = tf_s17_fr_unshr_co_comb.powspctrm - tf_s17_fr_unshr_ip_comb.powspctrm;
save tfdiff_s17_fr_unshr_co_ip tfdiff_s17_fr_unshr_co_ip

tfdiff_s18_fr_unshr_co_ip = tf_s18_fr_unshr_co_comb;
tfdiff_s18_fr_unshr_co_ip.powspctrm = tf_s18_fr_unshr_co_comb.powspctrm - tf_s18_fr_unshr_ip_comb.powspctrm;
save tfdiff_s18_fr_unshr_co_ip tfdiff_s18_fr_unshr_co_ip

tfdiff_s19_fr_unshr_co_ip = tf_s19_fr_unshr_co_comb;
tfdiff_s19_fr_unshr_co_ip.powspctrm = tf_s19_fr_unshr_co_comb.powspctrm - tf_s19_fr_unshr_ip_comb.powspctrm;
save tfdiff_s19_fr_unshr_co_ip tfdiff_s19_fr_unshr_co_ip

tfdiff_s20_fr_unshr_co_ip = tf_s20_fr_unshr_co_comb;
tfdiff_s20_fr_unshr_co_ip.powspctrm = tf_s20_fr_unshr_co_comb.powspctrm - tf_s20_fr_unshr_ip_comb.powspctrm;
save tfdiff_s20_fr_unshr_co_ip tfdiff_s20_fr_unshr_co_ip

cfg = [];
cfg.keepindividual = 'yes' ;
tfdiff_fr_unshr_co_ip_avg = ft_freqgrandaverage(cfg, tfdiff_s02_fr_unshr_co_ip, tfdiff_s03_fr_unshr_co_ip, tfdiff_s04_fr_unshr_co_ip, tfdiff_s05_fr_unshr_co_ip, tfdiff_s06_fr_unshr_co_ip, tfdiff_s08_fr_unshr_co_ip, tfdiff_s09_fr_unshr_co_ip, tfdiff_s10_fr_unshr_co_ip, tfdiff_s11_fr_unshr_co_ip, tfdiff_s12_fr_unshr_co_ip, tfdiff_s13_fr_unshr_co_ip, tfdiff_s14_fr_unshr_co_ip, tfdiff_s15_fr_unshr_co_ip, tfdiff_s16_fr_unshr_co_ip, tfdiff_s17_fr_unshr_co_ip, tfdiff_s18_fr_unshr_co_ip, tfdiff_s19_fr_unshr_co_ip, tfdiff_s20_fr_unshr_co_ip)
save tfdiff_fr_unshr_co_ip_avg tfdiff_fr_unshr_co_ip_avg
clear all

%%



%% sep
%% 
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['cd F:\2018_RMI_gamma\coip\s' aa '\rmtf']);
    eval(['load tf_s' aa '_sep_co_comb.mat']);
    eval(['load tf_s' aa '_sep_ip_comb.mat']);
end

cd F:\2018_RMI_gamma\coip

tfdiff_s02_sep_co_ip = tf_s02_sep_co_comb;
tfdiff_s02_sep_co_ip.powspctrm = tf_s02_sep_co_comb.powspctrm - tf_s02_sep_ip_comb.powspctrm;
save tfdiff_s02_sep_co_ip tfdiff_s02_sep_co_ip

tfdiff_s03_sep_co_ip = tf_s03_sep_co_comb;
tfdiff_s03_sep_co_ip.powspctrm = tf_s03_sep_co_comb.powspctrm - tf_s03_sep_ip_comb.powspctrm;
save tfdiff_s03_sep_co_ip tfdiff_s03_sep_co_ip

tfdiff_s04_sep_co_ip = tf_s04_sep_co_comb;
tfdiff_s04_sep_co_ip.powspctrm = tf_s04_sep_co_comb.powspctrm - tf_s04_sep_ip_comb.powspctrm;
save tfdiff_s04_sep_co_ip tfdiff_s04_sep_co_ip

tfdiff_s05_sep_co_ip = tf_s05_sep_co_comb;
tfdiff_s05_sep_co_ip.powspctrm = tf_s05_sep_co_comb.powspctrm - tf_s05_sep_ip_comb.powspctrm;
save tfdiff_s05_sep_co_ip tfdiff_s05_sep_co_ip

tfdiff_s06_sep_co_ip = tf_s06_sep_co_comb;
tfdiff_s06_sep_co_ip.powspctrm = tf_s06_sep_co_comb.powspctrm - tf_s06_sep_ip_comb.powspctrm;
save tfdiff_s06_sep_co_ip tfdiff_s06_sep_co_ip

tfdiff_s08_sep_co_ip = tf_s08_sep_co_comb;
tfdiff_s08_sep_co_ip.powspctrm = tf_s08_sep_co_comb.powspctrm - tf_s08_sep_ip_comb.powspctrm;
save tfdiff_s08_sep_co_ip tfdiff_s08_sep_co_ip

tfdiff_s09_sep_co_ip = tf_s09_sep_co_comb;
tfdiff_s09_sep_co_ip.powspctrm = tf_s09_sep_co_comb.powspctrm - tf_s09_sep_ip_comb.powspctrm;
save tfdiff_s09_sep_co_ip tfdiff_s09_sep_co_ip

tfdiff_s10_sep_co_ip = tf_s10_sep_co_comb;
tfdiff_s10_sep_co_ip.powspctrm = tf_s10_sep_co_comb.powspctrm - tf_s10_sep_ip_comb.powspctrm;
save tfdiff_s10_sep_co_ip tfdiff_s10_sep_co_ip

tfdiff_s11_sep_co_ip = tf_s11_sep_co_comb;
tfdiff_s11_sep_co_ip.powspctrm = tf_s11_sep_co_comb.powspctrm - tf_s11_sep_ip_comb.powspctrm;
save tfdiff_s11_sep_co_ip tfdiff_s11_sep_co_ip

tfdiff_s12_sep_co_ip = tf_s12_sep_co_comb;
tfdiff_s12_sep_co_ip.powspctrm = tf_s12_sep_co_comb.powspctrm - tf_s12_sep_ip_comb.powspctrm;
save tfdiff_s12_sep_co_ip tfdiff_s12_sep_co_ip

tfdiff_s13_sep_co_ip = tf_s13_sep_co_comb;
tfdiff_s13_sep_co_ip.powspctrm = tf_s13_sep_co_comb.powspctrm - tf_s13_sep_ip_comb.powspctrm;
save tfdiff_s13_sep_co_ip tfdiff_s13_sep_co_ip

tfdiff_s14_sep_co_ip = tf_s14_sep_co_comb;
tfdiff_s14_sep_co_ip.powspctrm = tf_s14_sep_co_comb.powspctrm - tf_s14_sep_ip_comb.powspctrm;
save tfdiff_s14_sep_co_ip tfdiff_s14_sep_co_ip

tfdiff_s15_sep_co_ip = tf_s15_sep_co_comb;
tfdiff_s15_sep_co_ip.powspctrm = tf_s15_sep_co_comb.powspctrm - tf_s15_sep_ip_comb.powspctrm;
save tfdiff_s15_sep_co_ip tfdiff_s15_sep_co_ip

tfdiff_s16_sep_co_ip = tf_s16_sep_co_comb;
tfdiff_s16_sep_co_ip.powspctrm = tf_s16_sep_co_comb.powspctrm - tf_s16_sep_ip_comb.powspctrm;
save tfdiff_s16_sep_co_ip tfdiff_s16_sep_co_ip

tfdiff_s17_sep_co_ip = tf_s17_sep_co_comb;
tfdiff_s17_sep_co_ip.powspctrm = tf_s17_sep_co_comb.powspctrm - tf_s17_sep_ip_comb.powspctrm;
save tfdiff_s17_sep_co_ip tfdiff_s17_sep_co_ip

tfdiff_s18_sep_co_ip = tf_s18_sep_co_comb;
tfdiff_s18_sep_co_ip.powspctrm = tf_s18_sep_co_comb.powspctrm - tf_s18_sep_ip_comb.powspctrm;
save tfdiff_s18_sep_co_ip tfdiff_s18_sep_co_ip

tfdiff_s19_sep_co_ip = tf_s19_sep_co_comb;
tfdiff_s19_sep_co_ip.powspctrm = tf_s19_sep_co_comb.powspctrm - tf_s19_sep_ip_comb.powspctrm;
save tfdiff_s19_sep_co_ip tfdiff_s19_sep_co_ip

tfdiff_s20_sep_co_ip = tf_s20_sep_co_comb;
tfdiff_s20_sep_co_ip.powspctrm = tf_s20_sep_co_comb.powspctrm - tf_s20_sep_ip_comb.powspctrm;
save tfdiff_s20_sep_co_ip tfdiff_s20_sep_co_ip

cfg = [];
cfg.keepindividual = 'yes' ;
tfdiff_sep_co_ip_avg = ft_freqgrandaverage(cfg, tfdiff_s02_sep_co_ip, tfdiff_s03_sep_co_ip, tfdiff_s04_sep_co_ip, tfdiff_s05_sep_co_ip, tfdiff_s06_sep_co_ip, tfdiff_s08_sep_co_ip, tfdiff_s09_sep_co_ip, tfdiff_s10_sep_co_ip, tfdiff_s11_sep_co_ip, tfdiff_s12_sep_co_ip, tfdiff_s13_sep_co_ip, tfdiff_s14_sep_co_ip, tfdiff_s15_sep_co_ip, tfdiff_s16_sep_co_ip, tfdiff_s17_sep_co_ip, tfdiff_s18_sep_co_ip, tfdiff_s19_sep_co_ip, tfdiff_s20_sep_co_ip)
save tfdiff_sep_co_ip_avg tfdiff_sep_co_ip_avg
clear all

%% plotting

%%
rmpath(genpath('D:\0_fMRI analysis\spm8_meeg\'))
rmpath(genpath('D:\0_EEG analysis\fieldtrip\'))
addpath(genpath('D:\0_EEG analysis\fieldtrip-20140611\'))
rmpath(genpath('D:\0_EEG analysis\fieldtrip-20160506\'))

%%
close all
clear all

load tfdiff_gp_shr_co_ip_avg
load tfdiff_gp_unshr_co_ip_avg
load tfdiff_fr_shr_co_ip_avg
load tfdiff_fr_unshr_co_ip_avg
load tfdiff_sep_co_ip_avg

cfg.colorbar = 'yes';
cfg.interactive = 'yes';
cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';   
cfg.xlim = [-0.2 0.8]; % time
% cfg.ylim = [1 20];
% cfg.zlim = [-2 2];

figure(1)
ft_multiplotTFR(cfg, tfdiff_gp_shr_co_ip_avg); 

figure(2)
ft_multiplotTFR(cfg, tfdiff_gp_unshr_co_ip_avg); 

figure(3)
ft_multiplotTFR(cfg, tfdiff_fr_shr_co_ip_avg); 

figure(4)
ft_multiplotTFR(cfg, tfdiff_fr_unshr_co_ip_avg); 

figure(5)
ft_multiplotTFR(cfg, tfdiff_sep_co_ip_avg); 


%%
close all
clear all

load tfdiff_gp_shr_co_ip_avg
load tfdiff_gp_unshr_co_ip_avg
load tfdiff_fr_shr_co_ip_avg
load tfdiff_fr_unshr_co_ip_avg
load tfdiff_sep_co_ip_avg

cfg.colorbar = 'yes';
cfg.interactive = 'yes';
cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306mag.lay';   
cfg.xlim = [-0.2 0.8]; % time
cfg.ylim = [30 58];
% cfg.zlim = [-1.5 1.5];

figure(1)
ft_multiplotTFR(cfg, tfdiff_gp_shr_co_ip_avg); 

figure(2)
ft_multiplotTFR(cfg, tfdiff_gp_unshr_co_ip_avg); 

figure(3)
ft_multiplotTFR(cfg, tfdiff_fr_shr_co_ip_avg); 

figure(4)
ft_multiplotTFR(cfg, tfdiff_fr_unshr_co_ip_avg); 

figure(5)
ft_multiplotTFR(cfg, tfdiff_sep_co_ip_avg); 
%%







