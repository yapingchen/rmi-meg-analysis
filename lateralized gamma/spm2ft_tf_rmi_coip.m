close all
clear all
clc
addpath(genpath('D:\0_fMRI analysis\spm8_meeg'))
rmpath(genpath('D:\0_fMRI analysis\spm8bc'))
addpath(genpath('D:\0_EEG analysis\tf_script'))
addpath(genpath('D:\0_EEG analysis\fieldtrip-20140611'));

%%
cd 'F:\2018_RMI_gamma\coip\s20\rmtf' % where the data is
% %% group
% %% _gp_Lshr_co

D = spm_eeg_load ('rmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 1
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_gp_Lshr_co = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_gp_Lshr_co.grad = cfg.grad;

save tf_s20_gp_Lshr_co tf_s20_gp_Lshr_co

tf_s20_gp_Lshr_co_check = checkdata(tf_s20_gp_Lshr_co, 'senstype','neuromag306');
tf_s20_gp_Lshr_co_check2 = checkdata(tf_s20_gp_Lshr_co_check, 'datatype', 'freq');
tf_s20_gp_Lshr_co_comb = ft_combineplanar(cfg, tf_s20_gp_Lshr_co_check);

save tf_s20_gp_Lshr_co_comb tf_s20_gp_Lshr_co_comb
clear all;

% %% 
% %% _gp_Rshr_co

D = spm_eeg_load ('rmtf_Mprcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 2
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_gp_Rshr_co = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_gp_Rshr_co.grad = cfg.grad;


save tf_s20_gp_Rshr_co tf_s20_gp_Rshr_co

tf_s20_gp_Rshr_co_check = checkdata(tf_s20_gp_Rshr_co, 'senstype','neuromag306');
tf_s20_gp_Rshr_co_check2 = checkdata(tf_s20_gp_Rshr_co_check, 'datatype', 'freq');
tf_s20_gp_Rshr_co_comb = ft_combineplanar(cfg, tf_s20_gp_Rshr_co_check);

save tf_s20_gp_Rshr_co_comb tf_s20_gp_Rshr_co_comb
clear all;

% %% 

close all
clear all

load tf_s20_gp_Lshr_co
load tf_s20_gp_Rshr_co

tf_s20_gp_shr_co = tf_s20_gp_Lshr_co;
tf_s20_gp_shr_co.powspctrm = (tf_s20_gp_Lshr_co.powspctrm + tf_s20_gp_Rshr_co.powspctrm)./2
save tf_s20_gp_shr_co tf_s20_gp_shr_co

clear all

load tf_s20_gp_Lshr_co_comb
load tf_s20_gp_Rshr_co_comb

tf_s20_gp_shr_co_comb = tf_s20_gp_Lshr_co_comb;
tf_s20_gp_shr_co_comb.powspctrm = (tf_s20_gp_Lshr_co_comb.powspctrm + tf_s20_gp_Rshr_co_comb.powspctrm)./2
save tf_s20_gp_shr_co_comb tf_s20_gp_shr_co_comb

clear all;

% %%
% %% _gp_Rshr_ip

D = spm_eeg_load ('rmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 2
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_gp_Rshr_ip = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_gp_Rshr_ip.grad = cfg.grad;


save tf_s20_gp_Rshr_ip tf_s20_gp_Rshr_ip

tf_s20_gp_Rshr_ip_check = checkdata(tf_s20_gp_Rshr_ip, 'senstype','neuromag306');
tf_s20_gp_Rshr_ip_check2 = checkdata(tf_s20_gp_Rshr_ip_check, 'datatype', 'freq');
tf_s20_gp_Rshr_ip_comb = ft_combineplanar(cfg, tf_s20_gp_Rshr_ip_check);

save tf_s20_gp_Rshr_ip_comb tf_s20_gp_Rshr_ip_comb
clear all;

% %% 
% %% _gp_Lshr_ip

D = spm_eeg_load ('rmtf_Mprcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 1
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_gp_Lshr_ip = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_gp_Lshr_ip.grad = cfg.grad;


save tf_s20_gp_Lshr_ip tf_s20_gp_Lshr_ip

tf_s20_gp_Lshr_ip_check = checkdata(tf_s20_gp_Lshr_ip, 'senstype','neuromag306');
tf_s20_gp_Lshr_ip_check2 = checkdata(tf_s20_gp_Lshr_ip_check, 'datatype', 'freq');
tf_s20_gp_Lshr_ip_comb = ft_combineplanar(cfg, tf_s20_gp_Lshr_ip_check);

save tf_s20_gp_Lshr_ip_comb tf_s20_gp_Lshr_ip_comb
clear all;

% %% 
% %% 

close all
clear all

load tf_s20_gp_Lshr_ip
load tf_s20_gp_Rshr_ip

tf_s20_gp_shr_ip = tf_s20_gp_Lshr_ip;
tf_s20_gp_shr_ip.powspctrm = (tf_s20_gp_Lshr_ip.powspctrm + tf_s20_gp_Rshr_ip.powspctrm)./2
save tf_s20_gp_shr_ip tf_s20_gp_shr_ip

clear all

load tf_s20_gp_Lshr_ip_comb
load tf_s20_gp_Rshr_ip_comb

tf_s20_gp_shr_ip_comb = tf_s20_gp_Lshr_ip_comb;
tf_s20_gp_shr_ip_comb.powspctrm = (tf_s20_gp_Lshr_ip_comb.powspctrm + tf_s20_gp_Rshr_ip_comb.powspctrm)./2
save tf_s20_gp_shr_ip_comb tf_s20_gp_shr_ip_comb

clear all;

% %%



% %% _gp_Lunshr_co

D = spm_eeg_load ('rmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 3
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_gp_Lunshr_co = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_gp_Lunshr_co.grad = cfg.grad;


save tf_s20_gp_Lunshr_co tf_s20_gp_Lunshr_co

tf_s20_gp_Lunshr_co_check = checkdata(tf_s20_gp_Lunshr_co, 'senstype','neuromag306');
tf_s20_gp_Lunshr_co_check2 = checkdata(tf_s20_gp_Lunshr_co_check, 'datatype', 'freq');
tf_s20_gp_Lunshr_co_comb = ft_combineplanar(cfg, tf_s20_gp_Lunshr_co_check);

save tf_s20_gp_Lunshr_co_comb tf_s20_gp_Lunshr_co_comb
clear all;

% %% 
% %% _gp_Runshr_co

D = spm_eeg_load ('rmtf_Mprcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 4
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_gp_Runshr_co = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_gp_Runshr_co.grad = cfg.grad;


save tf_s20_gp_Runshr_co tf_s20_gp_Runshr_co

tf_s20_gp_Runshr_co_check = checkdata(tf_s20_gp_Runshr_co, 'senstype','neuromag306');
tf_s20_gp_Runshr_co_check2 = checkdata(tf_s20_gp_Runshr_co_check, 'datatype', 'freq');
tf_s20_gp_Runshr_co_comb = ft_combineplanar(cfg, tf_s20_gp_Runshr_co_check);

save tf_s20_gp_Runshr_co_comb tf_s20_gp_Runshr_co_comb
clear all;

% %% 

close all
clear all

load tf_s20_gp_Lunshr_co
load tf_s20_gp_Runshr_co

tf_s20_gp_unshr_co = tf_s20_gp_Lunshr_co;
tf_s20_gp_unshr_co.powspctrm = (tf_s20_gp_Lunshr_co.powspctrm + tf_s20_gp_Runshr_co.powspctrm)./2
save tf_s20_gp_unshr_co tf_s20_gp_unshr_co

clear all

load tf_s20_gp_Lunshr_co_comb
load tf_s20_gp_Runshr_co_comb

tf_s20_gp_unshr_co_comb = tf_s20_gp_Lunshr_co_comb;
tf_s20_gp_unshr_co_comb.powspctrm = (tf_s20_gp_Lunshr_co_comb.powspctrm + tf_s20_gp_Runshr_co_comb.powspctrm)./2
save tf_s20_gp_unshr_co_comb tf_s20_gp_unshr_co_comb

clear all;

% %%
% %% _gp_Runshr_ip

D = spm_eeg_load ('rmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 4
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_gp_Runshr_ip = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_gp_Runshr_ip.grad = cfg.grad;


save tf_s20_gp_Runshr_ip tf_s20_gp_Runshr_ip

tf_s20_gp_Runshr_ip_check = checkdata(tf_s20_gp_Runshr_ip, 'senstype','neuromag306');
tf_s20_gp_Runshr_ip_check2 = checkdata(tf_s20_gp_Runshr_ip_check, 'datatype', 'freq');
tf_s20_gp_Runshr_ip_comb = ft_combineplanar(cfg, tf_s20_gp_Runshr_ip_check);

save tf_s20_gp_Runshr_ip_comb tf_s20_gp_Runshr_ip_comb
clear all;

% %% 
% %% _gp_Lunshr_ip

D = spm_eeg_load ('rmtf_Mprcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 3
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_gp_Lunshr_ip = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_gp_Lunshr_ip.grad = cfg.grad;


save tf_s20_gp_Lunshr_ip tf_s20_gp_Lunshr_ip

tf_s20_gp_Lunshr_ip_check = checkdata(tf_s20_gp_Lunshr_ip, 'senstype','neuromag306');
tf_s20_gp_Lunshr_ip_check2 = checkdata(tf_s20_gp_Lunshr_ip_check, 'datatype', 'freq');
tf_s20_gp_Lunshr_ip_comb = ft_combineplanar(cfg, tf_s20_gp_Lunshr_ip_check);

save tf_s20_gp_Lunshr_ip_comb tf_s20_gp_Lunshr_ip_comb
clear all;

% %% 
% %% 

close all
clear all

load tf_s20_gp_Lunshr_ip
load tf_s20_gp_Runshr_ip

tf_s20_gp_unshr_ip = tf_s20_gp_Lunshr_ip;
tf_s20_gp_unshr_ip.powspctrm = (tf_s20_gp_Lunshr_ip.powspctrm + tf_s20_gp_Runshr_ip.powspctrm)./2
save tf_s20_gp_unshr_ip tf_s20_gp_unshr_ip

clear all

load tf_s20_gp_Lunshr_ip_comb
load tf_s20_gp_Runshr_ip_comb

tf_s20_gp_unshr_ip_comb = tf_s20_gp_Lunshr_ip_comb;
tf_s20_gp_unshr_ip_comb.powspctrm = (tf_s20_gp_Lunshr_ip_comb.powspctrm + tf_s20_gp_Runshr_ip_comb.powspctrm)./2
save tf_s20_gp_unshr_ip_comb tf_s20_gp_unshr_ip_comb

clear all;



% % %% feature
% % %% _fr_Lshr_co

D = spm_eeg_load ('rmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 5
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_fr_Lshr_co = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_fr_Lshr_co.grad = cfg.grad;

save tf_s20_fr_Lshr_co tf_s20_fr_Lshr_co

tf_s20_fr_Lshr_co_check = checkdata(tf_s20_fr_Lshr_co, 'senstype','neuromag306');
tf_s20_fr_Lshr_co_check2 = checkdata(tf_s20_fr_Lshr_co_check, 'datatype', 'freq');
tf_s20_fr_Lshr_co_comb = ft_combineplanar(cfg, tf_s20_fr_Lshr_co_check);

save tf_s20_fr_Lshr_co_comb tf_s20_fr_Lshr_co_comb
clear all;

% %% 
% %% _fr_Rshr_co

D = spm_eeg_load ('rmtf_Mprcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 6
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_fr_Rshr_co = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_fr_Rshr_co.grad = cfg.grad;


save tf_s20_fr_Rshr_co tf_s20_fr_Rshr_co

tf_s20_fr_Rshr_co_check = checkdata(tf_s20_fr_Rshr_co, 'senstype','neuromag306');
tf_s20_fr_Rshr_co_check2 = checkdata(tf_s20_fr_Rshr_co_check, 'datatype', 'freq');
tf_s20_fr_Rshr_co_comb = ft_combineplanar(cfg, tf_s20_fr_Rshr_co_check);

save tf_s20_fr_Rshr_co_comb tf_s20_fr_Rshr_co_comb
clear all;

% %% 

close all
clear all

load tf_s20_fr_Lshr_co
load tf_s20_fr_Rshr_co

tf_s20_fr_shr_co = tf_s20_fr_Lshr_co;
tf_s20_fr_shr_co.powspctrm = (tf_s20_fr_Lshr_co.powspctrm + tf_s20_fr_Rshr_co.powspctrm)./2
save tf_s20_fr_shr_co tf_s20_fr_shr_co

clear all

load tf_s20_fr_Lshr_co_comb
load tf_s20_fr_Rshr_co_comb

tf_s20_fr_shr_co_comb = tf_s20_fr_Lshr_co_comb;
tf_s20_fr_shr_co_comb.powspctrm = (tf_s20_fr_Lshr_co_comb.powspctrm + tf_s20_fr_Rshr_co_comb.powspctrm)./2
save tf_s20_fr_shr_co_comb tf_s20_fr_shr_co_comb

clear all;

% %%
% %% _fr_Rshr_ip

D = spm_eeg_load ('rmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 6
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_fr_Rshr_ip = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_fr_Rshr_ip.grad = cfg.grad;


save tf_s20_fr_Rshr_ip tf_s20_fr_Rshr_ip

tf_s20_fr_Rshr_ip_check = checkdata(tf_s20_fr_Rshr_ip, 'senstype','neuromag306');
tf_s20_fr_Rshr_ip_check2 = checkdata(tf_s20_fr_Rshr_ip_check, 'datatype', 'freq');
tf_s20_fr_Rshr_ip_comb = ft_combineplanar(cfg, tf_s20_fr_Rshr_ip_check);

save tf_s20_fr_Rshr_ip_comb tf_s20_fr_Rshr_ip_comb
clear all;

% %% 
% %% _fr_Lshr_ip

D = spm_eeg_load ('rmtf_Mprcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 5
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_fr_Lshr_ip = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_fr_Lshr_ip.grad = cfg.grad;


save tf_s20_fr_Lshr_ip tf_s20_fr_Lshr_ip

tf_s20_fr_Lshr_ip_check = checkdata(tf_s20_fr_Lshr_ip, 'senstype','neuromag306');
tf_s20_fr_Lshr_ip_check2 = checkdata(tf_s20_fr_Lshr_ip_check, 'datatype', 'freq');
tf_s20_fr_Lshr_ip_comb = ft_combineplanar(cfg, tf_s20_fr_Lshr_ip_check);

save tf_s20_fr_Lshr_ip_comb tf_s20_fr_Lshr_ip_comb
clear all;

% %% 
% %% 

close all
clear all

load tf_s20_fr_Lshr_ip
load tf_s20_fr_Rshr_ip

tf_s20_fr_shr_ip = tf_s20_fr_Lshr_ip;
tf_s20_fr_shr_ip.powspctrm = (tf_s20_fr_Lshr_ip.powspctrm + tf_s20_fr_Rshr_ip.powspctrm)./2
save tf_s20_fr_shr_ip tf_s20_fr_shr_ip

clear all

load tf_s20_fr_Lshr_ip_comb
load tf_s20_fr_Rshr_ip_comb

tf_s20_fr_shr_ip_comb = tf_s20_fr_Lshr_ip_comb;
tf_s20_fr_shr_ip_comb.powspctrm = (tf_s20_fr_Lshr_ip_comb.powspctrm + tf_s20_fr_Rshr_ip_comb.powspctrm)./2
save tf_s20_fr_shr_ip_comb tf_s20_fr_shr_ip_comb

clear all;

% %%




% %% _fr_Lunshr_co

D = spm_eeg_load ('rmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 7
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_fr_Lunshr_co = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_fr_Lunshr_co.grad = cfg.grad;


save tf_s20_fr_Lunshr_co tf_s20_fr_Lunshr_co

tf_s20_fr_Lunshr_co_check = checkdata(tf_s20_fr_Lunshr_co, 'senstype','neuromag306');
tf_s20_fr_Lunshr_co_check2 = checkdata(tf_s20_fr_Lunshr_co_check, 'datatype', 'freq');
tf_s20_fr_Lunshr_co_comb = ft_combineplanar(cfg, tf_s20_fr_Lunshr_co_check);

save tf_s20_fr_Lunshr_co_comb tf_s20_fr_Lunshr_co_comb
clear all;

% %% 
% %% _fr_Runshr_co

D = spm_eeg_load ('rmtf_Mprcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 8
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_fr_Runshr_co = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_fr_Runshr_co.grad = cfg.grad;


save tf_s20_fr_Runshr_co tf_s20_fr_Runshr_co

tf_s20_fr_Runshr_co_check = checkdata(tf_s20_fr_Runshr_co, 'senstype','neuromag306');
tf_s20_fr_Runshr_co_check2 = checkdata(tf_s20_fr_Runshr_co_check, 'datatype', 'freq');
tf_s20_fr_Runshr_co_comb = ft_combineplanar(cfg, tf_s20_fr_Runshr_co_check);

save tf_s20_fr_Runshr_co_comb tf_s20_fr_Runshr_co_comb
clear all;

% %% 

close all
clear all

load tf_s20_fr_Lunshr_co
load tf_s20_fr_Runshr_co

tf_s20_fr_unshr_co = tf_s20_fr_Lunshr_co;
tf_s20_fr_unshr_co.powspctrm = (tf_s20_fr_Lunshr_co.powspctrm + tf_s20_fr_Runshr_co.powspctrm)./2
save tf_s20_fr_unshr_co tf_s20_fr_unshr_co

clear all

load tf_s20_fr_Lunshr_co_comb
load tf_s20_fr_Runshr_co_comb

tf_s20_fr_unshr_co_comb = tf_s20_fr_Lunshr_co_comb;
tf_s20_fr_unshr_co_comb.powspctrm = (tf_s20_fr_Lunshr_co_comb.powspctrm + tf_s20_fr_Runshr_co_comb.powspctrm)./2
save tf_s20_fr_unshr_co_comb tf_s20_fr_unshr_co_comb

clear all;

% %%
% %% _fr_Runshr_ip

D = spm_eeg_load ('rmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 8
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_fr_Runshr_ip = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_fr_Runshr_ip.grad = cfg.grad;


save tf_s20_fr_Runshr_ip tf_s20_fr_Runshr_ip

tf_s20_fr_Runshr_ip_check = checkdata(tf_s20_fr_Runshr_ip, 'senstype','neuromag306');
tf_s20_fr_Runshr_ip_check2 = checkdata(tf_s20_fr_Runshr_ip_check, 'datatype', 'freq');
tf_s20_fr_Runshr_ip_comb = ft_combineplanar(cfg, tf_s20_fr_Runshr_ip_check);

save tf_s20_fr_Runshr_ip_comb tf_s20_fr_Runshr_ip_comb
clear all;

% %% 
% %% _fr_Lunshr_ip

D = spm_eeg_load ('rmtf_Mprcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 7
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_fr_Lunshr_ip = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_fr_Lunshr_ip.grad = cfg.grad;


save tf_s20_fr_Lunshr_ip tf_s20_fr_Lunshr_ip

tf_s20_fr_Lunshr_ip_check = checkdata(tf_s20_fr_Lunshr_ip, 'senstype','neuromag306');
tf_s20_fr_Lunshr_ip_check2 = checkdata(tf_s20_fr_Lunshr_ip_check, 'datatype', 'freq');
tf_s20_fr_Lunshr_ip_comb = ft_combineplanar(cfg, tf_s20_fr_Lunshr_ip_check);

save tf_s20_fr_Lunshr_ip_comb tf_s20_fr_Lunshr_ip_comb
clear all;

% %% 
% %% 

close all
clear all

load tf_s20_fr_Lunshr_ip
load tf_s20_fr_Runshr_ip

tf_s20_fr_unshr_ip = tf_s20_fr_Lunshr_ip;
tf_s20_fr_unshr_ip.powspctrm = (tf_s20_fr_Lunshr_ip.powspctrm + tf_s20_fr_Runshr_ip.powspctrm)./2
save tf_s20_fr_unshr_ip tf_s20_fr_unshr_ip

clear all

load tf_s20_fr_Lunshr_ip_comb
load tf_s20_fr_Runshr_ip_comb

tf_s20_fr_unshr_ip_comb = tf_s20_fr_Lunshr_ip_comb;
tf_s20_fr_unshr_ip_comb.powspctrm = (tf_s20_fr_Lunshr_ip_comb.powspctrm + tf_s20_fr_Runshr_ip_comb.powspctrm)./2
save tf_s20_fr_unshr_ip_comb tf_s20_fr_unshr_ip_comb

clear all;

% %%


% %% sep
% %% _Lsep_co

D = spm_eeg_load ('rmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 9
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_Lsep_co = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_Lsep_co.grad = cfg.grad;


save tf_s20_Lsep_co tf_s20_Lsep_co

tf_s20_Lsep_co_check = checkdata(tf_s20_Lsep_co, 'senstype','neuromag306');
tf_s20_Lsep_co_check2 = checkdata(tf_s20_Lsep_co_check, 'datatype', 'freq');
tf_s20_Lsep_co_comb = ft_combineplanar(cfg, tf_s20_Lsep_co_check);

save tf_s20_Lsep_co_comb tf_s20_Lsep_co_comb
clear all;

% %% 
% %% _Rsep_co

D = spm_eeg_load ('rmtf_Mprcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 10
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_Rsep_co = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_Rsep_co.grad = cfg.grad;


save tf_s20_Rsep_co tf_s20_Rsep_co

tf_s20_Rsep_co_check = checkdata(tf_s20_Rsep_co, 'senstype','neuromag306');
tf_s20_Rsep_co_check2 = checkdata(tf_s20_Rsep_co_check, 'datatype', 'freq');
tf_s20_Rsep_co_comb = ft_combineplanar(cfg, tf_s20_Rsep_co_check);

save tf_s20_Rsep_co_comb tf_s20_Rsep_co_comb
clear all;

% %% 

close all
clear all

load tf_s20_Lsep_co
load tf_s20_Rsep_co

tf_s20_sep_co = tf_s20_Lsep_co;
tf_s20_sep_co.powspctrm = (tf_s20_Lsep_co.powspctrm + tf_s20_Rsep_co.powspctrm)./2
save tf_s20_sep_co tf_s20_sep_co

clear all

load tf_s20_Lsep_co_comb
load tf_s20_Rsep_co_comb

tf_s20_sep_co_comb = tf_s20_Lsep_co_comb;
tf_s20_sep_co_comb.powspctrm = (tf_s20_Lsep_co_comb.powspctrm + tf_s20_Rsep_co_comb.powspctrm)./2
save tf_s20_sep_co_comb tf_s20_sep_co_comb

clear all;

% %%
% %% _Rsep_ip

D = spm_eeg_load ('rmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 10
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_Rsep_ip = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_Rsep_ip.grad = cfg.grad;


save tf_s20_Rsep_ip tf_s20_Rsep_ip

tf_s20_Rsep_ip_check = checkdata(tf_s20_Rsep_ip, 'senstype','neuromag306');
tf_s20_Rsep_ip_check2 = checkdata(tf_s20_Rsep_ip_check, 'datatype', 'freq');
tf_s20_Rsep_ip_comb = ft_combineplanar(cfg, tf_s20_Rsep_ip_check);

save tf_s20_Rsep_ip_comb tf_s20_Rsep_ip_comb
clear all;

% %% 
% %% _Lsep_ip

D = spm_eeg_load ('rmtf_Mprcpbefdspm8_run1raw_trans_sss.mat');

%%%%%%%%%%%%%%%%%%%%%%%%
ind = 9
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

tf_s20_Lsep_ip = data;
%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

tf_s20_Lsep_ip.grad = cfg.grad;


save tf_s20_Lsep_ip tf_s20_Lsep_ip

tf_s20_Lsep_ip_check = checkdata(tf_s20_Lsep_ip, 'senstype','neuromag306');
tf_s20_Lsep_ip_check2 = checkdata(tf_s20_Lsep_ip_check, 'datatype', 'freq');
tf_s20_Lsep_ip_comb = ft_combineplanar(cfg, tf_s20_Lsep_ip_check);

save tf_s20_Lsep_ip_comb tf_s20_Lsep_ip_comb
clear all;

% %% 
% %% 

close all
clear all

load tf_s20_Lsep_ip
load tf_s20_Rsep_ip

tf_s20_sep_ip = tf_s20_Lsep_ip;
tf_s20_sep_ip.powspctrm = (tf_s20_Lsep_ip.powspctrm + tf_s20_Rsep_ip.powspctrm)./2
save tf_s20_sep_ip tf_s20_sep_ip

clear all

load tf_s20_Lsep_ip_comb
load tf_s20_Rsep_ip_comb

tf_s20_sep_ip_comb = tf_s20_Lsep_ip_comb;
tf_s20_sep_ip_comb.powspctrm = (tf_s20_Lsep_ip_comb.powspctrm + tf_s20_Rsep_ip_comb.powspctrm)./2
save tf_s20_sep_ip_comb tf_s20_sep_ip_comb

clear all;

% %%






% %% 
's20 done'