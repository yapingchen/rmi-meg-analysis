close all
clear all

cd 'F:\2018_RMI_gamma_scale_x' % where the data is

% rmpath(genpath('D:\0_fMRI analysis\spm8_meeg\'))
% rmpath(genpath('D:\0_EEG analysis\fieldtrip\'))
% addpath(genpath('D:\0_EEG analysis\fieldtrip-20140611\'))

%% cluster-based permutation test
close all
clear all

load tf_gp_shr_co_comb_avg
load tf_gp_shr_ip_comb_avg


cfg = [];
cfg.method      = 'distance'; % try 'template' and 'distance'
cfg.template    = 'D:\0_MEEG_toolbox\fieldtrip\template\neighbours\neuromag306cmb_neighb.mat';               % specify type of template
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';   
cfg.feedback    = 'no';                             % show a neighbour plot 
neighbours      = ft_prepare_neighbours(cfg, tf_gp_shr_co_comb_avg); % define neighbouring channels

cfg = [];
cfg.channel = {'MEG'};
%cfg.latency  = [0.276 0.436]; % alpha (8-12)
% cfg.latency  = [0.248 0.496]; % alpha (8-14); minnbchan = 3
%cfg.latency  = [0.4 0.8]; % theta 0.4-0.8; minnbchan = 5
%cfg.frequency = [3 7]; % theta
% cfg.latency  = [0.3 0.5]; % gamma
cfg.latency  = [0.45 0.6]; % gamma [0.3 0.7]
cfg.frequency = [30 58]; % gamma [30 58]

cfg.avgoverfreq      ='yes';
% cfg.avgovertime      ='yes';
cfg.method = 'montecarlo';
cfg.statistic = 'ft_statfun_depsamplesT';
cfg.correctm = 'cluster';
cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.minnbchan = 3;
cfg.neighbours = neighbours;
cfg.tail = 0;
cfg.clustertail = 0;
cfg.alpha = 0.05;
cfg.numrandomization = 500;

subj = 18;
design = zeros(2,2*subj);
for i = 1:subj
  design(1,i) = i;
end
for i = 1:subj
  design(1,subj+i) = i;
end
design(2,1:subj)        = 1;
design(2,subj+1:2*subj) = 2;

cfg.design = design;
cfg.uvar  = 1;
cfg.ivar  = 2;

[stat] = ft_freqstatistics(cfg, tf_gp_shr_co_comb_avg, tf_gp_shr_ip_comb_avg)
min(min(min(stat.prob)))
% save stat_tfcomb_ftshr_ftunshr_gamma stat
a = squeeze(stat.prob);
%%
% load stat_tfcomb_ftshr_ftunshr_alpha stat
%
cfg = [];
cfg.highlightsymbolseries = ['*','+','.','.','.'];
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';   
% cfg.colorbar = 'yes';
cfg.interactive = 'yes';
cfg.contournum = 0;
cfg.markersymbol = '.';
cfg.alpha = 0.05;
cfg.zlim = [-5 5];
cfg.parameter = 'stat';
ft_clusterplot(cfg,stat);
% saveas (gcf, 'ftshr_gpunshr.tif')

%% 
%%
% load stat_tfcomb_ftshr_ftunshr_alpha stat
%
stat.cfg.alpha = 0.1;
pvals = stat.prob;
psig = find(pvals < stat.cfg.alpha); 

cfg = [];
cfg.highlight = 'on';
cfg.highlightsymbol = '+';
cfg.highlightchannel = psig;
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';   
% cfg.colorbar = 'yes';
cfg.comment = 'no';
cfg.interactive = 'no';
cfg.contournum = 0;
cfg.marker = 'off';
cfg.markersymbol = '.';
cfg.zlim = [-5 5];
cfg.parameter = 'stat';

ft_topoplotTFR(cfg,stat);
% saveas (gcf, 'ftshr_gpunshr.tif')


