close all
clear all


%% gp
%% 
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['load tfdiff_s' aa '_gp_shr_co_ip.mat']);
    eval(['load tfdiff_s' aa '_gp_unshr_co_ip.mat']);
end

cd F:\2018_RMI_gamma\coip;
tfdiff_s02_gp_co_ip = tfdiff_s02_gp_shr_co_ip;
tfdiff_s02_gp_co_ip.powspctrm = (tfdiff_s02_gp_shr_co_ip.powspctrm + tfdiff_s02_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s02_gp_co_ip tfdiff_s02_gp_co_ip

tfdiff_s03_gp_co_ip = tfdiff_s03_gp_shr_co_ip;
tfdiff_s03_gp_co_ip.powspctrm = (tfdiff_s03_gp_shr_co_ip.powspctrm + tfdiff_s03_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s03_gp_co_ip tfdiff_s03_gp_co_ip

tfdiff_s04_gp_co_ip = tfdiff_s04_gp_shr_co_ip;
tfdiff_s04_gp_co_ip.powspctrm = (tfdiff_s04_gp_shr_co_ip.powspctrm + tfdiff_s04_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s04_gp_co_ip tfdiff_s04_gp_co_ip

tfdiff_s05_gp_co_ip = tfdiff_s05_gp_shr_co_ip;
tfdiff_s05_gp_co_ip.powspctrm = (tfdiff_s05_gp_shr_co_ip.powspctrm + tfdiff_s05_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s05_gp_co_ip tfdiff_s05_gp_co_ip

tfdiff_s06_gp_co_ip = tfdiff_s06_gp_shr_co_ip;
tfdiff_s06_gp_co_ip.powspctrm = (tfdiff_s06_gp_shr_co_ip.powspctrm + tfdiff_s06_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s06_gp_co_ip tfdiff_s06_gp_co_ip

tfdiff_s08_gp_co_ip = tfdiff_s08_gp_shr_co_ip;
tfdiff_s08_gp_co_ip.powspctrm = (tfdiff_s08_gp_shr_co_ip.powspctrm + tfdiff_s08_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s08_gp_co_ip tfdiff_s08_gp_co_ip

tfdiff_s09_gp_co_ip = tfdiff_s09_gp_shr_co_ip;
tfdiff_s09_gp_co_ip.powspctrm = (tfdiff_s09_gp_shr_co_ip.powspctrm + tfdiff_s09_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s09_gp_co_ip tfdiff_s09_gp_co_ip

tfdiff_s10_gp_co_ip = tfdiff_s10_gp_shr_co_ip;
tfdiff_s10_gp_co_ip.powspctrm = (tfdiff_s10_gp_shr_co_ip.powspctrm + tfdiff_s10_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s10_gp_co_ip tfdiff_s10_gp_co_ip

tfdiff_s11_gp_co_ip = tfdiff_s11_gp_shr_co_ip;
tfdiff_s11_gp_co_ip.powspctrm = (tfdiff_s11_gp_shr_co_ip.powspctrm + tfdiff_s11_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s11_gp_co_ip tfdiff_s11_gp_co_ip

tfdiff_s12_gp_co_ip = tfdiff_s12_gp_shr_co_ip;
tfdiff_s12_gp_co_ip.powspctrm = (tfdiff_s12_gp_shr_co_ip.powspctrm + tfdiff_s12_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s12_gp_co_ip tfdiff_s12_gp_co_ip

tfdiff_s13_gp_co_ip = tfdiff_s13_gp_shr_co_ip;
tfdiff_s13_gp_co_ip.powspctrm = (tfdiff_s13_gp_shr_co_ip.powspctrm + tfdiff_s13_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s13_gp_co_ip tfdiff_s13_gp_co_ip

tfdiff_s14_gp_co_ip = tfdiff_s14_gp_shr_co_ip;
tfdiff_s14_gp_co_ip.powspctrm = (tfdiff_s14_gp_shr_co_ip.powspctrm + tfdiff_s14_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s14_gp_co_ip tfdiff_s14_gp_co_ip

tfdiff_s15_gp_co_ip = tfdiff_s15_gp_shr_co_ip;
tfdiff_s15_gp_co_ip.powspctrm = (tfdiff_s15_gp_shr_co_ip.powspctrm + tfdiff_s15_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s15_gp_co_ip tfdiff_s15_gp_co_ip

tfdiff_s16_gp_co_ip = tfdiff_s16_gp_shr_co_ip;
tfdiff_s16_gp_co_ip.powspctrm = (tfdiff_s16_gp_shr_co_ip.powspctrm + tfdiff_s16_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s16_gp_co_ip tfdiff_s16_gp_co_ip

tfdiff_s17_gp_co_ip = tfdiff_s17_gp_shr_co_ip;
tfdiff_s17_gp_co_ip.powspctrm = (tfdiff_s17_gp_shr_co_ip.powspctrm + tfdiff_s17_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s17_gp_co_ip tfdiff_s17_gp_co_ip

tfdiff_s18_gp_co_ip = tfdiff_s18_gp_shr_co_ip;
tfdiff_s18_gp_co_ip.powspctrm = (tfdiff_s18_gp_shr_co_ip.powspctrm + tfdiff_s18_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s18_gp_co_ip tfdiff_s18_gp_co_ip

tfdiff_s19_gp_co_ip = tfdiff_s19_gp_shr_co_ip;
tfdiff_s19_gp_co_ip.powspctrm = (tfdiff_s19_gp_shr_co_ip.powspctrm + tfdiff_s19_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s19_gp_co_ip tfdiff_s19_gp_co_ip

tfdiff_s20_gp_co_ip = tfdiff_s20_gp_shr_co_ip;
tfdiff_s20_gp_co_ip.powspctrm = (tfdiff_s20_gp_shr_co_ip.powspctrm + tfdiff_s20_gp_unshr_co_ip.powspctrm)/2;
save tfdiff_s20_gp_co_ip tfdiff_s20_gp_co_ip

cfg = [];
cfg.keepindividual = 'yes' ;
tfdiff_gp_co_ip_avg = ft_freqgrandaverage(cfg, tfdiff_s02_gp_co_ip, tfdiff_s03_gp_co_ip, tfdiff_s04_gp_co_ip, tfdiff_s05_gp_co_ip, tfdiff_s06_gp_co_ip, tfdiff_s08_gp_co_ip, tfdiff_s09_gp_co_ip, tfdiff_s10_gp_co_ip, tfdiff_s11_gp_co_ip, tfdiff_s12_gp_co_ip, tfdiff_s13_gp_co_ip, tfdiff_s14_gp_co_ip, tfdiff_s15_gp_co_ip, tfdiff_s16_gp_co_ip, tfdiff_s17_gp_co_ip, tfdiff_s18_gp_co_ip, tfdiff_s19_gp_co_ip, tfdiff_s20_gp_co_ip)
save tfdiff_gp_co_ip_avg tfdiff_gp_co_ip_avg




%%

%% fr
%% 
clear all
cd F:\2018_RMI_gamma\coip;
for k = [2:6 8:20]
    aa = sprintf('%02d',k);
    eval(['load tfdiff_s' aa '_fr_shr_co_ip.mat']);
    eval(['load tfdiff_s' aa '_fr_unshr_co_ip.mat']);
end

cd F:\2018_RMI_gamma\coip;
tfdiff_s02_fr_co_ip = tfdiff_s02_fr_shr_co_ip;
tfdiff_s02_fr_co_ip.powspctrm = (tfdiff_s02_fr_shr_co_ip.powspctrm + tfdiff_s02_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s02_fr_co_ip tfdiff_s02_fr_co_ip

tfdiff_s03_fr_co_ip = tfdiff_s03_fr_shr_co_ip;
tfdiff_s03_fr_co_ip.powspctrm = (tfdiff_s03_fr_shr_co_ip.powspctrm + tfdiff_s03_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s03_fr_co_ip tfdiff_s03_fr_co_ip

tfdiff_s04_fr_co_ip = tfdiff_s04_fr_shr_co_ip;
tfdiff_s04_fr_co_ip.powspctrm = (tfdiff_s04_fr_shr_co_ip.powspctrm + tfdiff_s04_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s04_fr_co_ip tfdiff_s04_fr_co_ip

tfdiff_s05_fr_co_ip = tfdiff_s05_fr_shr_co_ip;
tfdiff_s05_fr_co_ip.powspctrm = (tfdiff_s05_fr_shr_co_ip.powspctrm + tfdiff_s05_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s05_fr_co_ip tfdiff_s05_fr_co_ip

tfdiff_s06_fr_co_ip = tfdiff_s06_fr_shr_co_ip;
tfdiff_s06_fr_co_ip.powspctrm = (tfdiff_s06_fr_shr_co_ip.powspctrm + tfdiff_s06_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s06_fr_co_ip tfdiff_s06_fr_co_ip

tfdiff_s08_fr_co_ip = tfdiff_s08_fr_shr_co_ip;
tfdiff_s08_fr_co_ip.powspctrm = (tfdiff_s08_fr_shr_co_ip.powspctrm + tfdiff_s08_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s08_fr_co_ip tfdiff_s08_fr_co_ip

tfdiff_s09_fr_co_ip = tfdiff_s09_fr_shr_co_ip;
tfdiff_s09_fr_co_ip.powspctrm = (tfdiff_s09_fr_shr_co_ip.powspctrm + tfdiff_s09_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s09_fr_co_ip tfdiff_s09_fr_co_ip

tfdiff_s10_fr_co_ip = tfdiff_s10_fr_shr_co_ip;
tfdiff_s10_fr_co_ip.powspctrm = (tfdiff_s10_fr_shr_co_ip.powspctrm + tfdiff_s10_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s10_fr_co_ip tfdiff_s10_fr_co_ip

tfdiff_s11_fr_co_ip = tfdiff_s11_fr_shr_co_ip;
tfdiff_s11_fr_co_ip.powspctrm = (tfdiff_s11_fr_shr_co_ip.powspctrm + tfdiff_s11_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s11_fr_co_ip tfdiff_s11_fr_co_ip

tfdiff_s12_fr_co_ip = tfdiff_s12_fr_shr_co_ip;
tfdiff_s12_fr_co_ip.powspctrm = (tfdiff_s12_fr_shr_co_ip.powspctrm + tfdiff_s12_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s12_fr_co_ip tfdiff_s12_fr_co_ip

tfdiff_s13_fr_co_ip = tfdiff_s13_fr_shr_co_ip;
tfdiff_s13_fr_co_ip.powspctrm = (tfdiff_s13_fr_shr_co_ip.powspctrm + tfdiff_s13_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s13_fr_co_ip tfdiff_s13_fr_co_ip

tfdiff_s14_fr_co_ip = tfdiff_s14_fr_shr_co_ip;
tfdiff_s14_fr_co_ip.powspctrm = (tfdiff_s14_fr_shr_co_ip.powspctrm + tfdiff_s14_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s14_fr_co_ip tfdiff_s14_fr_co_ip

tfdiff_s15_fr_co_ip = tfdiff_s15_fr_shr_co_ip;
tfdiff_s15_fr_co_ip.powspctrm = (tfdiff_s15_fr_shr_co_ip.powspctrm + tfdiff_s15_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s15_fr_co_ip tfdiff_s15_fr_co_ip

tfdiff_s16_fr_co_ip = tfdiff_s16_fr_shr_co_ip;
tfdiff_s16_fr_co_ip.powspctrm = (tfdiff_s16_fr_shr_co_ip.powspctrm + tfdiff_s16_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s16_fr_co_ip tfdiff_s16_fr_co_ip

tfdiff_s17_fr_co_ip = tfdiff_s17_fr_shr_co_ip;
tfdiff_s17_fr_co_ip.powspctrm = (tfdiff_s17_fr_shr_co_ip.powspctrm + tfdiff_s17_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s17_fr_co_ip tfdiff_s17_fr_co_ip

tfdiff_s18_fr_co_ip = tfdiff_s18_fr_shr_co_ip;
tfdiff_s18_fr_co_ip.powspctrm = (tfdiff_s18_fr_shr_co_ip.powspctrm + tfdiff_s18_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s18_fr_co_ip tfdiff_s18_fr_co_ip

tfdiff_s19_fr_co_ip = tfdiff_s19_fr_shr_co_ip;
tfdiff_s19_fr_co_ip.powspctrm = (tfdiff_s19_fr_shr_co_ip.powspctrm + tfdiff_s19_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s19_fr_co_ip tfdiff_s19_fr_co_ip

tfdiff_s20_fr_co_ip = tfdiff_s20_fr_shr_co_ip;
tfdiff_s20_fr_co_ip.powspctrm = (tfdiff_s20_fr_shr_co_ip.powspctrm + tfdiff_s20_fr_unshr_co_ip.powspctrm)/2;
save tfdiff_s20_fr_co_ip tfdiff_s20_fr_co_ip

cfg = [];
cfg.keepindividual = 'yes' ;
tfdiff_fr_co_ip_avg = ft_freqgrandaverage(cfg, tfdiff_s02_fr_co_ip, tfdiff_s03_fr_co_ip, tfdiff_s04_fr_co_ip, tfdiff_s05_fr_co_ip, tfdiff_s06_fr_co_ip, tfdiff_s08_fr_co_ip, tfdiff_s09_fr_co_ip, tfdiff_s10_fr_co_ip, tfdiff_s11_fr_co_ip, tfdiff_s12_fr_co_ip, tfdiff_s13_fr_co_ip, tfdiff_s14_fr_co_ip, tfdiff_s15_fr_co_ip, tfdiff_s16_fr_co_ip, tfdiff_s17_fr_co_ip, tfdiff_s18_fr_co_ip, tfdiff_s19_fr_co_ip, tfdiff_s20_fr_co_ip)
save tfdiff_fr_co_ip_avg tfdiff_fr_co_ip_avg

%% plotting

%%
rmpath(genpath('D:\0_fMRI analysis\spm8_meeg\'))
rmpath(genpath('D:\0_EEG analysis\fieldtrip\'))
addpath(genpath('D:\0_EEG analysis\fieldtrip-20140611\'))
rmpath(genpath('D:\0_EEG analysis\fieldtrip-20160506\'))

%%
close all
clear all

load tfdiff_fr_co_ip_avg
load tfdiff_fr_unshr_co_ip_avg
load tfdiff_fr_shr_co_ip_avg
load tfdiff_fr_unshr_co_ip_avg
load tfdiff_sep_co_ip_avg

cfg.colorbar = 'yes';
cfg.interactive = 'yes';
cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';   
cfg.xlim = [-0.2 0.8]; % time
% cfg.ylim = [1 20];
% cfg.zlim = [-2 2];

figure(1)
ft_multiplotTFR(cfg, tfdiff_fr_co_ip_avg); 

figure(2)
ft_multiplotTFR(cfg, tfdiff_fr_unshr_co_ip_avg); 

figure(3)
ft_multiplotTFR(cfg, tfdiff_fr_shr_co_ip_avg); 

figure(4)
ft_multiplotTFR(cfg, tfdiff_fr_unshr_co_ip_avg); 

figure(5)
ft_multiplotTFR(cfg, tfdiff_sep_co_ip_avg); 


%%
close all
clear all

load tfdiff_fr_co_ip_avg
load tfdiff_fr_unshr_co_ip_avg
load tfdiff_fr_shr_co_ip_avg
load tfdiff_fr_unshr_co_ip_avg
load tfdiff_sep_co_ip_avg

cfg.colorbar = 'yes';
cfg.interactive = 'yes';
cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306mag.lay';   
cfg.xlim = [-0.2 0.8]; % time
cfg.ylim = [30 58];
% cfg.zlim = [-1.5 1.5];

figure(1)
ft_multiplotTFR(cfg, tfdiff_fr_co_ip_avg); 

figure(2)
ft_multiplotTFR(cfg, tfdiff_fr_unshr_co_ip_avg); 

figure(3)
ft_multiplotTFR(cfg, tfdiff_fr_shr_co_ip_avg); 

figure(4)
ft_multiplotTFR(cfg, tfdiff_fr_unshr_co_ip_avg); 

figure(5)
ft_multiplotTFR(cfg, tfdiff_sep_co_ip_avg); 
%%







