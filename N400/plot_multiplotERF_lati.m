close all;
clear all;
cd F:\2017_RMI_MEG\N400_cell

load erf_ga_gpsh_data
load erf_ga_gpuns_data

cfg.xlim = [-0.1 0.5];
% cfg.ylim = [-3e-12 3e-12]; %'maxmin'; %[-3e-12 2e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay'; 
% cfg.channel = {'MEG2322+2323','MEG2432+2433','MEG2512+2513','MEG2522+2523','MEG2532+2533','MEG2542+2543'}; %  {'MEG2322','MEG2432','MEG2513','MEG2523','MEG2532','MEG2542'};
cfg.linewidth = 1.5;
figure;
% ft_multiplotER(cfg, gpsh, gpuns, singsh, singuns, sep);
ft_multiplotER(cfg, erf_ga_gpsh_data, erf_ga_gpuns_data);
% legend({'gpsh','gpuns','singsh','singuns','sep'});
legend({'gpsh','gpuns'});
% title('grouped shared unshared single shared unshared separated');
title('grouped, shared vs unshared lati');

%%
% close all;
% clear all;
cd F:\2017_RMI_MEG\N400_cell

load erf_ga_gpsh_data
load erf_ga_singsh_data

cfg.xlim = [-0.1 0.5];
% cfg.ylim = [-3e-12 3e-12]; %'maxmin'; %[-3e-12 2e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay'; 
% cfg.channel = {'MEG2322+2323','MEG2432+2433','MEG2512+2513','MEG2522+2523','MEG2532+2533','MEG2542+2543'}; %  {'MEG2322','MEG2432','MEG2513','MEG2523','MEG2532','MEG2542'};
cfg.linewidth = 1.5;
figure;
% ft_multiplotER(cfg, gpsh, gpuns, singsh, singuns, sep);
ft_multiplotER(cfg, erf_ga_gpsh_data, erf_ga_singsh_data);
% legend({'gpsh','gpuns','singsh','singuns','sep'});
legend({'gpsh','singsh'});
% title('grouped shared unshared single shared unshared separated');
title('shared, grouped vs single lati');

%%
close all;
clear all;
cd F:\2017_RMI_MEG\N2pc_gpsing\coip;

load erf_singsh_co_ga_data
load erf_singsh_ip_ga_data
load erf_singuns_co_ga_data
load erf_singuns_ip_ga_data

cfg.xlim = [-0.1 0.5];
% cfg.ylim = [0 5e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';
% cfg.channel = {'MEG2322+2323','MEG2432+2433','MEG2512+2513','MEG2522+2523','MEG2532+2533','MEG2542+2543'};
cfg.linewidth = 1.5;
% cfg.graphcolor = [1 0 0;1 0.5 0;0.5 1 0;0 0.2 0;0 0.5 1;0 0 1;0.5 0 1;0 0 0];
figure;
ft_multiplotER(cfg, erf_singsh_co_ga_data, erf_singsh_ip_ga_data, erf_singuns_co_ga_data, erf_singuns_ip_ga_data); %
legend({'single-sh co','single-sh ip','single-uns co','single-uns ip'});
title('single shared unshared');

% set(gca,'YTick',0:1e-12:5e-12,'fontsize',10)

%%
close all;
clear all;
cd F:\2017_RMI_MEG\N2pc_gpsing\coip;

load erf_gpsh_co_ga_data
load erf_singsh_co_ga_data

cfg.xlim = [-0.2 0.6];
% cfg.ylim = [-3e-12 3e-12]; %'maxmin'; %[-3e-12 2e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';
cfg.linewidth = 1.5;
figure;
ft_multiplotER(cfg, erf_gpsh_co_ga_data, erf_singsh_co_ga_data);
% ft_multiplotER(cfg, gpsh, gpuns, singsh, singuns);
legend({'gpsh','gpuns'});
% legend({'gpsh','gpuns','singsh','singuns'});
title('grouped shared unshared');
% title('grouped shared unshared single shared unshared');


