clear all
for k = [2:20]
    aa = sprintf('%02d',k);

eval(['cd F:\2017_RMI_MEG\N400_cell\sub' aa ]);

%%% singsh
D = spm_eeg_load('pwmfrcpbefdspm8_run.mat');
eval(['erf_rmi' aa '_singsh = D.ftraw(0)']);
if D.ntrials > 1
    clb = D.conditions
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|',
    %clb{:}),1:D.ntrials)
    ind = 1
    eval(['erf_rmi' aa '_singsh.trial = erf_rmi' aa '_singsh.trial(ind)']);
    eval(['erf_rmi' aa '_singsh.time =  erf_rmi' aa '_singsh.time(ind)']);
end

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);
eval(['erf_rmi' aa '_singsh.grad = cfg.grad']);
eval(['erf_rmi' aa '_singsh_data = ft_timelockanalysis([], erf_rmi' aa '_singsh)']);
eval(['save erf_rmi' aa '_singsh_data erf_rmi' aa '_singsh_data']);

eval(['erf_rmi' aa '_singsh_check = checkdata(erf_rmi' aa '_singsh_data, ''senstype'',''neuromag306'')']);
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
eval(['erf_rmi' aa '_singsh_comb = ft_combineplanar(cfg, erf_rmi' aa '_singsh_check)']);
eval(['save erf_rmi' aa '_singsh_comb erf_rmi' aa '_singsh_comb']);


%%% gpsh
D = spm_eeg_load('pwmfrcpbefdspm8_run.mat');
eval(['erf_rmi' aa '_gpsh = D.ftraw(0)']);
if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|',
    %clb{:}),1:D.ntrials)
    ind = 2
    eval(['erf_rmi' aa '_gpsh.trial = erf_rmi' aa '_gpsh.trial(ind)']);
    eval(['erf_rmi' aa '_gpsh.time =  erf_rmi' aa '_gpsh.time(ind)']);
end

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);
eval(['erf_rmi' aa '_gpsh.grad = cfg.grad']);
eval(['erf_rmi' aa '_gpsh_data = ft_timelockanalysis([], erf_rmi' aa '_gpsh)']);
eval(['save erf_rmi' aa '_gpsh_data erf_rmi' aa '_gpsh_data']);

eval(['erf_rmi' aa '_gpsh_check = checkdata(erf_rmi' aa '_gpsh_data, ''senstype'',''neuromag306'')']);
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
eval(['erf_rmi' aa '_gpsh_comb = ft_combineplanar(cfg, erf_rmi' aa '_gpsh_check)']);
eval(['save erf_rmi' aa '_gpsh_comb erf_rmi' aa '_gpsh_comb']);


%%% singuns
D = spm_eeg_load('pwmfrcpbefdspm8_run.mat');
eval(['erf_rmi' aa '_singuns = D.ftraw(0)']);
if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|',
    %clb{:}),1:D.ntrials)
    ind = 3
    eval(['erf_rmi' aa '_singuns.trial = erf_rmi' aa '_singuns.trial(ind)']);
    eval(['erf_rmi' aa '_singuns.time =  erf_rmi' aa '_singuns.time(ind)']);
end

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);
eval(['erf_rmi' aa '_singuns.grad = cfg.grad']);
eval(['erf_rmi' aa '_singuns_data = ft_timelockanalysis([], erf_rmi' aa '_singuns)']);
eval(['save erf_rmi' aa '_singuns_data erf_rmi' aa '_singuns_data']);

eval(['erf_rmi' aa '_singuns_check = checkdata(erf_rmi' aa '_singuns_data, ''senstype'',''neuromag306'')']);
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
eval(['erf_rmi' aa '_singuns_comb = ft_combineplanar(cfg, erf_rmi' aa '_singuns_check)']);
eval(['save erf_rmi' aa '_singuns_comb erf_rmi' aa '_singuns_comb']);


%%% gpuns
D = spm_eeg_load('pwmfrcpbefdspm8_run.mat');
eval(['erf_rmi' aa '_gpuns = D.ftraw(0)']);
if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|',
    %clb{:}),1:D.ntrials)
    ind = 4
    eval(['erf_rmi' aa '_gpuns.trial = erf_rmi' aa '_gpuns.trial(ind)']);
    eval(['erf_rmi' aa '_gpuns.time =  erf_rmi' aa '_gpuns.time(ind)']);
end

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);
eval(['erf_rmi' aa '_gpuns.grad = cfg.grad']);
eval(['erf_rmi' aa '_gpuns_data = ft_timelockanalysis([], erf_rmi' aa '_gpuns)']);
eval(['save erf_rmi' aa '_gpuns_data erf_rmi' aa '_gpuns_data']);

eval(['erf_rmi' aa '_gpuns_check = checkdata(erf_rmi' aa '_gpuns_data, ''senstype'',''neuromag306'')']);
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
eval(['erf_rmi' aa '_gpuns_comb = ft_combineplanar(cfg, erf_rmi' aa '_gpuns_check)']);
eval(['save erf_rmi' aa '_gpuns_comb erf_rmi' aa '_gpuns_comb']);


%%% separated
D = spm_eeg_load('pwmfrcpbefdspm8_run.mat');
eval(['erf_rmi' aa '_sep = D.ftraw(0)']);
if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|',
    %clb{:}),1:D.ntrials)
    ind = 5
    eval(['erf_rmi' aa '_sep.trial = erf_rmi' aa '_sep.trial(ind)']);
    eval(['erf_rmi' aa '_sep.time =  erf_rmi' aa '_sep.time(ind)']);
end

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);
eval(['erf_rmi' aa '_sep.grad = cfg.grad']);
eval(['erf_rmi' aa '_sep_data = ft_timelockanalysis([], erf_rmi' aa '_sep)']);
eval(['save erf_rmi' aa '_sep_data erf_rmi' aa '_sep_data']);

eval(['erf_rmi' aa '_sep_check = checkdata(erf_rmi' aa '_sep_data, ''senstype'',''neuromag306'')']);
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
eval(['erf_rmi' aa '_sep_comb = ft_combineplanar(cfg, erf_rmi' aa '_sep_check)']);
eval(['save erf_rmi' aa '_sep_comb erf_rmi' aa '_sep_comb']);
end