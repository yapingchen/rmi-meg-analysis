% close all;
% clear all;
cd F:\2017_RMI_MEG\N400_cell

load erf_ga_gpsh_comb
load erf_ga_gpuns_comb

cfg.xlim = [-0.1 0.5];
% cfg.ylim = [-3e-12 3e-12]; %'maxmin'; %[-3e-12 2e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay'; 
% cfg.channel = {'MEG2322+2323','MEG2432+2433','MEG2512+2513','MEG2522+2523','MEG2532+2533','MEG2542+2543'}; %  {'MEG2322','MEG2432','MEG2513','MEG2523','MEG2532','MEG2542'};
cfg.linewidth = 1.5;
figure;
ft_multiplotER(cfg, erf_ga_gpsh_comb, erf_ga_gpuns_comb);
% ft_multiplotER(cfg, gpsh, gpuns, singsh, singuns);
legend({'gpsh','gpuns'});
% legend({'gpsh','gpuns','singsh','singuns'});
title('grouped, shared vs unshared comb');
% title('grouped shared unshared single shared unshared');

%%
close all;
clear all;
cd F:\2017_RMI_MEG\N2pc_gpsing\coip;

load erf_gpsh_co_ga_comb
load erf_gpsh_ip_ga_comb
load erf_gpuns_co_ga_comb
load erf_gpuns_ip_ga_comb


cfg.xlim = [-0.1 0.5];
% cfg.ylim = [0 5e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';
% cfg.channel = {'MEG2322+2323','MEG2432+2433','MEG2512+2513','MEG2522+2523','MEG2532+2533','MEG2542+2543'};
cfg.linewidth = 1.5;
% cfg.graphcolor = [1 0 0;1 0.5 0;0.5 1 0;0 0.2 0;0 0.5 1;0 0 1;0.5 0 1;0 0 0];
figure;
ft_multiplotER(cfg, erf_gpsh_co_ga_comb, erf_gpsh_ip_ga_comb, erf_gpuns_co_ga_comb, erf_gpuns_ip_ga_comb);
legend({'gp-sh co','gp-sh ip','gp-uns co','gp-uns ip'});
title('grouped shared unshared');

%%
close all;
clear all;
cd F:\2017_RMI_MEG\N2pc_gpsing\coip;

load erf_singsh_co_ga_comb
load erf_singsh_ip_ga_comb
load erf_singuns_co_ga_comb
load erf_singuns_ip_ga_comb

cfg.xlim = [-0.1 0.5];
% cfg.ylim = [0 5e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';
% cfg.channel = {'MEG2322+2323','MEG2432+2433','MEG2512+2513','MEG2522+2523','MEG2532+2533','MEG2542+2543'};
cfg.linewidth = 1.5;
% cfg.graphcolor = [1 0 0;1 0.5 0;0.5 1 0;0 0.2 0;0 0.5 1;0 0 1;0.5 0 1;0 0 0];
figure;
ft_multiplotER(cfg, erf_singsh_co_ga_comb, erf_singsh_ip_ga_comb, erf_singuns_co_ga_comb, erf_singuns_ip_ga_comb); %
legend({'single-sh co','single-sh ip','single-uns co','single-uns ip'});
title('single shared unshared');

% set(gca,'YTick',0:1e-12:5e-12,'fontsize',10)
%%
close all;
clear all;
cd F:\2017_RMI_MEG\N2pc_gpsing\coip;

load erf_gpsh_co_ga_comb
load erf_gpsh_ip_ga_comb
load erf_singsh_co_ga_comb
load erf_singsh_ip_ga_comb


cfg.xlim = [-0.1 0.5];
% cfg.ylim = [0 5e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';
% cfg.channel = {'MEG2322+2323','MEG2432+2433','MEG2512+2513','MEG2522+2523','MEG2532+2533','MEG2542+2543'};
cfg.linewidth = 1.5;
% cfg.graphcolor = [1 0 0;1 0.5 0;0.5 1 0;0 0.2 0;0 0.5 1;0 0 1;0.5 0 1;0 0 0];
figure;
ft_multiplotER(cfg, erf_gpsh_co_ga_comb, erf_gpsh_ip_ga_comb, erf_singsh_co_ga_comb, erf_singsh_ip_ga_comb);
legend({'gp-sh co','gp-sh ip','single-sh co','single-sh ip'});
title('grouped shared vs. single shared');

%%
close all;
clear all;
cd F:\2017_RMI_MEG\N2pc_gpsing\coip;

load erf_gpuns_co_ga_comb
load erf_gpuns_ip_ga_comb
load erf_singuns_co_ga_comb
load erf_singuns_ip_ga_comb


cfg.xlim = [-0.1 0.5];
% cfg.ylim = [0 5e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';
% cfg.channel = {'MEG2322+2323','MEG2432+2433','MEG2512+2513','MEG2522+2523','MEG2532+2533','MEG2542+2543'};
cfg.linewidth = 1.5;
% cfg.graphcolor = [1 0 0;1 0.5 0;0.5 1 0;0 0.2 0;0 0.5 1;0 0 1;0.5 0 1;0 0 0];
figure;
ft_multiplotER(cfg, erf_gpuns_co_ga_comb, erf_gpuns_ip_ga_comb, erf_singuns_co_ga_comb, erf_singuns_ip_ga_comb);
legend({'gp-uns co','gp-uns ip','single-uns co','single-uns ip'});
title('grouped unshared vs. single unshared');


%% gp sep single
close all;
clear all;
cd F:\2017_RMI_MEG\N2pc_gpsing\coip;

load erf_gpuns_co_ga_comb
load erf_gpuns_ip_ga_comb
load erf_singuns_co_ga_comb
load erf_singuns_ip_ga_comb


cfg.xlim = [-0.1 0.5];
% cfg.ylim = [0 5e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';
% cfg.channel = {'MEG2322+2323','MEG2432+2433','MEG2512+2513','MEG2522+2523','MEG2532+2533','MEG2542+2543'};
cfg.linewidth = 1.5;
% cfg.graphcolor = [1 0 0;1 0.5 0;0.5 1 0;0 0.2 0;0 0.5 1;0 0 1;0.5 0 1;0 0 0];
figure;
ft_multiplotER(cfg, erf_gpuns_co_ga_comb, erf_gpuns_ip_ga_comb, erf_singuns_co_ga_comb, erf_singuns_ip_ga_comb);
legend({'gp-uns co','gp-uns ip','single-uns co','single-uns ip'});
title('grouped unshared vs. single unshared');

%%
close all;
clear all;
cd F:\2017_RMI_MEG\N2pc_gpsing\coip;

load erf_gpsh_co_ga_comb
load erf_gpuns_co_ga_comb

cfg.xlim = [-0.1 0.5];
% cfg.ylim = [-3e-12 3e-12]; %'maxmin'; %[-3e-12 2e-12];
cfg.layout  = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay'; 
cfg.linewidth = 1.5;
figure;
ft_multiplotER(cfg, erf_gpsh_co_ga_comb, erf_gpuns_co_ga_comb);
% ft_multiplotER(cfg, gpsh, gpuns, singsh, singuns);
legend({'gpsh','gpuns'});
% legend({'gpsh','gpuns','singsh','singuns'});
title('grouped shared unshared');
% title('grouped shared unshared single shared unshared');

