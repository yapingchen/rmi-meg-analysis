close all;
clear all;
cd F:\2017_RMI_MEG\N400_cell
%%


%%% load singsh and average
% grads
cd F:\2017_RMI_MEG\N400_cell
clear all
load('F:\2017_RMI_MEG\N400_cell\erf_rmi02_singsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi12_singsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi03_singsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi13_singsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi04_singsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi14_singsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi05_singsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi15_singsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi06_singsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi16_singsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi07_singsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi17_singsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi08_singsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi18_singsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi09_singsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi19_singsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi10_singsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi20_singsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi11_singsh_data.mat');

cfg = [];
cfg.keepindividual = 'yes' ;
erf_singsh_data = ft_timelockgrandaverage(cfg, erf_rmi02_singsh_data, erf_rmi03_singsh_data, erf_rmi04_singsh_data, erf_rmi05_singsh_data, erf_rmi06_singsh_data, erf_rmi07_singsh_data,...
                                           erf_rmi08_singsh_data, erf_rmi09_singsh_data, erf_rmi10_singsh_data, erf_rmi11_singsh_data, erf_rmi12_singsh_data, erf_rmi13_singsh_data,...
                                           erf_rmi14_singsh_data, erf_rmi15_singsh_data, erf_rmi16_singsh_data, erf_rmi17_singsh_data, erf_rmi18_singsh_data, erf_rmi19_singsh_data,...
                                           erf_rmi20_singsh_data);
save erf_singsh_data erf_singsh_data


%%% load gpsh and average
% grads
clear all
load('F:\2017_RMI_MEG\N400_cell\erf_rmi02_gpsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi12_gpsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi03_gpsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi13_gpsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi04_gpsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi14_gpsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi05_gpsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi15_gpsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi06_gpsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi16_gpsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi07_gpsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi17_gpsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi08_gpsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi18_gpsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi09_gpsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi19_gpsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi10_gpsh_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi20_gpsh_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi11_gpsh_data.mat');

cfg = [];
cfg.keepindividual = 'yes' ;
erf_gpsh_data = ft_timelockgrandaverage(cfg, erf_rmi02_gpsh_data, erf_rmi03_gpsh_data, erf_rmi04_gpsh_data, erf_rmi05_gpsh_data,erf_rmi06_gpsh_data, erf_rmi07_gpsh_data,...
                                           erf_rmi08_gpsh_data, erf_rmi09_gpsh_data, erf_rmi10_gpsh_data, erf_rmi11_gpsh_data, erf_rmi12_gpsh_data, erf_rmi13_gpsh_data,...
                                           erf_rmi14_gpsh_data, erf_rmi15_gpsh_data, erf_rmi16_gpsh_data, erf_rmi17_gpsh_data, erf_rmi18_gpsh_data, erf_rmi19_gpsh_data,...
                                           erf_rmi20_gpsh_data);
save erf_gpsh_data erf_gpsh_data

% load singuns and average
% grads
clear all
load('F:\2017_RMI_MEG\N400_cell\erf_rmi02_singuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi12_singuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi03_singuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi13_singuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi04_singuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi14_singuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi05_singuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi15_singuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi06_singuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi16_singuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi07_singuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi17_singuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi08_singuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi18_singuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi09_singuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi19_singuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi10_singuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi20_singuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi11_singuns_data.mat');

cfg = [];
cfg.keepindividual = 'yes' ;
erf_singuns_data = ft_timelockgrandaverage(cfg, erf_rmi02_singuns_data, erf_rmi03_singuns_data, erf_rmi04_singuns_data, erf_rmi05_singuns_data, erf_rmi06_singuns_data, erf_rmi07_singuns_data,...
                                           erf_rmi08_singuns_data, erf_rmi09_singuns_data, erf_rmi10_singuns_data, erf_rmi11_singuns_data, erf_rmi12_singuns_data, erf_rmi13_singuns_data,...
                                           erf_rmi14_singuns_data, erf_rmi15_singuns_data, erf_rmi16_singuns_data, erf_rmi17_singuns_data, erf_rmi18_singuns_data, erf_rmi19_singuns_data,...
                                           erf_rmi20_singuns_data);
save erf_singuns_data erf_singuns_data

% load gpuns and average
% grads
clear all
load('F:\2017_RMI_MEG\N400_cell\erf_rmi02_gpuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi12_gpuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi03_gpuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi13_gpuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi04_gpuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi14_gpuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi05_gpuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi15_gpuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi06_gpuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi16_gpuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi07_gpuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi17_gpuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi08_gpuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi18_gpuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi09_gpuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi19_gpuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi10_gpuns_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi20_gpuns_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi11_gpuns_data.mat');

cfg = [];
cfg.keepindividual = 'yes' ;
erf_gpuns_data = ft_timelockgrandaverage(cfg, erf_rmi02_gpuns_data, erf_rmi03_gpuns_data, erf_rmi04_gpuns_data, erf_rmi05_gpuns_data, erf_rmi06_gpuns_data, erf_rmi07_gpuns_data,...
                                           erf_rmi08_gpuns_data, erf_rmi09_gpuns_data, erf_rmi10_gpuns_data, erf_rmi11_gpuns_data, erf_rmi12_gpuns_data, erf_rmi13_gpuns_data,...
                                           erf_rmi14_gpuns_data, erf_rmi15_gpuns_data, erf_rmi16_gpuns_data, erf_rmi17_gpuns_data, erf_rmi18_gpuns_data, erf_rmi19_gpuns_data,...
                                           erf_rmi20_gpuns_data);
save erf_gpuns_data erf_gpuns_data


% load separated and average
% grads
clear all
load('F:\2017_RMI_MEG\N400_cell\erf_rmi02_sep_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi12_sep_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi03_sep_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi13_sep_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi04_sep_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi14_sep_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi05_sep_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi15_sep_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi06_sep_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi16_sep_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi07_sep_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi17_sep_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi08_sep_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi18_sep_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi09_sep_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi19_sep_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi10_sep_data.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi20_sep_data.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi11_sep_data.mat');

cfg = [];
cfg.keepindividual = 'yes' ;
erf_sep_data = ft_timelockgrandaverage(cfg, erf_rmi02_sep_data, erf_rmi03_sep_data, erf_rmi04_sep_data, erf_rmi05_sep_data, erf_rmi06_sep_data, erf_rmi07_sep_data,...
                                           erf_rmi08_sep_data, erf_rmi09_sep_data, erf_rmi10_sep_data, erf_rmi11_sep_data, erf_rmi12_sep_data, erf_rmi13_sep_data,...
                                           erf_rmi14_sep_data, erf_rmi15_sep_data, erf_rmi16_sep_data, erf_rmi17_sep_data, erf_rmi18_sep_data, erf_rmi19_sep_data,...
                                           erf_rmi20_sep_data);
save erf_sep_data erf_sep_data

%% plotting

close all;
clear all;

load erf_singsh_data
load erf_ga_singunssh_data

load erf_singuns_data
load erf_gpuns_data

cfg = [];
cfg.showlabels = 'yes'; 
cfg.interactive = 'yes'; 
cfg.fontsize = 6; 
cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306planar.lay';
%cfg.ylim = [-1e-12 8e-12];
figure(1)
ft_multiplotER(cfg, erf_singsh_data, erf_ga_singunssh_data); 

figure(2)
ft_multiplotER(cfg, erf_singuns_data, erf_gpuns_data); 



