%% Non-parametric permutation test

% rmpath(genpath('/home/kbc/matlab/fieldtrip/'));
% rmpath(genpath('/home/kbc/matlab/spm8_meeg/'));
% addpath(genpath('/home/kbc/matlab/spm8_meeg/'));

% rmpath(genpath('/home/kbc/matlab/fieldtrip-20150923/'));
% addpath(genpath('/home/kbc/matlab/fieldtrip-20140611/'));

%%  gp sh uns
close all;
clear all;
cd F:\2017_RMI_MEG\N400_cell\;

load erf_gpsh_comb
load erf_gpuns_comb

cfg             = [];
cfg.method      = 'template'; %% try 'template' and 'distance'
cfg.template    = 'D:\0_MEEG_toolbox\fieldtrip\template\neighbours\neuromag306cmb_neighb.mat';               % specify type of template
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';                      % specify layout of sensors*
cfg.feedback    = 'no';
neighbours      = ft_prepare_neighbours(cfg, erf_gpsh_comb); %% define neighbouring channels

cfg = [];
cfg.channel = {'MEG'};
cfg.latency = [0.2 0.22];
% cfg.avgovertime ='yes';

cfg.method = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.numrandomization = 500; %500
cfg.correctm = 'cluster';
cfg.alpha = 0.05;
cfg.tail = 0;

cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.clustertail = 0;

cfg.minnbchan = 2;
cfg.neighbours = neighbours;

subj = 19;
design = zeros(2,2*subj);
for i = 1:subj
  design(1,i) = i;
end
for i = 1:subj
  design(1,subj+i) = i;
end
design(2,1:subj)        = 1;
design(2,subj+1:2*subj) = 2;

cfg.design = design;
cfg.uvar  = 1;
cfg.ivar  = 2;

[stat] = ft_timelockstatistics(cfg, erf_gpsh_comb, erf_gpuns_comb);
% save stat_ERF_gpsh_gpuns_cluster_comb_14 stat
min(stat.prob)
stat.time
%% plotting gp sep
% clear all

% load stat_ERF_gpsh_gpuns_cluster
load erf_ga_gpsh_comb
load erf_ga_gpuns_comb
gpsh_sep = erf_ga_gpsh_comb; %%%
gpsh_sep.avg = erf_ga_gpsh_comb.avg - erf_ga_gpuns_comb.avg; %%%

stat.cfg.alpha = 0.1;

figure;  
% define parameters for plotting
timestep = 0.004;      %(in seconds)
sampling_rate = 250;
sample_count = length(stat.time);
j = [0.1:timestep:0.6];   % Temporal endpoints (in seconds) of the ERP average computed in each subplot
m = [1:timestep*sampling_rate:sample_count];  % temporal endpoints in MEEG samples
% get relevant (significant) values
pos_cluster_pvals = [stat.posclusters(:).prob];
pos_signif_clust = find(pos_cluster_pvals < stat.cfg.alpha);
pos = ismember(stat.posclusterslabelmat, pos_signif_clust);
neg_cluster_pvals = [stat.negclusters(:).prob];
neg_signif_clust = find(neg_cluster_pvals < stat.cfg.alpha);
neg = ismember(stat.negclusterslabelmat, neg_signif_clust);


for k = 1:20;
     subplot(4,5,k);     
     cfg = [];   
     cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';
     cfg.xlim=[j(k) j(k+1)];   
     cfg.zlim = [-1.0e-12 1.0e-12];   
     pos_int = all(pos(:, m(k):m(k+1)), 2);
     neg_int = all(neg(:, m(k):m(k+1)), 2);
     cfg.highlight = 'on';
     cfg.highlightchannel = find(pos_int);         
     cfg.comment = 'xlim';   
     cfg.commentpos = 'title';   
     ft_topoplotER(cfg, gpsh_sep);
end 
colorbar;
title('gpsh gpuns comb')
saveas (gcf, 'permute_gpsh_gpuns_cmb_1018.tif')

%% sh, gp vs. sing
close all;
clear all;
cd F:\2017_RMI_MEG\N400_cell\;

load erf_gpsh_comb
load erf_singsh_comb

cfg             = [];
cfg.method      = 'template'; % try 'distance' as well
cfg.template    = 'D:\0_MEEG_toolbox\fieldtrip\template\neighbours\neuromag306cmb_neighb.mat';               % specify type of template
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';                      % specify layout of sensors*
cfg.feedback    = 'no';
neighbours      = ft_prepare_neighbours(cfg, erf_gpsh_comb); % define neighbouring channels

cfg = [];
cfg.channel = {'MEG'};
cfg.latency = [0.22 0.28];
% cfg.avgovertime ='yes';

cfg.method = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.numrandomization = 500; %500
cfg.correctm = 'cluster';
cfg.alpha = 0.05;
cfg.tail = 0;

cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.clustertail = 0;

cfg.minnbchan = 2;
cfg.neighbours = neighbours;

subj = 19;
design = zeros(2,2*subj);
for i = 1:subj
  design(1,i) = i;
end
for i = 1:subj
  design(1,subj+i) = i;
end
design(2,1:subj)        = 1;
design(2,subj+1:2*subj) = 2;

cfg.design = design;
cfg.uvar  = 1;
cfg.ivar  = 2;

[stat] = ft_timelockstatistics(cfg, erf_gpsh_comb, erf_singsh_comb);
% save stat_ERF_gpsh_singsh_cluster_14 stat
min(stat.prob)
stat.time

%% plotting sh, gp vs. sing
% clear all

% load stat_ERF_gpsh_singsh_cluster
load erf_ga_gpsh_comb
load erf_ga_singsh_comb
gpsh_singsh = erf_ga_gpsh_comb; %%%
gpsh_singsh.avg = erf_ga_gpsh_comb.avg - erf_ga_singsh_comb.avg; %%%

stat.cfg.alpha = 0.1;

figure;  
% define parameters for plotting
timestep = 0.004;      %0.05 (in seconds)
sampling_rate = 250;
sample_count = length(stat.time);
j = [0.22:timestep:0.4];   % Temporal endpoints (in seconds) of the ERP average computed in each subplot
m = [1:timestep*sampling_rate:sample_count];  % temporal endpoints in MEEG samples
% get relevant (significant) values
pos_cluster_pvals = [stat.posclusters(:).prob];
pos_signif_clust = find(pos_cluster_pvals < stat.cfg.alpha);
pos = ismember(stat.posclusterslabelmat, pos_signif_clust);
% neg_cluster_pvals = [stat.negclusters(:).prob];
% neg_signif_clust = find(neg_cluster_pvals < stat.cfg.alpha);
% neg = ismember(stat.negclusterslabelmat, neg_signif_clust);


for k = 1:20;
     subplot(4,5,k);     
     cfg = [];   
     cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';
     cfg.xlim=[j(k) j(k+1)];   
     cfg.zlim = [-1.0e-12 1.0e-12];   
     pos_int = all(pos(:, m(k):m(k+1)), 2);
%      neg_int = all(neg(:, m(k):m(k+1)), 2);
     cfg.highlight = 'on';
     cfg.highlightchannel = find(pos_int);         
     cfg.comment = 'xlim';   
     cfg.commentpos = 'title';   
     ft_topoplotER(cfg, gpsh_singsh);
end 
colorbar;
title('gpsh singsh comb')
saveas(gcf, 'permute_gpsh_singsh_cmb_2228.tif')

%% uns, gp vs. sing
close all;
clear all;
cd F:\2017_RMI_MEG\N400_cell\;

load erf_gpuns_comb
load erf_singuns_comb

cfg             = [];
cfg.method      = 'template'; % try 'distance' as well
cfg.template    = 'D:\0_MEEG_toolbox\fieldtrip\template\neighbours\neuromag306cmb_neighb.mat';               % specify type of template
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';                      % specify layout of sensors*
cfg.feedback    = 'no';
neighbours      = ft_prepare_neighbours(cfg, erf_gpuns_comb); % define neighbouring channels

cfg = [];
cfg.channel = {'MEG'};
cfg.latency = [0.1 0.4];
% cfg.avgovertime ='yes';

cfg.method = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.numrandomization = 500; %500
cfg.correctm = 'cluster';
cfg.alpha = 0.05;
cfg.tail = 0;

cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.clustertail = 0;

cfg.minnbchan = 2;
cfg.neighbours = neighbours;

subj = 19;
design = zeros(2,2*subj);
for i = 1:subj
  design(1,i) = i;
end
for i = 1:subj
  design(1,subj+i) = i;
end
design(2,1:subj)        = 1;
design(2,subj+1:2*subj) = 2;

cfg.design = design;
cfg.uvar  = 1;
cfg.ivar  = 2;

[stat] = ft_timelockstatistics(cfg, erf_gpuns_comb, erf_singuns_comb);
% save stat_ERF_gpuns_singuns_cluster_14 stat
min(stat.prob)
stat.time

%% plotting uns, gp vs. sing
% clear all

% load stat_ERF_gpuns_singuns_cluster
load erf_ga_gpuns_comb
load erf_ga_singuns_comb
gpuns_singuns = erf_ga_gpuns_comb; %%%
gpuns_singuns.avg = erf_ga_gpuns_comb.avg - erf_ga_singuns_comb.avg; %%%

stat.cfg.alpha = 0.1;

figure;  
% define parameters for plotting
timestep = 0.02;      %0.05 (in seconds)
sampling_rate = 250;
sample_count = length(stat.time);
j = [0.1:timestep:0.4];   % Temporal endpoints (in seconds) of the ERP average computed in each subplot
m = [1:timestep*sampling_rate:sample_count];  % temporal endpoints in MEEG samples
% get relevant (significant) values
pos_cluster_pvals = [stat.posclusters(:).prob];
pos_signif_clust = find(pos_cluster_pvals < stat.cfg.alpha);
pos = ismember(stat.posclusterslabelmat, pos_signif_clust);
% neg_cluster_pvals = [stat.negclusters(:).prob];
% neg_signif_clust = find(neg_cluster_pvals < stat.cfg.alpha);
% neg = ismember(stat.negclusterslabelmat, neg_signif_clust);


for k = 1:20;
     subplot(4,5,k);     
     cfg = [];   
     cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';
     cfg.xlim=[j(k) j(k+1)];   
     cfg.zlim = [-1.0e-12 1.0e-12];   
     pos_int = all(pos(:, m(k):m(k+1)), 2);
%      neg_int = all(neg(:, m(k):m(k+1)), 2);
     cfg.highlight = 'on';
     cfg.highlightchannel = find(pos_int);         
     cfg.comment = 'xlim';   
     cfg.commentpos = 'title';   
     ft_topoplotER(cfg, gpuns_singuns);
end 
colorbar;
title('gpuns singuns comb')
saveas(gcf, 'permute_gpuns_singuns_cmb_14.tif')

