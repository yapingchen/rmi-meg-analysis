%% Non-parametric permutation test

% rmpath(genpath('/home/kbc/matlab/fieldtrip/'));
% rmpath(genpath('/home/kbc/matlab/spm8_meeg/'));
% rmpath(genpath('/home/kbc/matlab/fieldtrip-20150923/'));
% addpath(genpath('/home/kbc/matlab/fieldtrip-20140611/'));
 
%%  gp sep
close all;
clear all;
cd F:\2017_RMI_MEG\N400_cell\;

load erf_gpsh_data
load erf_gpuns_data

cfg             = [];
cfg.method      = 'template'; % try 'template' and 'distance' as well
cfg.template    = 'D:\0_MEEG_toolbox\fieldtrip\template\neighbours\neuromag306planar_neighb.mat';               % specify type of template
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';                      % specify layout of sensors*
cfg.feedback    = 'no';
neighbours      = ft_prepare_neighbours(cfg, erf_gpsh_data); % define neighbouring channels

cfg = [];
cfg.channel = {'MEG'};
cfg.latency = [0.14 0.18];
% cfg.avgovertime ='yes';

cfg.method = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.numrandomization = 500; %500
cfg.correctm = 'cluster';
cfg.alpha = 0.05;
cfg.tail = 0;

cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.clustertail = 0;

cfg.minnbchan = 2;
cfg.neighbours = neighbours;

subj = 19;
design = zeros(2,2*subj);
for i = 1:subj
  design(1,i) = i;
end
for i = 1:subj
  design(1,subj+i) = i;
end
design(2,1:subj)        = 1;
design(2,subj+1:2*subj) = 2;

cfg.design = design;
cfg.uvar  = 1;
cfg.ivar  = 2;

[stat] = ft_timelockstatistics(cfg, erf_gpsh_data, erf_gpuns_data);
% save stat_ERF_gpsh_gpuns_cluster_lati_2022 stat
min(stat.prob)

%% plotting gp sep
% clear all

% load stat_ERF_gpsh_gpuns_cluster_lati_26
load erf_ga_gpsh_data
load erf_ga_gpuns_data
gpsh_gpuns = erf_ga_gpsh_data; %%%
gpsh_gpuns.avg = erf_ga_gpsh_data.avg - erf_ga_gpuns_data.avg; %%%

figure;  
% define parameters for plotting
timestep = 0.004;      %(in seconds)
sampling_rate = 250;
sample_count = length(stat.time);
j = [0.2:timestep:0.4];   % Temporal endpoints (in seconds) of the ERP average computed in each subplot
m = [1:timestep*sampling_rate:sample_count];  % temporal endpoints in MEEG samples
% get relevant (significant) values
pos_cluster_pvals = [stat.posclusters(:).prob];
pos_signif_clust = find(pos_cluster_pvals < stat.cfg.alpha);
pos = ismember(stat.posclusterslabelmat, pos_signif_clust);
neg_cluster_pvals = [stat.negclusters(:).prob];
neg_signif_clust = find(neg_cluster_pvals < stat.cfg.alpha);
neg = ismember(stat.negclusterslabelmat, neg_signif_clust);


for k = 1:20;
     subplot(4,5,k);     
     cfg = [];   
     cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';
     cfg.xlim=[j(k) j(k+1)];   
     cfg.zlim = [-1.0e-12 1.0e-12];   
     pos_int = all(pos(:, m(k):m(k+1)), 2);
%      neg_int = all(neg(:, m(k):m(k+1)), 2);
     cfg.highlight = 'on';
     cfg.highlightchannel = find(pos_int);         %|neg_int
     cfg.comment = 'xlim';   
     cfg.commentpos = 'title';   
     ft_topoplotER(cfg, gpsh_gpuns);
end 
colorbar;
title('gpsh gpuns lati')
saveas (gcf, 'permute_gpsh_gpuns_lati_2024.tif')

%% sh, gp vs. sing
close all;
clear all;
cd F:\2017_RMI_MEG\N400_cell\;

load erf_gpsh_data
load erf_singsh_data

cfg             = [];
cfg.method      = 'template'; % try 'template' and 'distance' as well
cfg.template    = 'D:\0_MEEG_toolbox\fieldtrip\template\neighbours\neuromag306planar_neighb.mat';               % specify type of template
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';                      % specify layout of sensors*
cfg.feedback    = 'no';
neighbours      = ft_prepare_neighbours(cfg, erf_gpsh_data); % define neighbouring channels

cfg = [];
cfg.channel = {'MEG'};
cfg.latency = [0.1 0.4];
% cfg.avgovertime ='yes';

cfg.method = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.numrandomization = 500; %500
cfg.correctm = 'cluster';
cfg.alpha = 0.05;
cfg.tail = 0;

cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.clustertail = 0;

cfg.minnbchan = 2;
cfg.neighbours = neighbours;

subj = 19;
design = zeros(2,2*subj);
for i = 1:subj
  design(1,i) = i;
end
for i = 1:subj
  design(1,subj+i) = i;
end
design(2,1:subj)        = 1;
design(2,subj+1:2*subj) = 2;

cfg.design = design;
cfg.uvar  = 1;
cfg.ivar  = 2;

[stat] = ft_timelockstatistics(cfg, erf_gpsh_data, erf_singsh_data);
% save stat_ERF_gpsh_singsh_cluster_lati_14 stat
min(stat.prob)


%% plotting sh, gp vs. sing
% clear all

% load stat_ERF_gpsh_singsh_cluster_lati_26
load erf_ga_gpsh_data
load erf_ga_singsh_data
sh_gpsing = erf_ga_gpsh_data; %%%
sh_gpsing.avg = erf_ga_gpsh_data.avg - erf_ga_singsh_data.avg; %%%

stat.cfg.alpha = 0.1;

figure;  
% define parameters for plotting
timestep = 0.02;      %(in seconds)
sampling_rate = 250;
sample_count = length(stat.time);
j = [0.1:timestep:0.6];   % Temporal endpoints (in seconds) of the ERP average computed in each subplot
m = [1:timestep*sampling_rate:sample_count];  % temporal endpoints in MEEG samples
% get relevant (significant) values
pos_cluster_pvals = [stat.posclusters(:).prob];
pos_signif_clust = find(pos_cluster_pvals < stat.cfg.alpha);
pos = ismember(stat.posclusterslabelmat, pos_signif_clust);
neg_cluster_pvals = [stat.negclusters(:).prob];
neg_signif_clust = find(neg_cluster_pvals < stat.cfg.alpha);
neg = ismember(stat.negclusterslabelmat, neg_signif_clust);


for k = 1:20;
     subplot(4,5,k);     
     cfg = [];   
     cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';
     cfg.xlim=[j(k) j(k+1)];   
     cfg.zlim = [-1.0e-12 1.0e-12];   
     pos_int = all(pos(:, m(k):m(k+1)), 2);
     neg_int = all(neg(:, m(k):m(k+1)), 2);
     cfg.highlight = 'on';
     cfg.highlightchannel = find(pos_int|neg_int);         
     cfg.comment = 'xlim';   
     cfg.commentpos = 'title';   
     ft_topoplotER(cfg, sh_gpsing);
end 
colorbar;
title('sh, gp vs. single lati')
saveas(gcf, 'permute_gpsh_singsh_lati_14.tif')


%%  gp single
close all;
clear all;
cd F:\2017_RMI_MEG\N400_cell\;

load erf_gpuns_data
load erf_singuns_data

cfg             = [];
cfg.method      = 'template'; % try 'template' and 'distance' as well
cfg.template    = 'D:\0_MEEG_toolbox\fieldtrip\template\neighbours\neuromag306planar_neighb.mat';               % specify type of template
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';                      % specify layout of sensors*
cfg.feedback    = 'no';
neighbours      = ft_prepare_neighbours(cfg, erf_gpuns_data); % define neighbouring channels

cfg = [];
cfg.channel = {'MEG'};
cfg.latency = [0.34 0.38];
% cfg.avgovertime ='yes';

cfg.method = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.numrandomization = 500; %500
cfg.correctm = 'cluster';
cfg.alpha = 0.05;
cfg.tail = 0;

cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.clustertail = 0;

cfg.minnbchan = 2;
cfg.neighbours = neighbours;

subj = 19;
design = zeros(2,2*subj);
for i = 1:subj
  design(1,i) = i;
end
for i = 1:subj
  design(1,subj+i) = i;
end
design(2,1:subj)        = 1;
design(2,subj+1:2*subj) = 2;

cfg.design = design;
cfg.uvar  = 1;
cfg.ivar  = 2;

[stat] = ft_timelockstatistics(cfg, erf_gpuns_data, erf_singuns_data);
% save stat_ERF_gpuns_singuns_cluster_lati_2428 stat
min(stat.prob)
%% plotting gp single
% clear all

% load stat_ERF_gpuns_singuns_cluster_lati_26
load erf_ga_gpuns_data
load erf_ga_singuns_data
gpuns_singuns = erf_ga_gpuns_data; %%%
gpuns_singuns.avg = erf_ga_gpuns_data.avg - erf_ga_singuns_data.avg; %%%

figure;  
% define parameters for plotting
timestep = 0.004;      %(in seconds)
sampling_rate = 250;
sample_count = length(stat.time);
j = [0.34:timestep:0.4];   % Temporal endpoints (in seconds) of the ERP average computed in each subplot
m = [1:timestep*sampling_rate:sample_count];  % temporal endpoints in MEEG samples
% get relevant (significant) values
pos_cluster_pvals = [stat.posclusters(:).prob];
pos_signif_clust = find(pos_cluster_pvals < stat.cfg.alpha);
pos = ismember(stat.posclusterslabelmat, pos_signif_clust);
neg_cluster_pvals = [stat.negclusters(:).prob];
neg_signif_clust = find(neg_cluster_pvals < stat.cfg.alpha);
neg = ismember(stat.negclusterslabelmat, neg_signif_clust);


for k = 1:20;
     subplot(4,5,k);     
     cfg = [];   
     cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';
     cfg.xlim=[j(k) j(k+1)];   
     cfg.zlim = [-1.0e-12 1.0e-12];   
     pos_int = all(pos(:, m(k):m(k+1)), 2);
     neg_int = all(neg(:, m(k):m(k+1)), 2);
     cfg.highlight = 'on';
     cfg.highlightchannel = find(pos_int|neg_int);   % pos_int|
     cfg.comment = 'xlim';   
     cfg.commentpos = 'title';   
     ft_topoplotER(cfg, gpuns_singuns);
end 
% colorbar;
% title('gpuns singuns lati')
% saveas(gcf, 'permute_gpuns_singuns_lati_1519.tif')

%%  single, sh vs. uns 
% close all;
% clear all;
cd F:\2017_RMI_MEG\N400_cell\;

load erf_singsh_data
load erf_singuns_data

cfg             = [];
cfg.method      = 'distance'; % try 'template' and 'distance' as well
cfg.template    = 'D:\0_MEEG_toolbox\fieldtrip\template\neighbours\neuromag306planar_neighb.mat';               % specify type of template
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';                      % specify layout of sensors*
cfg.feedback    = 'no';
neighbours      = ft_prepare_neighbours(cfg, erf_singsh_data); % define neighbouring channels

cfg = [];
cfg.channel = {'MEG'};
cfg.latency = [0.1 0.4];
% cfg.avgovertime ='yes';

cfg.method = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.numrandomization = 500; %500
cfg.correctm = 'cluster';
cfg.alpha = 0.05;
cfg.tail = 0;

cfg.clusteralpha = 0.05;
cfg.clusterstatistic = 'maxsum';
cfg.clustertail = 0;

cfg.minnbchan = 2;
cfg.neighbours = neighbours;

subj = 19;
design = zeros(2,2*subj);
for i = 1:subj
  design(1,i) = i;
end
for i = 1:subj
  design(1,subj+i) = i;
end
design(2,1:subj)        = 1;
design(2,subj+1:2*subj) = 2;

cfg.design = design;
cfg.uvar  = 1;
cfg.ivar  = 2;

[stat] = ft_timelockstatistics(cfg, erf_singsh_data, erf_singuns_data);
% save stat_ERF_singsh_singuns_cluster_lati_26 stat
min(stat.prob)
%% plotting sep single
% clear all

% load stat_ERF_singsh_singuns_cluster_lati_26
load erf_ga_singsh_data
load erf_ga_singuns_data
singsh_single = erf_ga_singsh_data; %%%
singsh_single.avg = erf_ga_singsh_data.avg - erf_ga_singuns_data.avg; %%%

figure;  
% define parameters for plotting
timestep = 0.02;      %(in seconds)
sampling_rate = 250;
sample_count = length(stat.time);
j = [0.1:timestep:0.6];   % Temporal endpoints (in seconds) of the ERP average computed in each subplot
m = [1:timestep*sampling_rate:sample_count];  % temporal endpoints in MEEG samples
% get relevant (significant) values
pos_cluster_pvals = [stat.posclusters(:).prob];
pos_signif_clust = find(pos_cluster_pvals < stat.cfg.alpha);
pos = ismember(stat.posclusterslabelmat, pos_signif_clust);
neg_cluster_pvals = [stat.negclusters(:).prob];
neg_signif_clust = find(neg_cluster_pvals < stat.cfg.alpha);
neg = ismember(stat.negclusterslabelmat, neg_signif_clust);


for k = 1:20;
     subplot(4,5,k);     
     cfg = [];   
     cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';
     cfg.xlim=[j(k) j(k+1)];   
     cfg.zlim = [-1.0e-12 1.0e-12];   
     pos_int = all(pos(:, m(k):m(k+1)), 2);
     neg_int = all(neg(:, m(k):m(k+1)), 2);
     cfg.highlight = 'on';
     cfg.highlightchannel = find(pos_int|neg_int);       % pos_int| 
     cfg.comment = 'xlim';   
     cfg.commentpos = 'title';   
     ft_topoplotER(cfg, singsh_single);
end 
colorbar;
title('single, sh vs. uns lati')
% saveas (gcf, 'permute_singsh_singuns_lati_14')