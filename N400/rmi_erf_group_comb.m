close all;
clear all;
cd F:\2017_RMI_MEG\N400_cell
%%

%%% load singsh and average
% grads
cd F:\2017_RMI_MEG\N400_cell
clear all
load('F:\2017_RMI_MEG\N400_cell\erf_rmi02_singsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi12_singsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi03_singsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi13_singsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi04_singsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi14_singsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi05_singsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi15_singsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi06_singsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi16_singsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi07_singsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi17_singsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi08_singsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi18_singsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi09_singsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi19_singsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi10_singsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi20_singsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi11_singsh_comb.mat');

cfg = [];
cfg.keepindividual = 'yes' ;
erf_singsh_comb = ft_timelockgrandaverage(cfg, erf_rmi02_singsh_comb, erf_rmi03_singsh_comb, erf_rmi04_singsh_comb, erf_rmi05_singsh_comb, erf_rmi06_singsh_comb, erf_rmi07_singsh_comb,...
                                           erf_rmi08_singsh_comb, erf_rmi09_singsh_comb, erf_rmi10_singsh_comb, erf_rmi11_singsh_comb, erf_rmi12_singsh_comb, erf_rmi13_singsh_comb,...
                                           erf_rmi14_singsh_comb, erf_rmi15_singsh_comb, erf_rmi16_singsh_comb, erf_rmi17_singsh_comb, erf_rmi18_singsh_comb, erf_rmi19_singsh_comb,...
                                           erf_rmi20_singsh_comb);
save erf_singsh_comb erf_singsh_comb


%%% load gpsh and average
% grads
clear all
load('F:\2017_RMI_MEG\N400_cell\erf_rmi02_gpsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi12_gpsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi03_gpsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi13_gpsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi04_gpsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi14_gpsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi05_gpsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi15_gpsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi06_gpsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi16_gpsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi07_gpsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi17_gpsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi08_gpsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi18_gpsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi09_gpsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi19_gpsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi10_gpsh_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi20_gpsh_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi11_gpsh_comb.mat');

cfg = [];
cfg.keepindividual = 'yes' ;
erf_gpsh_comb = ft_timelockgrandaverage(cfg, erf_rmi02_gpsh_comb, erf_rmi03_gpsh_comb, erf_rmi04_gpsh_comb, erf_rmi05_gpsh_comb,erf_rmi06_gpsh_comb, erf_rmi07_gpsh_comb,...
                                           erf_rmi08_gpsh_comb, erf_rmi09_gpsh_comb, erf_rmi10_gpsh_comb, erf_rmi11_gpsh_comb, erf_rmi12_gpsh_comb, erf_rmi13_gpsh_comb,...
                                           erf_rmi14_gpsh_comb, erf_rmi15_gpsh_comb, erf_rmi16_gpsh_comb, erf_rmi17_gpsh_comb, erf_rmi18_gpsh_comb, erf_rmi19_gpsh_comb,...
                                           erf_rmi20_gpsh_comb);
save erf_gpsh_comb erf_gpsh_comb

% load singuns and average
% grads
clear all
load('F:\2017_RMI_MEG\N400_cell\erf_rmi02_singuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi12_singuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi03_singuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi13_singuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi04_singuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi14_singuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi05_singuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi15_singuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi06_singuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi16_singuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi07_singuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi17_singuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi08_singuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi18_singuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi09_singuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi19_singuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi10_singuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi20_singuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi11_singuns_comb.mat');

cfg = [];
cfg.keepindividual = 'yes' ;
erf_singuns_comb = ft_timelockgrandaverage(cfg, erf_rmi02_singuns_comb, erf_rmi03_singuns_comb, erf_rmi04_singuns_comb, erf_rmi05_singuns_comb, erf_rmi06_singuns_comb, erf_rmi07_singuns_comb,...
                                           erf_rmi08_singuns_comb, erf_rmi09_singuns_comb, erf_rmi10_singuns_comb, erf_rmi11_singuns_comb, erf_rmi12_singuns_comb, erf_rmi13_singuns_comb,...
                                           erf_rmi14_singuns_comb, erf_rmi15_singuns_comb, erf_rmi16_singuns_comb, erf_rmi17_singuns_comb, erf_rmi18_singuns_comb, erf_rmi19_singuns_comb,...
                                           erf_rmi20_singuns_comb);
save erf_singuns_comb erf_singuns_comb

% load gpuns and average
% grads
clear all
load('F:\2017_RMI_MEG\N400_cell\erf_rmi02_gpuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi12_gpuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi03_gpuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi13_gpuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi04_gpuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi14_gpuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi05_gpuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi15_gpuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi06_gpuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi16_gpuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi07_gpuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi17_gpuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi08_gpuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi18_gpuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi09_gpuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi19_gpuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi10_gpuns_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi20_gpuns_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi11_gpuns_comb.mat');

cfg = [];
cfg.keepindividual = 'yes' ;
erf_gpuns_comb = ft_timelockgrandaverage(cfg, erf_rmi02_gpuns_comb, erf_rmi03_gpuns_comb, erf_rmi04_gpuns_comb, erf_rmi05_gpuns_comb, erf_rmi06_gpuns_comb, erf_rmi07_gpuns_comb,...
                                           erf_rmi08_gpuns_comb, erf_rmi09_gpuns_comb, erf_rmi10_gpuns_comb, erf_rmi11_gpuns_comb, erf_rmi12_gpuns_comb, erf_rmi13_gpuns_comb,...
                                           erf_rmi14_gpuns_comb, erf_rmi15_gpuns_comb, erf_rmi16_gpuns_comb, erf_rmi17_gpuns_comb, erf_rmi18_gpuns_comb, erf_rmi19_gpuns_comb,...
                                           erf_rmi20_gpuns_comb);
save erf_gpuns_comb erf_gpuns_comb


% load separated and average
% grads
clear all
load('F:\2017_RMI_MEG\N400_cell\erf_rmi02_sep_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi12_sep_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi03_sep_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi13_sep_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi04_sep_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi14_sep_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi05_sep_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi15_sep_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi06_sep_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi16_sep_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi07_sep_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi17_sep_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi08_sep_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi18_sep_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi09_sep_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi19_sep_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi10_sep_comb.mat');load('F:\2017_RMI_MEG\N400_cell\erf_rmi20_sep_comb.mat');
load('F:\2017_RMI_MEG\N400_cell\erf_rmi11_sep_comb.mat');

cfg = [];
cfg.keepindividual = 'yes' ;
erf_sep_comb = ft_timelockgrandaverage(cfg, erf_rmi02_sep_comb, erf_rmi03_sep_comb, erf_rmi04_sep_comb, erf_rmi05_sep_comb, erf_rmi06_sep_comb, erf_rmi07_sep_comb,...
                                           erf_rmi08_sep_comb, erf_rmi09_sep_comb, erf_rmi10_sep_comb, erf_rmi11_sep_comb, erf_rmi12_sep_comb, erf_rmi13_sep_comb,...
                                           erf_rmi14_sep_comb, erf_rmi15_sep_comb, erf_rmi16_sep_comb, erf_rmi17_sep_comb, erf_rmi18_sep_comb, erf_rmi19_sep_comb,...
                                           erf_rmi20_sep_comb);
save erf_sep_comb erf_sep_comb

%% plotting

close all;
clear all;

load erf_singsh_comb
load erf_ga_singunssh_comb

load erf_singuns_comb
load erf_gpuns_comb

cfg = [];
cfg.showlabels = 'yes'; 
cfg.interactive = 'yes'; 
cfg.fontsize = 6; 
cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306planar.lay';
%cfg.ylim = [-1e-12 8e-12];
figure(1)
ft_multiplotER(cfg, erf_singsh_comb, erf_ga_singunssh_comb); 

figure(2)
ft_multiplotER(cfg, erf_singuns_comb, erf_gpuns_comb); 



