
spm('defaults', 'eeg');

for k = [2:6 8:11] % 2:6 8:20
    aa = sprintf('s%02d',k);
    eval(['cd F:\2018_RMI_theta\' aa ]);
    
%     S = [];
%     S.D = ['F:\2018_RMI_theta\' aa '\prcpbefdspm8_run1_raw_trans_sss.mat'];
%     S.frequencies = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29];
%     S.channels = {'all'};
%     S.timewin = [-Inf Inf];
%     S.phase = 0;
%     S.method = 'morlet';
%     S.settings.ncycles = 7;
%     S.settings.timeres = 0;
%     S.settings.subsample = 1;
%     D = spm_eeg_tf(S);
%     
%     
%     S = [];
%     S.D = ['F:\2018_RMI_theta\' aa '\tf_prcpbefdspm8_run1_raw_trans_sss.mat'];
%     S.circularise = 0;
%     S.robust = false;
%     D = spm_eeg_average_TF(S);
%     
%     
%     S = [];
%     S.D = ['F:\2018_RMI_theta\' aa '\mtf_prcpbefdspm8_run1_raw_trans_sss.mat'];
%     S.tf.method = 'logr';
%     S.tf.Db = [];
%     S.tf.Sbaseline = [-0.3 -0.1];
%     D = spm_eeg_tf_rescale(S);
    
    
    S = [];
    S.D = ['F:\2018_RMI_theta\' aa '\rmtf_prcpbefdspm8_run1_raw_trans_sss.mat'];
    %%%
    S.c = [1 1 1 1 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 2 2 0
        1 1 0 0 1 1 0 0 0 0 0
        0 0 1 1 0 0 1 1 0 0 0
        2 2 0 0 0 0 0 0 0 0 0
        0 0 2 2 0 0 0 0 0 0 0
        0 0 0 0 2 2 0 0 0 0 0
        0 0 0 0 0 0 2 2 0 0 0];
    
    S.label = {
        'gp'
        'sep'
        'shr'
        'unshr'
        'gpshr'
        'gpunshr'
        'ftshr'
        'ftunshr'
        }';
    %%%
    S.WeightAve = 0;
    D = spm_eeg_weight_epochs(S);
    
end

%%%

for k = [12:20] 
    aa = sprintf('s%02d',k);
    eval(['cd F:\2018_RMI_theta\' aa ]);
    
%     S = [];
%     S.D = ['F:\2018_RMI_theta\' aa '\prcpbefdspm8_run1raw_trans_sss.mat'];
%     S.frequencies = [30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58];
%     S.channels = {'all'};
%     S.timewin = [-Inf Inf];
%     S.phase = 0;
%     S.method = 'morlet';
%     S.settings.ncycles = 7;
%     S.settings.timeres = 0;
%     S.settings.subsample = 1;
%     D = spm_eeg_tf(S);
%     
%     
%     S = [];
%     S.D = ['F:\2018_RMI_theta\' aa '\tf_prcpbefdspm8_run1raw_trans_sss.mat'];
%     S.circularise = 0;
%     S.robust = false;
%     D = spm_eeg_average_TF(S);
    
    
%     S = [];
%     S.D = ['F:\2018_RMI_theta\' aa '\mtf_prcpbefdspm8_run1raw_trans_sss.mat'];
%     S.tf.method = 'LogR';
%     S.tf.Db = [];
%     S.tf.Sbaseline = [-0.2 0];
%     D = spm_eeg_tf_rescale(S);
%     
    
    S = [];
    S.D = ['F:\2018_RMI_theta\' aa '\rmtf_prcpbefdspm8_run1raw_trans_sss.mat'];
    %%%
    S.c = [1 1 1 1 0 0 0 0 0 0 0
        0 0 0 0 0 0 0 0 2 2 0
        1 1 0 0 1 1 0 0 0 0 0
        0 0 1 1 0 0 1 1 0 0 0
        2 2 0 0 0 0 0 0 0 0 0
        0 0 2 2 0 0 0 0 0 0 0
        0 0 0 0 2 2 0 0 0 0 0
        0 0 0 0 0 0 2 2 0 0 0];
    
    S.label = {
        'gp'
        'sep'
        'shr'
        'unshr'
        'gpshr'
        'gpunshr'
        'ftshr'
        'ftunshr'
        }';
    %%%
    S.WeightAve = 0;
    D = spm_eeg_weight_epochs(S);
    
end

