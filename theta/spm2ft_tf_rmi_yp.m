close all
clear all
clc

%%
clear all
clc
for k = [12:20] % 2:6 8:11 12:20
    aa = sprintf('s%02d',k);
    eval(['cd F:\2018_RMI_theta\' aa ]); % where the data is

% _gp

D = spm_eeg_load ('wrmtf_prcpbefdspm8_run1raw_trans_sss.mat'); % run1raw run1raw


ind = 1
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

eval(['tfdata_' aa '_gp = data']);
%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

eval(['tfdata_' aa '_gp.grad = cfg.grad']);

eval(['save tfdata_' aa '_gp tfdata_' aa '_gp']);

eval(['tfdata_' aa '_gp_check = ft_checkdata(tfdata_' aa '_gp, ''senstype'',''neuromag306'')']);
eval(['tfdata_' aa '_gp_check2 = ft_checkdata(tfdata_' aa '_gp_check, ''datatype'', ''freq'')']);
eval(['tfcomb_' aa '_gp = ft_combineplanar(cfg, tfdata_' aa '_gp)']);

eval(['save tfcomb_' aa '_gp tfcomb_' aa '_gp']);
% clear all;

% %% 
% %% _sep

D = spm_eeg_load ('wrmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%
ind = 2
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

eval(['tfdata_' aa '_sep = data']);
%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

eval(['tfdata_' aa '_sep.grad = cfg.grad']);

eval(['save tfdata_' aa '_sep tfdata_' aa '_sep']);

eval(['tfdata_' aa '_sep_check = checkdata(tfdata_' aa '_sep, ''senstype'',''neuromag306'')']);
eval(['tfdata_' aa '_sep_check2 = checkdata(tfdata_' aa '_sep_check, ''datatype'', ''freq'')']);
eval(['tfcomb_' aa '_sep = ft_combineplanar(cfg, tfdata_' aa '_sep)']);

eval(['save tfcomb_' aa '_sep tfcomb_' aa '_sep']);


% %% 
% %% _shr

D = spm_eeg_load ('wrmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%
ind = 3
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

eval(['tfdata_' aa '_shr = data']);
%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

eval(['tfdata_' aa '_shr.grad = cfg.grad']);

eval(['save tfdata_' aa '_shr tfdata_' aa '_shr']);

eval(['tfdata_' aa '_shr_check = checkdata(tfdata_' aa '_shr, ''senstype'',''neuromag306'')']);
eval(['tfdata_' aa '_shr_check2 = checkdata(tfdata_' aa '_shr_check, ''datatype'', ''freq'')']);
eval(['tfcomb_' aa '_shr = ft_combineplanar(cfg, tfdata_' aa '_shr)']);

eval(['save tfcomb_' aa '_shr tfcomb_' aa '_shr']);

% %% 
% %% _unshr

D = spm_eeg_load ('wrmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%
ind = 4
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

eval(['tfdata_' aa '_unshr = data']);
%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

eval(['tfdata_' aa '_unshr.grad = cfg.grad']);

eval(['save tfdata_' aa '_unshr tfdata_' aa '_unshr']);

eval(['tfdata_' aa '_unshr_check = checkdata(tfdata_' aa '_unshr, ''senstype'',''neuromag306'')']);
eval(['tfdata_' aa '_unshr_check2 = checkdata(tfdata_' aa '_unshr_check, ''datatype'', ''freq'')']);
eval(['tfcomb_' aa '_unshr = ft_combineplanar(cfg, tfdata_' aa '_unshr)']);

eval(['save tfcomb_' aa '_unshr tfcomb_' aa '_unshr']);

% %% 
% %% _gpshr

D = spm_eeg_load ('wrmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%
ind = 5
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

eval(['tfdata_' aa '_gpshr = data']);
%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

eval(['tfdata_' aa '_gpshr.grad = cfg.grad']);

eval(['save tfdata_' aa '_gpshr tfdata_' aa '_gpshr']);

eval(['tfdata_' aa '_gpshr_check = checkdata(tfdata_' aa '_gpshr, ''senstype'',''neuromag306'')']);
eval(['tfdata_' aa '_gpshr_check2 = checkdata(tfdata_' aa '_gpshr_check, ''datatype'', ''freq'')']);
eval(['tfcomb_' aa '_gpshr = ft_combineplanar(cfg, tfdata_' aa '_gpshr)']);

eval(['save tfcomb_' aa '_gpshr tfcomb_' aa '_gpshr']);


% %% _gpunshr

D = spm_eeg_load ('wrmtf_prcpbefdspm8_run1raw_trans_sss.mat');

%%%
ind = 6
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

eval(['tfdata_' aa '_gpunshr = data']);
%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

eval(['tfdata_' aa '_gpunshr.grad = cfg.grad']);

eval(['save tfdata_' aa '_gpunshr tfdata_' aa '_gpunshr']);

eval(['tfdata_' aa '_gpunshr_check = checkdata(tfdata_' aa '_gpunshr, ''senstype'',''neuromag306'')']);
eval(['tfdata_' aa '_gpunshr_check2 = checkdata(tfdata_' aa '_gpunshr_check, ''datatype'', ''freq'')']);
eval(['tfcomb_' aa '_gpunshr = ft_combineplanar(cfg, tfdata_' aa '_gpunshr)']);

eval(['save tfcomb_' aa '_gpunshr tfcomb_' aa '_gpunshr']);

%%%
% %% _ftshr

D = spm_eeg_load ('wrmtf_prcpbefdspm8_run1raw_trans_sss.mat');


ind = 7
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

eval(['tfdata_' aa '_ftshr = data']);
%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

eval(['tfdata_' aa '_ftshr.grad = cfg.grad']);

eval(['save tfdata_' aa '_ftshr tfdata_' aa '_ftshr']);

eval(['tfdata_' aa '_ftshr_check = checkdata(tfdata_' aa '_ftshr, ''senstype'',''neuromag306'')']);
eval(['tfdata_' aa '_ftshr_check2 = checkdata(tfdata_' aa '_ftshr_check, ''datatype'', ''freq'')']);
eval(['tfcomb_' aa '_ftshr = ft_combineplanar(cfg, tfdata_' aa '_ftshr)']);

eval(['save tfcomb_' aa '_ftshr tfcomb_' aa '_ftshr']);

%%%
% %% _ftunshr

D = spm_eeg_load ('wrmtf_prcpbefdspm8_run1raw_trans_sss.mat');


ind = 8
data = D.fttimelock;
data.dimord='chan_freq_time';
data.powspctrm = data.powspctrm(ind,:,:,:);
data.powspctrm = squeeze(data.powspctrm);

eval(['tfdata_' aa '_ftunshr = data']);
%

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

eval(['tfdata_' aa '_ftunshr.grad = cfg.grad']);

eval(['save tfdata_' aa '_ftunshr tfdata_' aa '_ftunshr']);

eval(['tfdata_' aa '_ftunshr_check = checkdata(tfdata_' aa '_ftunshr, ''senstype'',''neuromag306'')']);
eval(['tfdata_' aa '_ftunshr_check2 = checkdata(tfdata_' aa '_ftunshr_check, ''datatype'', ''freq'')']);
eval(['tfcomb_' aa '_ftunshr = ft_combineplanar(cfg, tfdata_' aa '_ftunshr)']);

eval(['save tfcomb_' aa '_ftunshr tfcomb_' aa '_ftunshr']);

end 



'done'

