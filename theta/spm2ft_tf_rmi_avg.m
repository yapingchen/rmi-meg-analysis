close all
clear all

% addpath(genpath('D:\0_EEG analysis\tf_script\'))
% addpath(genpath('D:\0_EEG analysis\fieldtrip\'))

cd 'F:\2018_RMI_theta\tf_group' % where the data is

%% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_gpshr.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_gpshr.mat'
load 'F:\2018_RMI_theta\s04\tfcomb_s04_gpshr.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_gpshr.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_gpshr.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_gpshr.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_gpshr.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_gpshr.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_gpshr.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_gpshr.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_gpshr.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_gpshr.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_gpshr.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_gpshr.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_gpshr.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_gpshr.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_gpshr.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_gpshr.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_gpshr_avg = ft_freqgrandaverage(cfg, tfcomb_s02_gpshr, tfcomb_s03_gpshr, tfcomb_s04_gpshr, tfcomb_s05_gpshr, tfcomb_s06_gpshr, tfcomb_s08_gpshr, tfcomb_s09_gpshr, tfcomb_s10_gpshr, tfcomb_s11_gpshr, tfcomb_s12_gpshr, tfcomb_s13_gpshr, tfcomb_s14_gpshr, tfcomb_s15_gpshr, tfcomb_s16_gpshr, tfcomb_s17_gpshr, tfcomb_s18_gpshr, tfcomb_s19_gpshr, tfcomb_s20_gpshr)
save tfcomb_gpshr_avg tfcomb_gpshr_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_gpunshr.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_gpunshr.mat'
load 'F:\2018_RMI_theta\s04\tfcomb_s04_gpunshr.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_gpunshr.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_gpunshr.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_gpunshr.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_gpunshr.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_gpunshr.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_gpunshr.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_gpunshr.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_gpunshr.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_gpunshr.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_gpunshr.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_gpunshr.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_gpunshr.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_gpunshr.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_gpunshr.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_gpunshr.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_gpunshr_avg = ft_freqgrandaverage(cfg, tfcomb_s02_gpunshr, tfcomb_s03_gpunshr, tfcomb_s04_gpunshr, tfcomb_s05_gpunshr, tfcomb_s06_gpunshr, tfcomb_s08_gpunshr, tfcomb_s09_gpunshr, tfcomb_s10_gpunshr, tfcomb_s11_gpunshr, tfcomb_s12_gpunshr, tfcomb_s13_gpunshr, tfcomb_s14_gpunshr, tfcomb_s15_gpunshr, tfcomb_s16_gpunshr, tfcomb_s17_gpunshr, tfcomb_s18_gpunshr, tfcomb_s19_gpunshr, tfcomb_s20_gpunshr)
save tfcomb_gpunshr_avg tfcomb_gpunshr_avg
clear all
%% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_sep.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_sep.mat'
load 'F:\2018_RMI_theta\s04\tfcomb_s04_sep.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_sep.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_sep.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_sep.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_sep.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_sep.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_sep.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_sep.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_sep.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_sep.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_sep.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_sep.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_sep.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_sep.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_sep.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_sep.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_sep_avg = ft_freqgrandaverage(cfg, tfcomb_s02_sep, tfcomb_s03_sep, tfcomb_s04_sep, tfcomb_s05_sep, tfcomb_s06_sep, tfcomb_s08_sep, tfcomb_s09_sep, tfcomb_s10_sep, tfcomb_s11_sep, tfcomb_s12_sep, tfcomb_s13_sep, tfcomb_s14_sep, tfcomb_s15_sep, tfcomb_s16_sep, tfcomb_s17_sep, tfcomb_s18_sep, tfcomb_s19_sep, tfcomb_s20_sep)
save tfcomb_sep_avg tfcomb_sep_avg
clear all
%%
% %% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_ftshr.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_ftshr.mat'
load 'F:\2018_RMI_theta\s04\tfcomb_s04_ftshr.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_ftshr.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_ftshr.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_ftshr.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_ftshr.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_ftshr.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_ftshr.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_ftshr.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_ftshr.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_ftshr.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_ftshr.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_ftshr.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_ftshr.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_ftshr.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_ftshr.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_ftshr.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_ftshr_avg = ft_freqgrandaverage(cfg, tfcomb_s02_ftshr, tfcomb_s03_ftshr, tfcomb_s04_ftshr, tfcomb_s05_ftshr, tfcomb_s06_ftshr, tfcomb_s08_ftshr, tfcomb_s09_ftshr, tfcomb_s10_ftshr, tfcomb_s11_ftshr, tfcomb_s12_ftshr, tfcomb_s13_ftshr, tfcomb_s14_ftshr, tfcomb_s15_ftshr, tfcomb_s16_ftshr, tfcomb_s17_ftshr, tfcomb_s18_ftshr, tfcomb_s19_ftshr, tfcomb_s20_ftshr)
save tfcomb_ftshr_avg tfcomb_ftshr_avg
clear all

% %% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_ftunshr.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_ftunshr.mat'
load 'F:\2018_RMI_theta\s04\tfcomb_s04_ftunshr.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_ftunshr.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_ftunshr.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_ftunshr.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_ftunshr.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_ftunshr.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_ftunshr.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_ftunshr.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_ftunshr.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_ftunshr.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_ftunshr.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_ftunshr.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_ftunshr.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_ftunshr.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_ftunshr.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_ftunshr.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_ftunshr_avg = ft_freqgrandaverage(cfg, tfcomb_s02_ftunshr, tfcomb_s03_ftunshr, tfcomb_s04_ftunshr, tfcomb_s05_ftunshr, tfcomb_s06_ftunshr, tfcomb_s08_ftunshr, tfcomb_s09_ftunshr, tfcomb_s10_ftunshr, tfcomb_s11_ftunshr, tfcomb_s12_ftunshr, tfcomb_s13_ftunshr, tfcomb_s14_ftunshr, tfcomb_s15_ftunshr, tfcomb_s16_ftunshr, tfcomb_s17_ftunshr, tfcomb_s18_ftunshr, tfcomb_s19_ftunshr, tfcomb_s20_ftunshr)
save tfcomb_ftunshr_avg tfcomb_ftunshr_avg
clear all
%% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_shr.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_shr.mat'
load 'F:\2018_RMI_theta\s04\tfcomb_s04_shr.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_shr.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_shr.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_shr.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_shr.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_shr.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_shr.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_shr.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_shr.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_shr.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_shr.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_shr.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_shr.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_shr.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_shr.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_shr.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_shr_avg = ft_freqgrandaverage(cfg, tfcomb_s02_shr, tfcomb_s03_shr, tfcomb_s04_shr, tfcomb_s05_shr, tfcomb_s06_shr, tfcomb_s08_shr, tfcomb_s09_shr, tfcomb_s10_shr, tfcomb_s11_shr, tfcomb_s12_shr, tfcomb_s13_shr, tfcomb_s14_shr, tfcomb_s15_shr, tfcomb_s16_shr, tfcomb_s17_shr, tfcomb_s18_shr, tfcomb_s19_shr, tfcomb_s20_shr)
save tfcomb_shr_avg tfcomb_shr_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_unshr.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_unshr.mat'
load 'F:\2018_RMI_theta\s04\tfcomb_s04_unshr.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_unshr.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_unshr.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_unshr.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_unshr.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_unshr.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_unshr.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_unshr.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_unshr.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_unshr.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_unshr.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_unshr.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_unshr.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_unshr.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_unshr.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_unshr.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_unshr_avg = ft_freqgrandaverage(cfg, tfcomb_s02_unshr, tfcomb_s03_unshr, tfcomb_s04_unshr, tfcomb_s05_unshr, tfcomb_s06_unshr, tfcomb_s08_unshr, tfcomb_s09_unshr, tfcomb_s10_unshr, tfcomb_s11_unshr, tfcomb_s12_unshr, tfcomb_s13_unshr, tfcomb_s14_unshr, tfcomb_s15_unshr, tfcomb_s16_unshr, tfcomb_s17_unshr, tfcomb_s18_unshr, tfcomb_s19_unshr, tfcomb_s20_unshr)
save tfcomb_unshr_avg tfcomb_unshr_avg
clear all

%% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_gp.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_gp.mat'
load 'F:\2018_RMI_theta\s04\tfcomb_s04_gp.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_gp.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_gp.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_gp.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_gp.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_gp.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_gp.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_gp.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_gp.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_gp.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_gp.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_gp.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_gp.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_gp.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_gp.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_gp.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_gp_avg = ft_freqgrandaverage(cfg, tfcomb_s02_gp, tfcomb_s03_gp, tfcomb_s04_gp, tfcomb_s05_gp, tfcomb_s06_gp, tfcomb_s08_gp, tfcomb_s09_gp, tfcomb_s10_gp, tfcomb_s11_gp, tfcomb_s12_gp, tfcomb_s13_gp, tfcomb_s14_gp, tfcomb_s15_gp, tfcomb_s16_gp, tfcomb_s17_gp, tfcomb_s18_gp, tfcomb_s19_gp, tfcomb_s20_gp)
save tfcomb_gp_avg tfcomb_gp_avg
clear all

%% 
load 'F:\2018_RMI_theta\s02\tfdata_s02_gpunshr.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_gpunshr.mat'
load 'F:\2018_RMI_theta\s04\tfdata_s04_gpunshr.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_gpunshr.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_gpunshr.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_gpunshr.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_gpunshr.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_gpunshr.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_gpunshr.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_gpunshr.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_gpunshr.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_gpunshr.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_gpunshr.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_gpunshr.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_gpunshr.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_gpunshr.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_gpunshr.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_gpunshr.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_gpunshr_avg = ft_freqgrandaverage(cfg, tfdata_s02_gpunshr, tfdata_s03_gpunshr, tfdata_s04_gpunshr, tfdata_s05_gpunshr, tfdata_s06_gpunshr, tfdata_s08_gpunshr, tfdata_s09_gpunshr, tfdata_s10_gpunshr, tfdata_s11_gpunshr, tfdata_s12_gpunshr, tfdata_s13_gpunshr, tfdata_s14_gpunshr, tfdata_s15_gpunshr, tfdata_s16_gpunshr, tfdata_s17_gpunshr, tfdata_s18_gpunshr, tfdata_s19_gpunshr, tfdata_s20_gpunshr)
save tfdata_gpunshr_avg tfdata_gpunshr_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfdata_s02_sep.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_sep.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_sep.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_sep.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_sep.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_sep.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_sep.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_sep.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_sep.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_sep.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_sep.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_sep.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_sep.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_sep.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_sep.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_sep.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_sep.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_sep.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_sep_avg = ft_freqgrandaverage(cfg, tfdata_s02_sep, tfdata_s03_sep, tfdata_s05_sep, tfdata_s06_sep, tfdata_s07_sep, tfdata_s08_sep, tfdata_s09_sep, tfdata_s10_sep, tfdata_s11_sep, tfdata_s12_sep, tfdata_s13_sep, tfdata_s14_sep, tfdata_s15_sep, tfdata_s16_sep, tfdata_s17_sep, tfdata_s18_sep, tfdata_s19_sep, tfdata_s20_sep)
save tfdata_sep_avg tfdata_sep_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfdata_s02_gp.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_gp.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_gp.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_gp.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_gp.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_gp.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_gp.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_gp.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_gp.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_gp.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_gp.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_gp.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_gp.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_gp.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_gp.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_gp.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_gp.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_gp.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_gp_avg = ft_freqgrandaverage(cfg, tfdata_s02_gp, tfdata_s03_gp, tfdata_s05_gp, tfdata_s06_gp, tfdata_s07_gp, tfdata_s08_gp, tfdata_s09_gp, tfdata_s10_gp, tfdata_s11_gp, tfdata_s12_gp, tfdata_s13_gp, tfdata_s14_gp, tfdata_s15_gp, tfdata_s16_gp, tfdata_s17_gp, tfdata_s18_gp, tfdata_s19_gp, tfdata_s20_gp)
save tfdata_gp_avg tfdata_gp_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfdata_s02_unshr.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_unshr.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_unshr.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_unshr.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_unshr.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_unshr.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_unshr.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_unshr.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_unshr.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_unshr.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_unshr.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_unshr.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_unshr.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_unshr.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_unshr.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_unshr.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_unshr.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_unshr.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_unshr_avg = ft_freqgrandaverage(cfg, tfdata_s02_unshr, tfdata_s03_unshr, tfdata_s05_unshr, tfdata_s06_unshr, tfdata_s07_unshr, tfdata_s08_unshr, tfdata_s09_unshr, tfdata_s10_unshr, tfdata_s11_unshr, tfdata_s12_unshr, tfdata_s13_unshr, tfdata_s14_unshr, tfdata_s15_unshr, tfdata_s16_unshr, tfdata_s17_unshr, tfdata_s18_unshr, tfdata_s19_unshr, tfdata_s20_unshr)
save tfdata_unshr_avg tfdata_unshr_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfdata_s02_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_gpunshr_L.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_gpunshr_L_avg = ft_freqgrandaverage(cfg, tfdata_s02_gpunshr_L, tfdata_s03_gpunshr_L, tfdata_s05_gpunshr_L, tfdata_s06_gpunshr_L, tfdata_s07_gpunshr_L, tfdata_s08_gpunshr_L, tfdata_s09_gpunshr_L, tfdata_s10_gpunshr_L, tfdata_s11_gpunshr_L, tfdata_s12_gpunshr_L, tfdata_s13_gpunshr_L, tfdata_s14_gpunshr_L, tfdata_s15_gpunshr_L, tfdata_s16_gpunshr_L, tfdata_s17_gpunshr_L, tfdata_s18_gpunshr_L, tfdata_s19_gpunshr_L, tfdata_s20_gpunshr_L)
save tfdata_gpunshr_L_avg tfdata_gpunshr_L_avg
clear all
%%
%%
load 'F:\2018_RMI_theta\s02\tfdata_s02_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_gpunshr_R.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_gpunshr_R_avg = ft_freqgrandaverage(cfg, tfdata_s02_gpunshr_R, tfdata_s03_gpunshr_R, tfdata_s05_gpunshr_R, tfdata_s06_gpunshr_R, tfdata_s07_gpunshr_R, tfdata_s08_gpunshr_R, tfdata_s09_gpunshr_R, tfdata_s10_gpunshr_R, tfdata_s11_gpunshr_R, tfdata_s12_gpunshr_R, tfdata_s13_gpunshr_R, tfdata_s14_gpunshr_R, tfdata_s15_gpunshr_R, tfdata_s16_gpunshr_R, tfdata_s17_gpunshr_R, tfdata_s18_gpunshr_R, tfdata_s19_gpunshr_R, tfdata_s20_gpunshr_R)
save tfdata_gpunshr_R_avg tfdata_gpunshr_R_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfdata_s02_sep_L.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_sep_L.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_sep_L.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_sep_L.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_sep_L.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_sep_L.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_sep_L.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_sep_L.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_sep_L.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_sep_L.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_sep_L.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_sep_L.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_sep_L.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_sep_L.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_sep_L.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_sep_L.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_sep_L.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_sep_L.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_sep_L_avg = ft_freqgrandaverage(cfg, tfdata_s02_sep_L, tfdata_s03_sep_L, tfdata_s05_sep_L, tfdata_s06_sep_L, tfdata_s07_sep_L, tfdata_s08_sep_L, tfdata_s09_sep_L, tfdata_s10_sep_L, tfdata_s11_sep_L, tfdata_s12_sep_L, tfdata_s13_sep_L, tfdata_s14_sep_L, tfdata_s15_sep_L, tfdata_s16_sep_L, tfdata_s17_sep_L, tfdata_s18_sep_L, tfdata_s19_sep_L, tfdata_s20_sep_L)
save tfdata_sep_L_avg tfdata_sep_L_avg
clear all
%%
%%
load 'F:\2018_RMI_theta\s02\tfdata_s02_sep_R.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_sep_R.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_sep_R.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_sep_R.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_sep_R.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_sep_R.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_sep_R.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_sep_R.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_sep_R.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_sep_R.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_sep_R.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_sep_R.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_sep_R.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_sep_R.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_sep_R.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_sep_R.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_sep_R.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_sep_R.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_sep_R_avg = ft_freqgrandaverage(cfg, tfdata_s02_sep_R, tfdata_s03_sep_R, tfdata_s05_sep_R, tfdata_s06_sep_R, tfdata_s07_sep_R, tfdata_s08_sep_R, tfdata_s09_sep_R, tfdata_s10_sep_R, tfdata_s11_sep_R, tfdata_s12_sep_R, tfdata_s13_sep_R, tfdata_s14_sep_R, tfdata_s15_sep_R, tfdata_s16_sep_R, tfdata_s17_sep_R, tfdata_s18_sep_R, tfdata_s19_sep_R, tfdata_s20_sep_R)
save tfdata_sep_R_avg tfdata_sep_R_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfdata_s02_gp_L.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_gp_L.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_gp_L.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_gp_L.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_gp_L.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_gp_L.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_gp_L.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_gp_L.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_gp_L.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_gp_L.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_gp_L.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_gp_L.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_gp_L.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_gp_L.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_gp_L.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_gp_L.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_gp_L.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_gp_L.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_gp_L_avg = ft_freqgrandaverage(cfg, tfdata_s02_gp_L, tfdata_s03_gp_L, tfdata_s05_gp_L, tfdata_s06_gp_L, tfdata_s07_gp_L, tfdata_s08_gp_L, tfdata_s09_gp_L, tfdata_s10_gp_L, tfdata_s11_gp_L, tfdata_s12_gp_L, tfdata_s13_gp_L, tfdata_s14_gp_L, tfdata_s15_gp_L, tfdata_s16_gp_L, tfdata_s17_gp_L, tfdata_s18_gp_L, tfdata_s19_gp_L, tfdata_s20_gp_L)
save tfdata_gp_L_avg tfdata_gp_L_avg
clear all
%%
%%
load 'F:\2018_RMI_theta\s02\tfdata_s02_gp_R.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_gp_R.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_gp_R.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_gp_R.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_gp_R.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_gp_R.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_gp_R.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_gp_R.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_gp_R.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_gp_R.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_gp_R.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_gp_R.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_gp_R.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_gp_R.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_gp_R.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_gp_R.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_gp_R.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_gp_R.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_gp_R_avg = ft_freqgrandaverage(cfg, tfdata_s02_gp_R, tfdata_s03_gp_R, tfdata_s05_gp_R, tfdata_s06_gp_R, tfdata_s07_gp_R, tfdata_s08_gp_R, tfdata_s09_gp_R, tfdata_s10_gp_R, tfdata_s11_gp_R, tfdata_s12_gp_R, tfdata_s13_gp_R, tfdata_s14_gp_R, tfdata_s15_gp_R, tfdata_s16_gp_R, tfdata_s17_gp_R, tfdata_s18_gp_R, tfdata_s19_gp_R, tfdata_s20_gp_R)
save tfdata_gp_R_avg tfdata_gp_R_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfdata_s02_unshr_L.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_unshr_L.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_unshr_L.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_unshr_L.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_unshr_L.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_unshr_L.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_unshr_L.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_unshr_L.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_unshr_L.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_unshr_L.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_unshr_L.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_unshr_L.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_unshr_L.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_unshr_L.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_unshr_L.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_unshr_L.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_unshr_L.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_unshr_L.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_unshr_L_avg = ft_freqgrandaverage(cfg, tfdata_s02_unshr_L, tfdata_s03_unshr_L, tfdata_s05_unshr_L, tfdata_s06_unshr_L, tfdata_s07_unshr_L, tfdata_s08_unshr_L, tfdata_s09_unshr_L, tfdata_s10_unshr_L, tfdata_s11_unshr_L, tfdata_s12_unshr_L, tfdata_s13_unshr_L, tfdata_s14_unshr_L, tfdata_s15_unshr_L, tfdata_s16_unshr_L, tfdata_s17_unshr_L, tfdata_s18_unshr_L, tfdata_s19_unshr_L, tfdata_s20_unshr_L)
save tfdata_unshr_L_avg tfdata_unshr_L_avg
clear all
%%
%%
load 'F:\2018_RMI_theta\s02\tfdata_s02_unshr_R.mat'
load 'F:\2018_RMI_theta\s03\tfdata_s03_unshr_R.mat'
load 'F:\2018_RMI_theta\s05\tfdata_s05_unshr_R.mat'
load 'F:\2018_RMI_theta\s06\tfdata_s06_unshr_R.mat'
load 'F:\2018_RMI_theta\s07\tfdata_s07_unshr_R.mat'
load 'F:\2018_RMI_theta\s08\tfdata_s08_unshr_R.mat'
load 'F:\2018_RMI_theta\s09\tfdata_s09_unshr_R.mat'
load 'F:\2018_RMI_theta\s10\tfdata_s10_unshr_R.mat'
load 'F:\2018_RMI_theta\s11\tfdata_s11_unshr_R.mat'
load 'F:\2018_RMI_theta\s12\tfdata_s12_unshr_R.mat'
load 'F:\2018_RMI_theta\s13\tfdata_s13_unshr_R.mat'
load 'F:\2018_RMI_theta\s14\tfdata_s14_unshr_R.mat'
load 'F:\2018_RMI_theta\s15\tfdata_s15_unshr_R.mat'
load 'F:\2018_RMI_theta\s16\tfdata_s16_unshr_R.mat'
load 'F:\2018_RMI_theta\s17\tfdata_s17_unshr_R.mat'
load 'F:\2018_RMI_theta\s18\tfdata_s18_unshr_R.mat'
load 'F:\2018_RMI_theta\s19\tfdata_s19_unshr_R.mat'
load 'F:\2018_RMI_theta\s20\tfdata_s20_unshr_R.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfdata_unshr_R_avg = ft_freqgrandaverage(cfg, tfdata_s02_unshr_R, tfdata_s03_unshr_R, tfdata_s05_unshr_R, tfdata_s06_unshr_R, tfdata_s07_unshr_R, tfdata_s08_unshr_R, tfdata_s09_unshr_R, tfdata_s10_unshr_R, tfdata_s11_unshr_R, tfdata_s12_unshr_R, tfdata_s13_unshr_R, tfdata_s14_unshr_R, tfdata_s15_unshr_R, tfdata_s16_unshr_R, tfdata_s17_unshr_R, tfdata_s18_unshr_R, tfdata_s19_unshr_R, tfdata_s20_unshr_R)
save tfdata_unshr_R_avg tfdata_unshr_R_avg
clear all
%%











%%
%% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s07\tfcomb_s07_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_gpunshr_L.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_gpunshr_L.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_gpunshr_L_avg = ft_freqgrandaverage(cfg, tfcomb_s02_gpunshr_L, tfcomb_s03_gpunshr_L, tfcomb_s05_gpunshr_L, tfcomb_s06_gpunshr_L, tfcomb_s07_gpunshr_L, tfcomb_s08_gpunshr_L, tfcomb_s09_gpunshr_L, tfcomb_s10_gpunshr_L, tfcomb_s11_gpunshr_L, tfcomb_s12_gpunshr_L, tfcomb_s13_gpunshr_L, tfcomb_s14_gpunshr_L, tfcomb_s15_gpunshr_L, tfcomb_s16_gpunshr_L, tfcomb_s17_gpunshr_L, tfcomb_s18_gpunshr_L, tfcomb_s19_gpunshr_L, tfcomb_s20_gpunshr_L)
save tfcomb_gpunshr_L_avg tfcomb_gpunshr_L_avg
clear all
%%
%%
load 'F:\2018_RMI_theta\s02\tfcomb_s02_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s07\tfcomb_s07_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_gpunshr_R.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_gpunshr_R.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_gpunshr_R_avg = ft_freqgrandaverage(cfg, tfcomb_s02_gpunshr_R, tfcomb_s03_gpunshr_R, tfcomb_s05_gpunshr_R, tfcomb_s06_gpunshr_R, tfcomb_s07_gpunshr_R, tfcomb_s08_gpunshr_R, tfcomb_s09_gpunshr_R, tfcomb_s10_gpunshr_R, tfcomb_s11_gpunshr_R, tfcomb_s12_gpunshr_R, tfcomb_s13_gpunshr_R, tfcomb_s14_gpunshr_R, tfcomb_s15_gpunshr_R, tfcomb_s16_gpunshr_R, tfcomb_s17_gpunshr_R, tfcomb_s18_gpunshr_R, tfcomb_s19_gpunshr_R, tfcomb_s20_gpunshr_R)
save tfcomb_gpunshr_R_avg tfcomb_gpunshr_R_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_sep_L.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_sep_L.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_sep_L.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_sep_L.mat'
load 'F:\2018_RMI_theta\s07\tfcomb_s07_sep_L.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_sep_L.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_sep_L.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_sep_L.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_sep_L.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_sep_L.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_sep_L.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_sep_L.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_sep_L.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_sep_L.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_sep_L.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_sep_L.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_sep_L.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_sep_L.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_sep_L_avg = ft_freqgrandaverage(cfg, tfcomb_s02_sep_L, tfcomb_s03_sep_L, tfcomb_s05_sep_L, tfcomb_s06_sep_L, tfcomb_s07_sep_L, tfcomb_s08_sep_L, tfcomb_s09_sep_L, tfcomb_s10_sep_L, tfcomb_s11_sep_L, tfcomb_s12_sep_L, tfcomb_s13_sep_L, tfcomb_s14_sep_L, tfcomb_s15_sep_L, tfcomb_s16_sep_L, tfcomb_s17_sep_L, tfcomb_s18_sep_L, tfcomb_s19_sep_L, tfcomb_s20_sep_L)
save tfcomb_sep_L_avg tfcomb_sep_L_avg
clear all
%%
%%
load 'F:\2018_RMI_theta\s02\tfcomb_s02_sep_R.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_sep_R.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_sep_R.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_sep_R.mat'
load 'F:\2018_RMI_theta\s07\tfcomb_s07_sep_R.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_sep_R.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_sep_R.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_sep_R.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_sep_R.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_sep_R.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_sep_R.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_sep_R.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_sep_R.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_sep_R.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_sep_R.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_sep_R.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_sep_R.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_sep_R.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_sep_R_avg = ft_freqgrandaverage(cfg, tfcomb_s02_sep_R, tfcomb_s03_sep_R, tfcomb_s05_sep_R, tfcomb_s06_sep_R, tfcomb_s07_sep_R, tfcomb_s08_sep_R, tfcomb_s09_sep_R, tfcomb_s10_sep_R, tfcomb_s11_sep_R, tfcomb_s12_sep_R, tfcomb_s13_sep_R, tfcomb_s14_sep_R, tfcomb_s15_sep_R, tfcomb_s16_sep_R, tfcomb_s17_sep_R, tfcomb_s18_sep_R, tfcomb_s19_sep_R, tfcomb_s20_sep_R)
save tfcomb_sep_R_avg tfcomb_sep_R_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_gp_L.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_gp_L.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_gp_L.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_gp_L.mat'
load 'F:\2018_RMI_theta\s07\tfcomb_s07_gp_L.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_gp_L.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_gp_L.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_gp_L.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_gp_L.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_gp_L.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_gp_L.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_gp_L.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_gp_L.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_gp_L.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_gp_L.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_gp_L.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_gp_L.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_gp_L.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_gp_L_avg = ft_freqgrandaverage(cfg, tfcomb_s02_gp_L, tfcomb_s03_gp_L, tfcomb_s05_gp_L, tfcomb_s06_gp_L, tfcomb_s07_gp_L, tfcomb_s08_gp_L, tfcomb_s09_gp_L, tfcomb_s10_gp_L, tfcomb_s11_gp_L, tfcomb_s12_gp_L, tfcomb_s13_gp_L, tfcomb_s14_gp_L, tfcomb_s15_gp_L, tfcomb_s16_gp_L, tfcomb_s17_gp_L, tfcomb_s18_gp_L, tfcomb_s19_gp_L, tfcomb_s20_gp_L)
save tfcomb_gp_L_avg tfcomb_gp_L_avg
clear all
%%
%%
load 'F:\2018_RMI_theta\s02\tfcomb_s02_gp_R.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_gp_R.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_gp_R.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_gp_R.mat'
load 'F:\2018_RMI_theta\s07\tfcomb_s07_gp_R.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_gp_R.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_gp_R.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_gp_R.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_gp_R.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_gp_R.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_gp_R.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_gp_R.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_gp_R.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_gp_R.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_gp_R.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_gp_R.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_gp_R.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_gp_R.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_gp_R_avg = ft_freqgrandaverage(cfg, tfcomb_s02_gp_R, tfcomb_s03_gp_R, tfcomb_s05_gp_R, tfcomb_s06_gp_R, tfcomb_s07_gp_R, tfcomb_s08_gp_R, tfcomb_s09_gp_R, tfcomb_s10_gp_R, tfcomb_s11_gp_R, tfcomb_s12_gp_R, tfcomb_s13_gp_R, tfcomb_s14_gp_R, tfcomb_s15_gp_R, tfcomb_s16_gp_R, tfcomb_s17_gp_R, tfcomb_s18_gp_R, tfcomb_s19_gp_R, tfcomb_s20_gp_R)
save tfcomb_gp_R_avg tfcomb_gp_R_avg
clear all
%%
%% 
load 'F:\2018_RMI_theta\s02\tfcomb_s02_unshr_L.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_unshr_L.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_unshr_L.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_unshr_L.mat'
load 'F:\2018_RMI_theta\s07\tfcomb_s07_unshr_L.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_unshr_L.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_unshr_L.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_unshr_L.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_unshr_L.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_unshr_L.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_unshr_L.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_unshr_L.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_unshr_L.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_unshr_L.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_unshr_L.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_unshr_L.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_unshr_L.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_unshr_L.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_unshr_L_avg = ft_freqgrandaverage(cfg, tfcomb_s02_unshr_L, tfcomb_s03_unshr_L, tfcomb_s05_unshr_L, tfcomb_s06_unshr_L, tfcomb_s07_unshr_L, tfcomb_s08_unshr_L, tfcomb_s09_unshr_L, tfcomb_s10_unshr_L, tfcomb_s11_unshr_L, tfcomb_s12_unshr_L, tfcomb_s13_unshr_L, tfcomb_s14_unshr_L, tfcomb_s15_unshr_L, tfcomb_s16_unshr_L, tfcomb_s17_unshr_L, tfcomb_s18_unshr_L, tfcomb_s19_unshr_L, tfcomb_s20_unshr_L)
save tfcomb_unshr_L_avg tfcomb_unshr_L_avg
clear all
%%
%%
load 'F:\2018_RMI_theta\s02\tfcomb_s02_unshr_R.mat'
load 'F:\2018_RMI_theta\s03\tfcomb_s03_unshr_R.mat'
load 'F:\2018_RMI_theta\s05\tfcomb_s05_unshr_R.mat'
load 'F:\2018_RMI_theta\s06\tfcomb_s06_unshr_R.mat'
load 'F:\2018_RMI_theta\s07\tfcomb_s07_unshr_R.mat'
load 'F:\2018_RMI_theta\s08\tfcomb_s08_unshr_R.mat'
load 'F:\2018_RMI_theta\s09\tfcomb_s09_unshr_R.mat'
load 'F:\2018_RMI_theta\s10\tfcomb_s10_unshr_R.mat'
load 'F:\2018_RMI_theta\s11\tfcomb_s11_unshr_R.mat'
load 'F:\2018_RMI_theta\s12\tfcomb_s12_unshr_R.mat'
load 'F:\2018_RMI_theta\s13\tfcomb_s13_unshr_R.mat'
load 'F:\2018_RMI_theta\s14\tfcomb_s14_unshr_R.mat'
load 'F:\2018_RMI_theta\s15\tfcomb_s15_unshr_R.mat'
load 'F:\2018_RMI_theta\s16\tfcomb_s16_unshr_R.mat'
load 'F:\2018_RMI_theta\s17\tfcomb_s17_unshr_R.mat'
load 'F:\2018_RMI_theta\s18\tfcomb_s18_unshr_R.mat'
load 'F:\2018_RMI_theta\s19\tfcomb_s19_unshr_R.mat'
load 'F:\2018_RMI_theta\s20\tfcomb_s20_unshr_R.mat'

cfg = [];
cfg.keepindividual = 'yes' ;
tfcomb_unshr_R_avg = ft_freqgrandaverage(cfg, tfcomb_s02_unshr_R, tfcomb_s03_unshr_R, tfcomb_s05_unshr_R, tfcomb_s06_unshr_R, tfcomb_s07_unshr_R, tfcomb_s08_unshr_R, tfcomb_s09_unshr_R, tfcomb_s10_unshr_R, tfcomb_s11_unshr_R, tfcomb_s12_unshr_R, tfcomb_s13_unshr_R, tfcomb_s14_unshr_R, tfcomb_s15_unshr_R, tfcomb_s16_unshr_R, tfcomb_s17_unshr_R, tfcomb_s18_unshr_R, tfcomb_s19_unshr_R, tfcomb_s20_unshr_R)
save tfcomb_unshr_R_avg tfcomb_unshr_R_avg
clear all
%%
'done'
%%




%% plotting - rescale data
close all
clear all

rmpath(genpath('D:\0_EEG analysis\fieldtrip\'))
addpath(genpath('D:\0_EEG analysis\fieldtrip-20140611\'))

load tfcomb_gpunshr_avg
load tfcomb_sep_avg
load tfcomb_gp_avg
load tfcomb_unshr_avg

cfg = [];
%cfg.baseline	= [-0.3 0]; % BASELINE IS -500ms TO 0ms 	        
cfg.zlim       = [-5 5];	% power (ie colour scale bar in plots)
%cfg.showlabels = 'markers';	  
%cfg.markersymbol = '.';

cfg.colorbar = 'yes';
cfg.interactive = 'yes';
cfg.layout = 'D:\0_EEG analysis\fieldtrip-20140611\template\layout\neuromag306cmb.lay';   
cfg.xlim = [-0.2 0.8]; % time
cfg.ylim = [1 20]; % frequemoy (Hz)
%cfg.baselinetype = 'absolute';
figure(1); ft_multiplotTFR(cfg, tfcomb_gpunshr_avg);
figure(2); ft_multiplotTFR(cfg, tfcomb_sep_avg);

diff_gpunshr_sep = tfcomb_gpunshr_avg;
diff_gpunshr_sep.powspctrm = tfcomb_gpunshr_avg.powspctrm - tfcomb_sep_avg.powspctrm;
figure(3); ft_multiplotTFR(cfg, diff_gpunshr_sep);

diff_gp_unshr = tfcomb_gp_avg;
diff_gp_unshr.powspctrm = tfcomb_gp_avg.powspctrm - tfcomb_unshr_avg.powspctrm;
figure(4); ft_multiplotTFR(cfg, diff_gp_unshr);

%%
%% plotting - rescale data
close all
clear all

rmpath(genpath('D:\0_EEG analysis\fieldtrip\'))
addpath(genpath('D:\0_EEG analysis\fieldtrip-20140611\'))

load tfcomb_gpunshr_L_avg
load tfcomb_gpunshr_R_avg
load tfcomb_sep_L_avg
load tfcomb_sep_R_avg

load tfcomb_gp_L_avg
load tfcomb_gp_R_avg
load tfcomb_unshr_L_avg
load tfcomb_unshr_R_avg

cfg = [];
%cfg.baseline	= [-0.3 0]; % BASELINE IS -500ms TO 0ms 	        
cfg.zlim       = [-5 5];	% power (ie colour scale bar in plots)
%cfg.showlabels = 'markers';	  
%cfg.markersymbol = '.';

cfg.colorbar = 'yes';
cfg.interactive = 'yes';
cfg.layout = 'D:\0_EEG analysis\fieldtrip-20140611\template\layout\neuromag306cmb.lay';   
cfg.xlim = [-0.2 0.8]; % time
cfg.ylim = [3 20]; % frequemoy (Hz)
%cfg.baselinetype = 'absolute';

diff_gpunshr_L_R = tfcomb_gpunshr_L_avg;
diff_gpunshr_L_R.powspctrm = tfcomb_gpunshr_L_avg.powspctrm - tfcomb_gpunshr_R_avg.powspctrm;
figure(1); ft_multiplotTFR(cfg, diff_gpunshr_L_R);

diff_sep_L_R = tfcomb_sep_L_avg;
diff_sep_L_R.powspctrm = tfcomb_sep_L_avg.powspctrm - tfcomb_sep_R_avg.powspctrm;
figure(2); ft_multiplotTFR(cfg, diff_sep_L_R);

diff_gp_L_R = tfcomb_gp_L_avg;
diff_gp_L_R.powspctrm = tfcomb_gp_L_avg.powspctrm - tfcomb_gp_R_avg.powspctrm;
figure(3); ft_multiplotTFR(cfg, diff_gp_L_R);

diff_unshr_L_R = tfcomb_unshr_L_avg;
diff_unshr_L_R.powspctrm = tfcomb_unshr_L_avg.powspctrm - tfcomb_unshr_R_avg.powspctrm;
figure(4); ft_multiplotTFR(cfg, diff_unshr_L_R);

diff_gpunshr_sep_L_R = tfcomb_gpunshr_L_avg;
diff_gpunshr_sep_L_R.powspctrm = diff_gpunshr_L_R.powspctrm - diff_sep_L_R.powspctrm;
figure(5); ft_multiplotTFR(cfg, diff_gpunshr_sep_L_R);

diff_gp_unshr_L_R = tfcomb_gp_L_avg;
diff_gp_unshr_L_R.powspctrm = diff_gp_L_R.powspctrm - diff_unshr_L_R.powspctrm;
figure(6); ft_multiplotTFR(cfg, diff_gp_unshr_L_R);



%%












