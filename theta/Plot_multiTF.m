%%
load tfcomb_gp_avg
load tfcomb_sep_avg

gp_sep_avg = tfcomb_gp_avg;
gp_sep_avg.powspctrm = squeeze(mean(tfcomb_gp_avg.powspctrm,1)) - squeeze(mean(tfcomb_sep_avg.powspctrm,1));
gp_sep_avg.dimord = 'chan_freq_time';

cfg = [];
cfg.xlim = [-0.3 0.8];
cfg.zlim = [-2 2];
% cfg.baselinetype = 'absolute';
cfg.colorbar = 'yes';
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';   
figure; ft_multiplotTFR(cfg,gp_sep_avg);

%%

load tfcomb_gpshr_avg
load tfcomb_gpunshr_avg

gpshr_gpunshr_avg = tfcomb_gpshr_avg;
gpshr_gpunshr_avg.powspctrm = squeeze(mean(tfcomb_gpshr_avg.powspctrm,1)) - squeeze(mean(tfcomb_gpunshr_avg.powspctrm,1));
gpshr_gpunshr_avg.dimord = 'chan_freq_time';

cfg = [];
cfg.xlim = [-0.3 0.8];
cfg.zlim = [-2 2];
% cfg.baselinetype = 'absolute';
cfg.colorbar = 'yes';
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';   
figure; ft_multiplotTFR(cfg,gpshr_gpunshr_avg);



%%
load tfcomb_ftshr_avg
load tfcomb_ftunshr_avg

ftshr_ftunshr_avg = tfcomb_ftshr_avg;
ftshr_ftunshr_avg.powspctrm = squeeze(mean(tfcomb_ftshr_avg.powspctrm,1)) - squeeze(mean(tfcomb_ftunshr_avg.powspctrm,1));
ftshr_ftunshr_avg.dimord = 'chan_freq_time';

cfg = [];
cfg.xlim = [-0.3 0.8];
cfg.zlim = [-2 2];
% cfg.baselinetype = 'absolute';
cfg.colorbar = 'yes';
cfg.layout      = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';   
figure; ft_multiplotTFR(cfg,ftshr_ftunshr_avg);