clear all
close all

clc
cd 'F:\2017_RMI_MEG\N2pc'

%%% Lsh_co
cd 'F:\2017_RMI_MEG\N2pc\sub20'

D = spm_eeg_load ('wmrcpbefdspm8_run1raw_trans_sss.mat');
erf_co_sub20_Lsh = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 1
    erf_co_sub20_Lsh.trial = erf_co_sub20_Lsh.trial(ind);
    erf_co_sub20_Lsh.time =  erf_co_sub20_Lsh.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub20_Lsh.grad = cfg.grad;
erf_co_sub20_Lsh_data = ft_timelockanalysis([], erf_co_sub20_Lsh);

save erf_co_sub20_Lsh_data erf_co_sub20_Lsh_data

erf_co_sub20_Lsh_check = ft_checkdata(erf_co_sub20_Lsh_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
% cfg.combinemethod  = 'sum';
erf_co_sub20_Lsh_comb = ft_combineplanar(cfg, erf_co_sub20_Lsh_check);

save erf_co_sub20_Lsh_comb erf_co_sub20_Lsh_comb
clear all;


%%% Rsh_co

D = spm_eeg_load ('Mwmrcpbefdspm8_run1raw_trans_sss.mat');
erf_co_sub20_Rsh = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 2
    erf_co_sub20_Rsh.trial = erf_co_sub20_Rsh.trial(ind);
    erf_co_sub20_Rsh.time =  erf_co_sub20_Rsh.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub20_Rsh.grad = cfg.grad;
erf_co_sub20_Rsh_data = ft_timelockanalysis([], erf_co_sub20_Rsh);

save erf_co_sub20_Rsh_data erf_co_sub20_Rsh_data

erf_co_sub20_Rsh_check = ft_checkdata(erf_co_sub20_Rsh_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_co_sub20_Rsh_comb = ft_combineplanar(cfg, erf_co_sub20_Rsh_check);

save erf_co_sub20_Rsh_comb erf_co_sub20_Rsh_comb
clear all;

%%% Luns_co

D = spm_eeg_load ('wmrcpbefdspm8_run1raw_trans_sss.mat');
erf_co_sub20_Luns = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 3
    erf_co_sub20_Luns.trial = erf_co_sub20_Luns.trial(ind);
    erf_co_sub20_Luns.time =  erf_co_sub20_Luns.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub20_Luns.grad = cfg.grad;
erf_co_sub20_Luns_data = ft_timelockanalysis([], erf_co_sub20_Luns);

save erf_co_sub20_Luns_data erf_co_sub20_Luns_data

erf_co_sub20_Luns_check = ft_checkdata(erf_co_sub20_Luns_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_co_sub20_Luns_comb = ft_combineplanar(cfg, erf_co_sub20_Luns_check);

save erf_co_sub20_Luns_comb erf_co_sub20_Luns_comb
clear all;


%%% Runs_co

D = spm_eeg_load ('Mwmrcpbefdspm8_run1raw_trans_sss.mat');
erf_co_sub20_Runs = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 4
    erf_co_sub20_Runs.trial = erf_co_sub20_Runs.trial(ind);
    erf_co_sub20_Runs.time =  erf_co_sub20_Runs.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub20_Runs.grad = cfg.grad;
erf_co_sub20_Runs_data = ft_timelockanalysis([], erf_co_sub20_Runs);

save erf_co_sub20_Runs_data erf_co_sub20_Runs_data

erf_co_sub20_Runs_check = ft_checkdata(erf_co_sub20_Runs_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_co_sub20_Runs_comb = ft_combineplanar(cfg, erf_co_sub20_Runs_check);

save erf_co_sub20_Runs_comb erf_co_sub20_Runs_comb
clear all;



%%% Lgp_co

D = spm_eeg_load ('wmrcpbefdspm8_run1raw_trans_sss.mat');
erf_co_sub20_Lgp = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 5
    erf_co_sub20_Lgp.trial = erf_co_sub20_Lgp.trial(ind);
    erf_co_sub20_Lgp.time =  erf_co_sub20_Lgp.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub20_Lgp.grad = cfg.grad;
erf_co_sub20_Lgp_data = ft_timelockanalysis([], erf_co_sub20_Lgp);

save erf_co_sub20_Lgp_data erf_co_sub20_Lgp_data

erf_co_sub20_Lgp_check = ft_checkdata(erf_co_sub20_Lgp_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_co_sub20_Lgp_comb = ft_combineplanar(cfg, erf_co_sub20_Lgp_check);

save erf_co_sub20_Lgp_comb erf_co_sub20_Lgp_comb
clear all;


%%% Rgp_co

D = spm_eeg_load ('Mwmrcpbefdspm8_run1raw_trans_sss.mat');
erf_co_sub20_Rgp = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 6
    erf_co_sub20_Rgp.trial = erf_co_sub20_Rgp.trial(ind);
    erf_co_sub20_Rgp.time =  erf_co_sub20_Rgp.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub20_Rgp.grad = cfg.grad;
erf_co_sub20_Rgp_data = ft_timelockanalysis([], erf_co_sub20_Rgp);

save erf_co_sub20_Rgp_data erf_co_sub20_Rgp_data

erf_co_sub20_Rgp_check = ft_checkdata(erf_co_sub20_Rgp_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_co_sub20_Rgp_comb = ft_combineplanar(cfg, erf_co_sub20_Rgp_check);

save erf_co_sub20_Rgp_comb erf_co_sub20_Rgp_comb
clear all;

%%% Lsep_co

D = spm_eeg_load ('wmrcpbefdspm8_run1raw_trans_sss.mat');
erf_co_sub20_Lsep = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 7
    erf_co_sub20_Lsep.trial = erf_co_sub20_Lsep.trial(ind);
    erf_co_sub20_Lsep.time =  erf_co_sub20_Lsep.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub20_Lsep.grad = cfg.grad;
erf_co_sub20_Lsep_data = ft_timelockanalysis([], erf_co_sub20_Lsep);

save erf_co_sub20_Lsep_data erf_co_sub20_Lsep_data

erf_co_sub20_Lsep_check = ft_checkdata(erf_co_sub20_Lsep_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_co_sub20_Lsep_comb = ft_combineplanar(cfg, erf_co_sub20_Lsep_check);

save erf_co_sub20_Lsep_comb erf_co_sub20_Lsep_comb
clear all;


%%% Rsep_co

D = spm_eeg_load ('Mwmrcpbefdspm8_run1raw_trans_sss.mat');
erf_co_sub20_Rsep = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 8
    erf_co_sub20_Rsep.trial = erf_co_sub20_Rsep.trial(ind);
    erf_co_sub20_Rsep.time =  erf_co_sub20_Rsep.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub20_Rsep.grad = cfg.grad;
erf_co_sub20_Rsep_data = ft_timelockanalysis([], erf_co_sub20_Rsep);

save erf_co_sub20_Rsep_data erf_co_sub20_Rsep_data

erf_co_sub20_Rsep_check = ft_checkdata(erf_co_sub20_Rsep_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_co_sub20_Rsep_comb = ft_combineplanar(cfg, erf_co_sub20_Rsep_check);

save erf_co_sub20_Rsep_comb erf_co_sub20_Rsep_comb
clear all;



%%%%%
close all
clear all

load erf_co_sub20_Lsh_data
load erf_co_sub20_Rsh_data

erf_co_sub20_sh_data = erf_co_sub20_Lsh_data;
erf_co_sub20_sh_data.avg = (erf_co_sub20_Lsh_data.avg + erf_co_sub20_Rsh_data.avg)./2;
save erf_co_sub20_sh_data erf_co_sub20_sh_data


close all
clear all

load erf_co_sub20_Luns_data
load erf_co_sub20_Runs_data

erf_co_sub20_uns_data = erf_co_sub20_Luns_data;
erf_co_sub20_uns_data.avg = (erf_co_sub20_Luns_data.avg + erf_co_sub20_Runs_data.avg)./2;
save erf_co_sub20_uns_data erf_co_sub20_uns_data


close all
clear all

load erf_co_sub20_Lgp_data
load erf_co_sub20_Rgp_data

erf_co_sub20_gp_data = erf_co_sub20_Lgp_data;
erf_co_sub20_gp_data.avg = (erf_co_sub20_Lgp_data.avg + erf_co_sub20_Rgp_data.avg)./2;
save erf_co_sub20_gp_data erf_co_sub20_gp_data


close all
clear all

load erf_co_sub20_Lsep_data
load erf_co_sub20_Rsep_data

erf_co_sub20_sep_data = erf_co_sub20_Lsep_data;
erf_co_sub20_sep_data.avg = (erf_co_sub20_Lsep_data.avg + erf_co_sub20_Rsep_data.avg)./2;
save erf_co_sub20_sep_data erf_co_sub20_sep_data


close all
clear all

load erf_co_sub20_Lsh_comb
load erf_co_sub20_Rsh_comb

erf_co_sub20_sh_comb = erf_co_sub20_Lsh_comb;
erf_co_sub20_sh_comb.avg = (erf_co_sub20_Lsh_comb.avg + erf_co_sub20_Rsh_comb.avg)./2;
save erf_co_sub20_sh_comb erf_co_sub20_sh_comb

close all
clear all

load erf_co_sub20_Luns_comb
load erf_co_sub20_Runs_comb

erf_co_sub20_uns_comb = erf_co_sub20_Luns_comb;
erf_co_sub20_uns_comb.avg = (erf_co_sub20_Luns_comb.avg + erf_co_sub20_Runs_comb.avg)./2;
save erf_co_sub20_uns_comb erf_co_sub20_uns_comb

close all
clear all

load erf_co_sub20_Lgp_comb
load erf_co_sub20_Rgp_comb

erf_co_sub20_gp_comb = erf_co_sub20_Lgp_comb;
erf_co_sub20_gp_comb.avg = (erf_co_sub20_Lgp_comb.avg + erf_co_sub20_Rgp_comb.avg)./2;
save erf_co_sub20_gp_comb erf_co_sub20_gp_comb

close all
clear all

load erf_co_sub20_Lsep_comb
load erf_co_sub20_Rsep_comb

erf_co_sub20_sep_comb = erf_co_sub20_Lsep_comb;
erf_co_sub20_sep_comb.avg = (erf_co_sub20_Lsep_comb.avg + erf_co_sub20_Rsep_comb.avg)./2;
save erf_co_sub20_sep_comb erf_co_sub20_sep_comb

close all
clear all
%%%%%



%%% Lsh_ip

D = spm_eeg_load ('Mwmrcpbefdspm8_run1raw_trans_sss.mat');
erf_ip_sub20_Lsh = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 1
    erf_ip_sub20_Lsh.trial = erf_ip_sub20_Lsh.trial(ind);
    erf_ip_sub20_Lsh.time =  erf_ip_sub20_Lsh.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub20_Lsh.grad = cfg.grad;
erf_ip_sub20_Lsh_data = ft_timelockanalysis([], erf_ip_sub20_Lsh);

save erf_ip_sub20_Lsh_data erf_ip_sub20_Lsh_data

erf_ip_sub20_Lsh_check = ft_checkdata(erf_ip_sub20_Lsh_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_ip_sub20_Lsh_comb = ft_combineplanar(cfg, erf_ip_sub20_Lsh_check);

save erf_ip_sub20_Lsh_comb erf_ip_sub20_Lsh_comb
clear all;


%%% Rsh_ip

D = spm_eeg_load ('wmrcpbefdspm8_run1raw_trans_sss.mat');
erf_ip_sub20_Rsh = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 2
    erf_ip_sub20_Rsh.trial = erf_ip_sub20_Rsh.trial(ind);
    erf_ip_sub20_Rsh.time =  erf_ip_sub20_Rsh.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub20_Rsh.grad = cfg.grad;
erf_ip_sub20_Rsh_data = ft_timelockanalysis([], erf_ip_sub20_Rsh);

save erf_ip_sub20_Rsh_data erf_ip_sub20_Rsh_data

erf_ip_sub20_Rsh_check = ft_checkdata(erf_ip_sub20_Rsh_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_ip_sub20_Rsh_comb = ft_combineplanar(cfg, erf_ip_sub20_Rsh_check);

save erf_ip_sub20_Rsh_comb erf_ip_sub20_Rsh_comb
clear all;

%%% Luns_ip

D = spm_eeg_load ('Mwmrcpbefdspm8_run1raw_trans_sss.mat');
erf_ip_sub20_Luns = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 3
    erf_ip_sub20_Luns.trial = erf_ip_sub20_Luns.trial(ind);
    erf_ip_sub20_Luns.time =  erf_ip_sub20_Luns.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub20_Luns.grad = cfg.grad;
erf_ip_sub20_Luns_data = ft_timelockanalysis([], erf_ip_sub20_Luns);

save erf_ip_sub20_Luns_data erf_ip_sub20_Luns_data

erf_ip_sub20_Luns_check = ft_checkdata(erf_ip_sub20_Luns_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_ip_sub20_Luns_comb = ft_combineplanar(cfg, erf_ip_sub20_Luns_check);

save erf_ip_sub20_Luns_comb erf_ip_sub20_Luns_comb
clear all;


%%% Runs_ip

D = spm_eeg_load ('wmrcpbefdspm8_run1raw_trans_sss.mat');
erf_ip_sub20_Runs = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 4
    erf_ip_sub20_Runs.trial = erf_ip_sub20_Runs.trial(ind);
    erf_ip_sub20_Runs.time =  erf_ip_sub20_Runs.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub20_Runs.grad = cfg.grad;
erf_ip_sub20_Runs_data = ft_timelockanalysis([], erf_ip_sub20_Runs);

save erf_ip_sub20_Runs_data erf_ip_sub20_Runs_data

erf_ip_sub20_Runs_check = ft_checkdata(erf_ip_sub20_Runs_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_ip_sub20_Runs_comb = ft_combineplanar(cfg, erf_ip_sub20_Runs_check);

save erf_ip_sub20_Runs_comb erf_ip_sub20_Runs_comb
clear all;



%%% Lgp_ip

D = spm_eeg_load ('Mwmrcpbefdspm8_run1raw_trans_sss.mat');
erf_ip_sub20_Lgp = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 5
    erf_ip_sub20_Lgp.trial = erf_ip_sub20_Lgp.trial(ind);
    erf_ip_sub20_Lgp.time =  erf_ip_sub20_Lgp.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub20_Lgp.grad = cfg.grad;
erf_ip_sub20_Lgp_data = ft_timelockanalysis([], erf_ip_sub20_Lgp);

save erf_ip_sub20_Lgp_data erf_ip_sub20_Lgp_data

erf_ip_sub20_Lgp_check = ft_checkdata(erf_ip_sub20_Lgp_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_ip_sub20_Lgp_comb = ft_combineplanar(cfg, erf_ip_sub20_Lgp_check);

save erf_ip_sub20_Lgp_comb erf_ip_sub20_Lgp_comb
clear all;


%%% Rgp_ip

D = spm_eeg_load ('wmrcpbefdspm8_run1raw_trans_sss.mat');
erf_ip_sub20_Rgp = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 6
    erf_ip_sub20_Rgp.trial = erf_ip_sub20_Rgp.trial(ind);
    erf_ip_sub20_Rgp.time =  erf_ip_sub20_Rgp.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub20_Rgp.grad = cfg.grad;
erf_ip_sub20_Rgp_data = ft_timelockanalysis([], erf_ip_sub20_Rgp);

save erf_ip_sub20_Rgp_data erf_ip_sub20_Rgp_data

erf_ip_sub20_Rgp_check = ft_checkdata(erf_ip_sub20_Rgp_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_ip_sub20_Rgp_comb = ft_combineplanar(cfg, erf_ip_sub20_Rgp_check);

save erf_ip_sub20_Rgp_comb erf_ip_sub20_Rgp_comb
clear all;

%%% Lsep_ip

D = spm_eeg_load ('Mwmrcpbefdspm8_run1raw_trans_sss.mat');
erf_ip_sub20_Lsep = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 7
    erf_ip_sub20_Lsep.trial = erf_ip_sub20_Lsep.trial(ind);
    erf_ip_sub20_Lsep.time =  erf_ip_sub20_Lsep.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub20_Lsep.grad = cfg.grad;
erf_ip_sub20_Lsep_data = ft_timelockanalysis([], erf_ip_sub20_Lsep);

save erf_ip_sub20_Lsep_data erf_ip_sub20_Lsep_data

erf_ip_sub20_Lsep_check = ft_checkdata(erf_ip_sub20_Lsep_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_ip_sub20_Lsep_comb = ft_combineplanar(cfg, erf_ip_sub20_Lsep_check);

save erf_ip_sub20_Lsep_comb erf_ip_sub20_Lsep_comb
clear all;


%%% Rsep_ip

D = spm_eeg_load ('wmrcpbefdspm8_run1raw_trans_sss.mat');
erf_ip_sub20_Rsep = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 8
    erf_ip_sub20_Rsep.trial = erf_ip_sub20_Rsep.trial(ind);
    erf_ip_sub20_Rsep.time =  erf_ip_sub20_Rsep.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub20_Rsep.grad = cfg.grad;
erf_ip_sub20_Rsep_data = ft_timelockanalysis([], erf_ip_sub20_Rsep);

save erf_ip_sub20_Rsep_data erf_ip_sub20_Rsep_data

erf_ip_sub20_Rsep_check = ft_checkdata(erf_ip_sub20_Rsep_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'sum'); % svd->sum
cfg.combinemethod  = 'sum';
erf_ip_sub20_Rsep_comb = ft_combineplanar(cfg, erf_ip_sub20_Rsep_check);

save erf_ip_sub20_Rsep_comb erf_ip_sub20_Rsep_comb
clear all;


%%%%%
close all
clear all

load erf_ip_sub20_Lsh_data
load erf_ip_sub20_Rsh_data

erf_ip_sub20_sh_data = erf_ip_sub20_Lsh_data;
erf_ip_sub20_sh_data.avg = (erf_ip_sub20_Lsh_data.avg + erf_ip_sub20_Rsh_data.avg)./2;
save erf_ip_sub20_sh_data erf_ip_sub20_sh_data


close all
clear all

load erf_ip_sub20_Luns_data
load erf_ip_sub20_Runs_data

erf_ip_sub20_uns_data = erf_ip_sub20_Luns_data;
erf_ip_sub20_uns_data.avg = (erf_ip_sub20_Luns_data.avg + erf_ip_sub20_Runs_data.avg)./2;
save erf_ip_sub20_uns_data erf_ip_sub20_uns_data


close all
clear all

load erf_ip_sub20_Lgp_data
load erf_ip_sub20_Rgp_data

erf_ip_sub20_gp_data = erf_ip_sub20_Lgp_data;
erf_ip_sub20_gp_data.avg = (erf_ip_sub20_Lgp_data.avg + erf_ip_sub20_Rgp_data.avg)./2;
save erf_ip_sub20_gp_data erf_ip_sub20_gp_data


close all
clear all

load erf_ip_sub20_Lsep_data
load erf_ip_sub20_Rsep_data

erf_ip_sub20_sep_data = erf_ip_sub20_Lsep_data;
erf_ip_sub20_sep_data.avg = (erf_ip_sub20_Lsep_data.avg + erf_ip_sub20_Rsep_data.avg)./2;
save erf_ip_sub20_sep_data erf_ip_sub20_sep_data


close all
clear all

load erf_ip_sub20_Lsh_comb
load erf_ip_sub20_Rsh_comb

erf_ip_sub20_sh_comb = erf_ip_sub20_Lsh_comb;
erf_ip_sub20_sh_comb.avg = (erf_ip_sub20_Lsh_comb.avg + erf_ip_sub20_Rsh_comb.avg)./2;
save erf_ip_sub20_sh_comb erf_ip_sub20_sh_comb


close all
clear all

load erf_ip_sub20_Luns_comb
load erf_ip_sub20_Runs_comb

erf_ip_sub20_uns_comb = erf_ip_sub20_Luns_comb;
erf_ip_sub20_uns_comb.avg = (erf_ip_sub20_Luns_comb.avg + erf_ip_sub20_Runs_comb.avg)./2;
save erf_ip_sub20_uns_comb erf_ip_sub20_uns_comb


close all
clear all

load erf_ip_sub20_Lgp_comb
load erf_ip_sub20_Rgp_comb

erf_ip_sub20_gp_comb = erf_ip_sub20_Lgp_comb;
erf_ip_sub20_gp_comb.avg = (erf_ip_sub20_Lgp_comb.avg + erf_ip_sub20_Rgp_comb.avg)./2;
save erf_ip_sub20_gp_comb erf_ip_sub20_gp_comb


close all
clear all

load erf_ip_sub20_Lsep_comb
load erf_ip_sub20_Rsep_comb

erf_ip_sub20_sep_comb = erf_ip_sub20_Lsep_comb;
erf_ip_sub20_sep_comb.avg = (erf_ip_sub20_Lsep_comb.avg + erf_ip_sub20_Rsep_comb.avg)./2;
save erf_ip_sub20_sep_comb erf_ip_sub20_sep_comb
