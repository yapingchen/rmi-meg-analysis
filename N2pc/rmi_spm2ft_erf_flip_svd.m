clear all
close all

clc
cd 'F:\2017_RMI_MEG\N2pc'

%% Lsh_co

D = spm_eeg_load ('wmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_co_sub02_Lsh = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 1
    erf_co_sub02_Lsh.trial = erf_co_sub02_Lsh.trial(ind);
    erf_co_sub02_Lsh.time =  erf_co_sub02_Lsh.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub02_Lsh.grad = cfg.grad;
erf_co_sub02_Lsh_data = ft_timelockanalysis([], erf_co_sub02_Lsh);

save erf_co_sub02_Lsh_data erf_co_sub02_Lsh_data

erf_co_sub02_Lsh_check = checkdata(erf_co_sub02_Lsh_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_co_sub02_Lsh_comb = ft_combineplanar(cfg, erf_co_sub02_Lsh_check);

save erf_co_sub02_Lsh_comb erf_co_sub02_Lsh_comb
clear all;


%% Rsh_co

D = spm_eeg_load ('Mwmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_co_sub02_Rsh = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 2
    erf_co_sub02_Rsh.trial = erf_co_sub02_Rsh.trial(ind);
    erf_co_sub02_Rsh.time =  erf_co_sub02_Rsh.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub02_Rsh.grad = cfg.grad;
erf_co_sub02_Rsh_data = ft_timelockanalysis([], erf_co_sub02_Rsh);

save erf_co_sub02_Rsh_data erf_co_sub02_Rsh_data

erf_co_sub02_Rsh_check = checkdata(erf_co_sub02_Rsh_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_co_sub02_Rsh_comb = ft_combineplanar(cfg, erf_co_sub02_Rsh_check);

save erf_co_sub02_Rsh_comb erf_co_sub02_Rsh_comb
clear all;

%% Luns_co

D = spm_eeg_load ('wmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_co_sub02_Luns = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 3
    erf_co_sub02_Luns.trial = erf_co_sub02_Luns.trial(ind);
    erf_co_sub02_Luns.time =  erf_co_sub02_Luns.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub02_Luns.grad = cfg.grad;
erf_co_sub02_Luns_data = ft_timelockanalysis([], erf_co_sub02_Luns);

save erf_co_sub02_Luns_data erf_co_sub02_Luns_data

erf_co_sub02_Luns_check = checkdata(erf_co_sub02_Luns_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_co_sub02_Luns_comb = ft_combineplanar(cfg, erf_co_sub02_Luns_check);

save erf_co_sub02_Luns_comb erf_co_sub02_Luns_comb
clear all;


%% Runs_co

D = spm_eeg_load ('Mwmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_co_sub02_Runs = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 4
    erf_co_sub02_Runs.trial = erf_co_sub02_Runs.trial(ind);
    erf_co_sub02_Runs.time =  erf_co_sub02_Runs.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub02_Runs.grad = cfg.grad;
erf_co_sub02_Runs_data = ft_timelockanalysis([], erf_co_sub02_Runs);

save erf_co_sub02_Runs_data erf_co_sub02_Runs_data

erf_co_sub02_Runs_check = checkdata(erf_co_sub02_Runs_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_co_sub02_Runs_comb = ft_combineplanar(cfg, erf_co_sub02_Runs_check);

save erf_co_sub02_Runs_comb erf_co_sub02_Runs_comb
clear all;



%% Lgp_co

D = spm_eeg_load ('wmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_co_sub02_Lgp = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 5
    erf_co_sub02_Lgp.trial = erf_co_sub02_Lgp.trial(ind);
    erf_co_sub02_Lgp.time =  erf_co_sub02_Lgp.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub02_Lgp.grad = cfg.grad;
erf_co_sub02_Lgp_data = ft_timelockanalysis([], erf_co_sub02_Lgp);

save erf_co_sub02_Lgp_data erf_co_sub02_Lgp_data

erf_co_sub02_Lgp_check = checkdata(erf_co_sub02_Lgp_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_co_sub02_Lgp_comb = ft_combineplanar(cfg, erf_co_sub02_Lgp_check);

save erf_co_sub02_Lgp_comb erf_co_sub02_Lgp_comb
clear all;


%% Rgp_co

D = spm_eeg_load ('Mwmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_co_sub02_Rgp = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 6
    erf_co_sub02_Rgp.trial = erf_co_sub02_Rgp.trial(ind);
    erf_co_sub02_Rgp.time =  erf_co_sub02_Rgp.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub02_Rgp.grad = cfg.grad;
erf_co_sub02_Rgp_data = ft_timelockanalysis([], erf_co_sub02_Rgp);

save erf_co_sub02_Rgp_data erf_co_sub02_Rgp_data

erf_co_sub02_Rgp_check = checkdata(erf_co_sub02_Rgp_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_co_sub02_Rgp_comb = ft_combineplanar(cfg, erf_co_sub02_Rgp_check);

save erf_co_sub02_Rgp_comb erf_co_sub02_Rgp_comb
clear all;

%% Lsep_co

D = spm_eeg_load ('wmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_co_sub02_Lsep = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 7
    erf_co_sub02_Lsep.trial = erf_co_sub02_Lsep.trial(ind);
    erf_co_sub02_Lsep.time =  erf_co_sub02_Lsep.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub02_Lsep.grad = cfg.grad;
erf_co_sub02_Lsep_data = ft_timelockanalysis([], erf_co_sub02_Lsep);

save erf_co_sub02_Lsep_data erf_co_sub02_Lsep_data

erf_co_sub02_Lsep_check = checkdata(erf_co_sub02_Lsep_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_co_sub02_Lsep_comb = ft_combineplanar(cfg, erf_co_sub02_Lsep_check);

save erf_co_sub02_Lsep_comb erf_co_sub02_Lsep_comb
clear all;


%% Rsep_co

D = spm_eeg_load ('Mwmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_co_sub02_Rsep = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 8
    erf_co_sub02_Rsep.trial = erf_co_sub02_Rsep.trial(ind);
    erf_co_sub02_Rsep.time =  erf_co_sub02_Rsep.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_co_sub02_Rsep.grad = cfg.grad;
erf_co_sub02_Rsep_data = ft_timelockanalysis([], erf_co_sub02_Rsep);

save erf_co_sub02_Rsep_data erf_co_sub02_Rsep_data

erf_co_sub02_Rsep_check = checkdata(erf_co_sub02_Rsep_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_co_sub02_Rsep_comb = ft_combineplanar(cfg, erf_co_sub02_Rsep_check);

save erf_co_sub02_Rsep_comb erf_co_sub02_Rsep_comb
clear all;


%%
%%
%%
close all
clear all

load erf_co_sub02_Lsh_data
load erf_co_sub02_Rsh_data

erf_co_sub02_sh_data = erf_co_sub02_Lsh_data;
erf_co_sub02_sh_data.avg = (erf_co_sub02_Lsh_data.avg + erf_co_sub02_Rsh_data.avg)./2;
save erf_co_sub02_sh_data erf_co_sub02_sh_data


close all
clear all

load erf_co_sub02_Luns_data
load erf_co_sub02_Runs_data

erf_co_sub02_uns_data = erf_co_sub02_Luns_data;
erf_co_sub02_uns_data.avg = (erf_co_sub02_Luns_data.avg + erf_co_sub02_Runs_data.avg)./2;
save erf_co_sub02_uns_data erf_co_sub02_uns_data


close all
clear all

load erf_co_sub02_Lgp_data
load erf_co_sub02_Rgp_data

erf_co_sub02_gp_data = erf_co_sub02_Lgp_data;
erf_co_sub02_gp_data.avg = (erf_co_sub02_Lgp_data.avg + erf_co_sub02_Rgp_data.avg)./2;
save erf_co_sub02_gp_data erf_co_sub02_gp_data


close all
clear all

load erf_co_sub02_Lsep_data
load erf_co_sub02_Rsep_data

erf_co_sub02_sep_data = erf_co_sub02_Lsep_data;
erf_co_sub02_sep_data.avg = (erf_co_sub02_Lsep_data.avg + erf_co_sub02_Rsep_data.avg)./2;
save erf_co_sub02_sep_data erf_co_sub02_sep_data


close all
clear all


%%



%% Lsh_ip

D = spm_eeg_load ('Mwmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_ip_sub02_Lsh = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 1
    erf_ip_sub02_Lsh.trial = erf_ip_sub02_Lsh.trial(ind);
    erf_ip_sub02_Lsh.time =  erf_ip_sub02_Lsh.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub02_Lsh.grad = cfg.grad;
erf_ip_sub02_Lsh_data = ft_timelockanalysis([], erf_ip_sub02_Lsh);

save erf_ip_sub02_Lsh_data erf_ip_sub02_Lsh_data

erf_ip_sub02_Lsh_check = checkdata(erf_ip_sub02_Lsh_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_ip_sub02_Lsh_comb = ft_combineplanar(cfg, erf_ip_sub02_Lsh_check);

save erf_ip_sub02_Lsh_comb erf_ip_sub02_Lsh_comb
clear all;


%% Rsh_ip

D = spm_eeg_load ('wmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_ip_sub02_Rsh = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 2
    erf_ip_sub02_Rsh.trial = erf_ip_sub02_Rsh.trial(ind);
    erf_ip_sub02_Rsh.time =  erf_ip_sub02_Rsh.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub02_Rsh.grad = cfg.grad;
erf_ip_sub02_Rsh_data = ft_timelockanalysis([], erf_ip_sub02_Rsh);

save erf_ip_sub02_Rsh_data erf_ip_sub02_Rsh_data

erf_ip_sub02_Rsh_check = checkdata(erf_ip_sub02_Rsh_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_ip_sub02_Rsh_comb = ft_combineplanar(cfg, erf_ip_sub02_Rsh_check);

save erf_ip_sub02_Rsh_comb erf_ip_sub02_Rsh_comb
clear all;

%% Luns_ip

D = spm_eeg_load ('Mwmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_ip_sub02_Luns = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 3
    erf_ip_sub02_Luns.trial = erf_ip_sub02_Luns.trial(ind);
    erf_ip_sub02_Luns.time =  erf_ip_sub02_Luns.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub02_Luns.grad = cfg.grad;
erf_ip_sub02_Luns_data = ft_timelockanalysis([], erf_ip_sub02_Luns);

save erf_ip_sub02_Luns_data erf_ip_sub02_Luns_data

erf_ip_sub02_Luns_check = checkdata(erf_ip_sub02_Luns_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_ip_sub02_Luns_comb = ft_combineplanar(cfg, erf_ip_sub02_Luns_check);

save erf_ip_sub02_Luns_comb erf_ip_sub02_Luns_comb
clear all;


%% Runs_ip

D = spm_eeg_load ('wmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_ip_sub02_Runs = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 4
    erf_ip_sub02_Runs.trial = erf_ip_sub02_Runs.trial(ind);
    erf_ip_sub02_Runs.time =  erf_ip_sub02_Runs.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub02_Runs.grad = cfg.grad;
erf_ip_sub02_Runs_data = ft_timelockanalysis([], erf_ip_sub02_Runs);

save erf_ip_sub02_Runs_data erf_ip_sub02_Runs_data

erf_ip_sub02_Runs_check = checkdata(erf_ip_sub02_Runs_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_ip_sub02_Runs_comb = ft_combineplanar(cfg, erf_ip_sub02_Runs_check);

save erf_ip_sub02_Runs_comb erf_ip_sub02_Runs_comb
clear all;



%% Lgp_ip

D = spm_eeg_load ('Mwmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_ip_sub02_Lgp = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 5
    erf_ip_sub02_Lgp.trial = erf_ip_sub02_Lgp.trial(ind);
    erf_ip_sub02_Lgp.time =  erf_ip_sub02_Lgp.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub02_Lgp.grad = cfg.grad;
erf_ip_sub02_Lgp_data = ft_timelockanalysis([], erf_ip_sub02_Lgp);

save erf_ip_sub02_Lgp_data erf_ip_sub02_Lgp_data

erf_ip_sub02_Lgp_check = checkdata(erf_ip_sub02_Lgp_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_ip_sub02_Lgp_comb = ft_combineplanar(cfg, erf_ip_sub02_Lgp_check);

save erf_ip_sub02_Lgp_comb erf_ip_sub02_Lgp_comb
clear all;


%% Rgp_ip

D = spm_eeg_load ('wmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_ip_sub02_Rgp = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 6
    erf_ip_sub02_Rgp.trial = erf_ip_sub02_Rgp.trial(ind);
    erf_ip_sub02_Rgp.time =  erf_ip_sub02_Rgp.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub02_Rgp.grad = cfg.grad;
erf_ip_sub02_Rgp_data = ft_timelockanalysis([], erf_ip_sub02_Rgp);

save erf_ip_sub02_Rgp_data erf_ip_sub02_Rgp_data

erf_ip_sub02_Rgp_check = checkdata(erf_ip_sub02_Rgp_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_ip_sub02_Rgp_comb = ft_combineplanar(cfg, erf_ip_sub02_Rgp_check);

save erf_ip_sub02_Rgp_comb erf_ip_sub02_Rgp_comb
clear all;

%% Lsep_ip

D = spm_eeg_load ('Mwmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_ip_sub02_Lsep = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 7
    erf_ip_sub02_Lsep.trial = erf_ip_sub02_Lsep.trial(ind);
    erf_ip_sub02_Lsep.time =  erf_ip_sub02_Lsep.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub02_Lsep.grad = cfg.grad;
erf_ip_sub02_Lsep_data = ft_timelockanalysis([], erf_ip_sub02_Lsep);

save erf_ip_sub02_Lsep_data erf_ip_sub02_Lsep_data

erf_ip_sub02_Lsep_check = checkdata(erf_ip_sub02_Lsep_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_ip_sub02_Lsep_comb = ft_combineplanar(cfg, erf_ip_sub02_Lsep_check);

save erf_ip_sub02_Lsep_comb erf_ip_sub02_Lsep_comb
clear all;


%% Rsep_ip

D = spm_eeg_load ('wmrcpbefdspm8_run1_raw_trans_sss.mat');
erf_ip_sub02_Rsep = D.ftraw(0);

if D.ntrials > 1
    clb = D.conditions;
    %ind = spm_input('Select trial',1, 'm', sprintf('%s|', clb{:}),1:D.ntrials)
    ind = 8
    erf_ip_sub02_Rsep.trial = erf_ip_sub02_Rsep.trial(ind);
    erf_ip_sub02_Rsep.time =  erf_ip_sub02_Rsep.time(ind);
end


cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

erf_ip_sub02_Rsep.grad = cfg.grad;
erf_ip_sub02_Rsep_data = ft_timelockanalysis([], erf_ip_sub02_Rsep);

save erf_ip_sub02_Rsep_data erf_ip_sub02_Rsep_data

erf_ip_sub02_Rsep_check = checkdata(erf_ip_sub02_Rsep_data, 'senstype','neuromag306');
cfg.combinemethod  = ft_getopt(cfg, 'combinemethod',  'svd');
cfg.combinemethod  = 'svd';
erf_ip_sub02_Rsep_comb = ft_combineplanar(cfg, erf_ip_sub02_Rsep_check);

save erf_ip_sub02_Rsep_comb erf_ip_sub02_Rsep_comb
clear all;


%%
%%
%%
close all
clear all

load erf_ip_sub02_Lsh_data
load erf_ip_sub02_Rsh_data

erf_ip_sub02_sh_data = erf_ip_sub02_Lsh_data;
erf_ip_sub02_sh_data.avg = (erf_ip_sub02_Lsh_data.avg + erf_ip_sub02_Rsh_data.avg)./2;
save erf_ip_sub02_sh_data erf_ip_sub02_sh_data


close all
clear all

load erf_ip_sub02_Luns_data
load erf_ip_sub02_Runs_data

erf_ip_sub02_uns_data = erf_ip_sub02_Luns_data;
erf_ip_sub02_uns_data.avg = (erf_ip_sub02_Luns_data.avg + erf_ip_sub02_Runs_data.avg)./2;
save erf_ip_sub02_uns_data erf_ip_sub02_uns_data


close all
clear all

load erf_ip_sub02_Lgp_data
load erf_ip_sub02_Rgp_data

erf_ip_sub02_gp_data = erf_ip_sub02_Lgp_data;
erf_ip_sub02_gp_data.avg = (erf_ip_sub02_Lgp_data.avg + erf_ip_sub02_Rgp_data.avg)./2;
save erf_ip_sub02_gp_data erf_ip_sub02_gp_data


close all
clear all

load erf_ip_sub02_Lsep_data
load erf_ip_sub02_Rsep_data

erf_ip_sub02_sep_data = erf_ip_sub02_Lsep_data;
erf_ip_sub02_sep_data.avg = (erf_ip_sub02_Lsep_data.avg + erf_ip_sub02_Rsep_data.avg)./2;
save erf_ip_sub02_sep_data erf_ip_sub02_sep_data


close all
clear all

