cd 'F:\2017_RMI_MEG\N2pc'
clear all

spm('defaults', 'eeg');

% for k = 6:11;
%     aa = sprintf('sub%02d',k);
%     eval(['cd  F:\2017_RMI_MEG\N2pc\' aa]);
% S = [];
% S.D = ['F:\2017_RMI_MEG\N2pc\' aa '\cpbefdspm8_run1_raw_trans_sss.mat'];
% D = spm_eeg_remove_bad_trials(S);
% 
% 
% S = [];
% S.D = ['F:\2017_RMI_MEG\N2pc\' aa '\rcpbefdspm8_run1_raw_trans_sss.mat'];
% S.robust = false;
% D = spm_eeg_average(S);
% 
% end
% 
for k = 12:20;
    aa = sprintf('sub%02d',k);
    eval(['cd  F:\2017_RMI_MEG\N2pc\' aa]);
S = [];
S.D = ['F:\2017_RMI_MEG\N2pc\' aa '\cpbefdspm8_run1raw_trans_sss.mat'];
D = spm_eeg_remove_bad_trials(S);


S = [];
S.D = ['F:\2017_RMI_MEG\N2pc\' aa '\rcpbefdspm8_run1raw_trans_sss.mat'];
S.robust = false;
D = spm_eeg_average(S);

end