spm('defaults', 'eeg');

for k = [2 3 5:20]
    aa = sprintf('sub%02d',k);
       
S = [];
S.D = ['F:\2017_RMI_MEG\N2pc\' aa '\rcpbefdspm8_run1_raw_trans_sss.mat'];
S.newname = 'rcpbefdspm8_run.dat';
D = spm_eeg_copy(S);

end

