clear all
spm('defaults', 'eeg');

% for k = 2:11;
%     aa = sprintf('sub%02d',k);
%     eval(['cd  F:\2017_RMI_MEG\N2pc\' aa]);
% S = [];
% S.D = ['F:\2017_RMI_MEG\N2pc\' aa '\mrcpbefdspm8_run1_raw_trans_sss.mat'];
% S.c = [0 1/2 0 1/2 0 0 0 0 0 0 0
%        1/2 0 1/2 0 0 0 0 0 0 0 0
%        0 0 0 0 0 1/2 0 1/2 0 0 0
%        0 0 0 0 1/2 0 1/2 0 0 0 0
%        0 0 0 1/2 0 0 0 1/2 0 0 0
%        0 0 1/2 0 0 0 1/2 0 0 0 0
%        0 0 0 0 0 0 0 0 0 1 0
%        0 0 0 0 0 0 0 0 1 0 0];
% S.label = {
%            'Lsh'
%            'Rsh'
%            'Luns'
%            'Runs'
%            'Lgp'
%            'Rgp'
%            'Lsep'
%            'Rsep'
%            };
% S.WeightAve = 0;
% D = spm_eeg_weight_epochs(S);
% end

for k = 12:20;
    aa = sprintf('sub%02d',k);
    eval(['cd  F:\2017_RMI_MEG\N2pc\' aa]);
S = [];
S.D = ['F:\2017_RMI_MEG\N2pc\' aa '\mrcpbefdspm8_run1raw_trans_sss.mat'];
S.c = [0 1/2 0 1/2 0 0 0 0 0 0 0
       1/2 0 1/2 0 0 0 0 0 0 0 0
       0 0 0 0 0 1/2 0 1/2 0 0 0
       0 0 0 0 1/2 0 1/2 0 0 0 0
       0 0 0 1/2 0 0 0 1/2 0 0 0
       0 0 1/2 0 0 0 1/2 0 0 0 0
       0 0 0 0 0 0 0 0 0 1 0
       0 0 0 0 0 0 0 0 1 0 0];
S.label = {
           'Lsh'
           'Rsh'
           'Luns'
           'Runs'
           'Lgp'
           'Rgp'
           'Lsep'
           'Rsep'
           };
S.WeightAve = 0;
D = spm_eeg_weight_epochs(S);
end