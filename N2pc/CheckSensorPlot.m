%%
clear all
% cd F:\2017_RMI_MEG\N2pc\COIP;
load erf_cl_coip_comb_avg17
load erf_cl_ipco_comb_avg17

cfg = [];
cfg.showlabels = 'yes'; 
cfg.fontsize = 8; 
cfg.layout = 'C:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306cmb.lay';
cfg.baseline = [-0.2 0]; 
cfg.xlim = [0 0.6]; 
cfg.ylim = [-3e-12 3e-12]; 
ft_multiplotER(cfg, erf_cl_coip_comb_avg17, erf_cl_ipco_comb_avg17);
% ft_multiplotER(cfg, ipip);

%%

cfg.xlim = [-0.2 0.8];
cfg.ylim = [-3e-12 3e-12];
cfg.channel = 'all'; %'MEG0822';'MEG0823';'MEG0821'
clf;
ft_singleplotER(cfg, gpco, gpip);