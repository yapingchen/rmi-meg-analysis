close all
clear all
clc

cd 'F:\2017_RMI_MEG\N2pc\COIP\source'
load erf_coip_sub02_gp_data


%% step 1 - read MRI data and get coord info from MEG
mri_orig = ft_read_mri('F:\2017_RMI_MEG\T1\s02.nii');
dataset = 'F:\2017_RMI_MEG\02_ylc\run1_raw_trans_sss.fif';

grad    = ft_read_sens(dataset,'senstype','meg');
% elec    = ft_read_sens(dataset,'senstype','eeg');
shape   = ft_read_headshape(dataset,'unit','cm');

figure;
ft_plot_headshape(shape);
ft_plot_sens(grad, 'style', '*b');
% ft_plot_sens(elec, 'style', '*g');

view([1 0 0])
print -dpng natmeg_dip_geometry1.png

figure;
cfg = [];
ft_sourceplot(cfg, mri_orig);

save mri_orig mri_orig

%% step 2 -  MRI coregister
cfg = [];
cfg.method = 'interactive'; 
cfg.coordsys = 'neuromag';
[mri_realigned1] = ft_volumerealign(cfg, mri_orig);

save mri_realigned1 mri_realigned1


cfg = [];
cfg.method = 'headshape';
cfg.coordsys = 'neuromag';
cfg.headshape.headshape = shape;
[mri_realigned2] = ft_volumerealign(cfg, mri_realigned1);

save mri_realigned2 mri_realigned2


%% step 3 -  MRI reslice

cfg = [];
cfg.resolution = 1;
cfg.xrange = [-100 100];
cfg.yrange = [-110 140];
cfg.zrange = [-50 150];
mri_resliced = ft_volumereslice(cfg, mri_realigned2);

save mri_resliced mri_resliced

figure
ft_sourceplot([], mri_resliced);
print -dpng natmeg_dip_mri_resliced.png

mri_resliced_cm = ft_convert_units(mri_resliced, 'cm');
save mri_resliced_cm mri_resliced_cm

%% step 4 - Construct the MEG volume conduction model

cfg           = [];
cfg.output    = {'brain', 'skull', 'scalp'};
mri_segmented = ft_volumesegment(cfg, mri_resliced);

mri_segmented.anatomy = mri_resliced.anatomy;
save mri_segmented mri_segmented


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.funparameter = 'brain';
ft_sourceplot(cfg, mri_segmented);
print -dpng natmeg_dip_segmented_brain.png

cfg.funparameter = 'skull';
ft_sourceplot(cfg, mri_segmented);
print -dpng natmeg_dip_segmented_skull.png

cfg.funparameter = 'scalp';
ft_sourceplot(cfg, mri_segmented);
print -dpng natmeg_dip_segmented_scalp.png


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg = [];
cfg.method = 'projectmesh';
cfg.tissue = 'brain';
cfg.numvertices = 3000;
mesh_brain = ft_prepare_mesh(cfg, mri_segmented);

cfg = [];
cfg.method = 'projectmesh';
cfg.tissue = 'skull';
cfg.numvertices = 2000;
mesh_skull = ft_prepare_mesh(cfg, mri_segmented);

cfg = [];
cfg.method = 'projectmesh';
cfg.tissue = 'scalp';
cfg.numvertices = 1000;
mesh_scalp = ft_prepare_mesh(cfg, mri_segmented);

cfg = [];
cfg.method = 'isosurface';
cfg.tissue = 'scalp';
cfg.numvertices = 16000;
highres_scalp = ft_prepare_mesh(cfg, mri_segmented);

save mesh mesh_* highres_scalp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
ft_plot_mesh(mesh_scalp, 'edgecolor', 'none', 'facecolor', 'skin')
material dull
camlight
lighting phong
print -dpng natmeg_dip_scalp.png


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
ft_plot_mesh(highres_scalp, 'edgecolor', 'none', 'facecolor', 'skin')
material dull
camlight
lighting phong
print -dpng natmeg_dip_highres_scalp.png


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg = [];
cfg.method = 'singleshell';
headmodel_meg = ft_prepare_headmodel(cfg, mesh_brain);

headmodel_meg = ft_convert_units(headmodel_meg,'cm');

save headmodel_meg headmodel_meg

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
hold on
ft_plot_headshape(shape);
ft_plot_sens(grad, 'style', 'ob');
ft_plot_sens(elec, 'style', 'og');
ft_plot_vol(headmodel_meg, 'facealpha', 0.5, 'edgecolor', 'none'); % "lighting phong" does not work with opacity
material dull
camlight

view([1 0 0]) 
print -dpng natmeg_dip_geometry2.png

%% step 5 create leadfield

cfg                 = [];
cfg.grad            = erf_coip_sub02_gp_data.grad;
cfg.vol             = headmodel_meg;
cfg.reducerank      = 2;
cfg.channel         = {'MEG'};
cfg.grid.resolution = 0.7;   % use a 3-D grid with a 1 cm resolution
cfg.grid.unit       = 'cm';
grid = ft_prepare_leadfield(cfg);

%% step 6 create spatial filter using the lcmv beamformer

cfg                  = [];
cfg.method           = 'lcmv';
cfg.grid             = grid; % leadfield, which has the grid information
cfg.vol              = headmodel_meg; % volume conduction model (headmodel)
cfg.keepfilter       = 'yes';
cfg.lcmv.fixedori    = 'yes'; % project on axis of most variance using SVD
source_avg           = ft_sourceanalysis(cfg, timelock);



%%
cfg = [];                                           
cfg.toilim = [-.2 0];                       
dataPre = ft_redefinetrial(cfg, erf_coip_sub02_gp_data);
   
cfg.toilim = [0.2 0.6];                       
dataPost = ft_redefinetrial(cfg, erf_coip_sub02_gp_data);
cfg=[];
cfg.covariance = 'yes';
avgpst = ft_timelockanalysis(cfg,dataPost);
avgpre = ft_timelockanalysis(cfg,dataPre);

cfg =[];
cfg.method = 'lcmv';
cfg.grid             = grid; % leadfield, which has the grid information
cfg.vol              = headmodel_meg; % volume conduction model (headmodel)
% cfg.grid.filter      = source_avg.avg.filter;
source_avgpre           = ft_sourceanalysis(cfg, avgpre);
source_avgpst           = ft_sourceanalysis(cfg, avgpst);
%%

sourceDiff = source_avgpst;
sourceDiff.avg.pow = (source_avgpst.avg.pow - source_avgpre.avg.pow) ./ source_avgpre.avg.pow;

cfg            = [];
cfg.downsample = 2;
cfg.parameter  = 'avg.pow';
sourceDiffInt  = ft_sourceinterpolate(cfg, sourceDiff , mri_segmented);

%% 2D plot
%sourceDiffInt.mask = sourceDiffInt.pow > sourceDiffInt.pow.*0.5;
cfg = [];
cfg.method        = 'ortho';
cfg.funparameter  = 'avg.pow';
cfg.maskparameter = 'mask';
cfg.funcolorlim   = [-0.5 0.5];
cfg.opacitylim    = [-0.5 0.5]; 
  
ft_sourceplot(cfg, sourceDiffInt);

saveas(gcf,'Fig_result_LR1_2ndHarmonic') 
save sourceDiffInt sourceDiffInt

%  3D plot
cfg = [];
cfg.nonlinear     = 'no';
sourceDiffIntNorm = ft_volumenormalise(cfg, sourceDiffInt);

cfg = [];
cfg.method         = 'surface';
cfg.funparameter   = 'avg.pow';
cfg.maskparameter  = cfg.funparameter;
cfg.funcolorlim    = [-0.5 0.5];
cfg.funcolormap    = 'jet';
cfg.opacitylim     = [-0.5 0.5]; 
cfg.opacitymap     = 'rampup';  
cfg.projmethod     = 'nearest'; 
cfg.surffile       = 'surface_white_both.mat';
cfg.surfdownsample = 10; 
ft_sourceplot(cfg, sourceDiffIntNorm);
view ([90 0])
saveas(gcf,'Fig3D_result_LR1_2ndHarmonic') 
save sourceDiffIntNorm sourceDiffIntNorm
%%
cfg= [];
cfg.parameter = 'pow';
cfg.filename = 'SResult';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, sourceDiffIntNorm);