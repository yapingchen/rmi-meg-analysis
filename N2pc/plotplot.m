clear all
close all

load erf_gp_coip_ga_data
load erf_sep_coip_ga_data
gpsep_coip = erf_gp_coip_ga_data; 
gpsep_coip.avg = erf_gp_coip_ga_data.avg - erf_sep_coip_ga_data.avg; 

cfg = [];
cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';
cfg.xlim = [0.2 : 0.02 : 0.6];  % Define 12 time intervals
cfg.zlim = [-2e-13 2e-13];      % Set the 'color' limits.
clf;
ft_topoplotER(cfg, gpsep_coip);
colorbar;

%%
clear all
close all

load erf_sh_coip_ga_data
load erf_uns_coip_ga_data
shuns_coip = erf_sh_coip_ga_data; %%%
shuns_coip.avg = erf_sh_coip_ga_data.avg - erf_uns_coip_ga_data.avg; %%%

cfg = [];
cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306lati.lay';
cfg.xlim = [0.2 : 0.02 : 0.6];  % Define 12 time intervals
cfg.zlim = [-2e-12 2e-12];      % Set the 'color' limits.
clf;
ft_topoplotER(cfg, shuns_coip);
colorbar;


%%
clear all
close all

load erf_gp_coip_ga_data
load erf_sep_coip_ga_data
gpsep_coip = erf_gp_coip_ga_data; 
gpsep_coip.avg = erf_gp_coip_ga_data.avg - erf_sep_coip_ga_data.avg; 
cfg = [];
cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306long.lay';
cfg.xlim = [0.2 : 0.02 : 0.6];  % Define 12 time intervals
cfg.zlim = [-2e-12 2e-12];      % Set the 'color' limits.
clf;
ft_topoplotER(cfg, gpsep_coip);
colorbar;

%%
clear all
close all

load erf_sh_coip_ga_data
load erf_uns_coip_ga_data
shuns_coip = erf_sh_coip_ga_data; %%%
shuns_coip.avg = erf_sh_coip_ga_data.avg - erf_uns_coip_ga_data.avg; %%%

cfg = [];
cfg.layout = 'D:\0_MEEG_toolbox\fieldtrip\template\layout\neuromag306long.lay';
cfg.xlim = [0.2 : 0.02 : 0.6];  % Define 12 time intervals
cfg.zlim = [-2e-12 2e-12];      % Set the 'color' limits.
clf;
ft_topoplotER(cfg, shuns_coip);
colorbar;