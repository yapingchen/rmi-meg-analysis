subs = [2 3 5:20];
for k = subs
    clearvars -except k
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    
    load sourceDiffIntNorm_L15
    load sourceDiffIntNorm_L20
    
    sourceDiffIntNorm_L1520 = sourceDiffIntNorm_L15;
    sourceDiffIntNorm_L1520.pow = (sourceDiffIntNorm_L15.pow + sourceDiffIntNorm_L20.pow) ./ 2;
    save sourceDiffIntNorm_L1520 sourceDiffIntNorm_L1520
end
%%
clc
clear all
close all
grandavg_R1520 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R1520.mat']);
   grandavg_R1520{a} = sourceDiffIntNorm_R1520;
   a = a+1;
end
cd F:\2017_localiser\s20_cbr
save -v7.3 grandavg_R1520 grandavg_R1520

clear all
close all
grandavg_L1520 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L1520.mat']);
   grandavg_L1520{a} = sourceDiffIntNorm_L1520;
   a = a+1;
end
cd F:\2017_localiser\s20_cbr
save -v7.3 grandavg_L1520 grandavg_L1520

%% run statistics over subjects %
clc
clear all
close all
cd F:\2017_localiser\s20_cbr
load grandavg_L1520
load grandavg_R1520

cfg=[];
cfg.dim         = grandavg_L1520{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_L1520);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_L1520{:}, grandavg_R1520{:});

save -v7.3 LR1520_source_stat_p33 stat


%% nifti
load LR1520_source_stat_p33

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'LR1520_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

clear all
close all
grandavg_R20 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R20.mat']);
   grandavg_R20{a} = sourceDiffIntNorm_R20;
   a = a+1;
end
save -v7.3 grandavg_R20 grandavg_R20

clear all
close all
grandavg_L20 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20.mat']);
   grandavg_L20{a} = sourceDiffIntNorm_L20;
   a = a+1;
end
save -v7.3 grandavg_L20 grandavg_L20

%% run statistics over subjects %
clc
clear all
close all
load grandavg_L20
load grandavg_R20

cfg=[];
cfg.dim         = grandavg_L20{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_L20);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_L20{:}, grandavg_R20{:});

save -v7.3 LR20_source_stat_p33 stat


%% nifti
load LR20_source_stat_p33

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'LR20_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);
%%
clc
clear all
close all
cd F:\2017_localiser
grandavg_R152020 = cell(1,36);
subs = [2 3 5:20];
a=1;
for k = subs
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R1520.mat']);
    grandavg_R152020{a} = sourceDiffIntNorm_R1520;
    a = a+1;
    eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R20.mat']);
    grandavg_R152020{a} = sourceDiffIntNorm_R20;
    a = a+1;
end
save -v7.3 grandavg_R152020 grandavg_R152020

clear all
close all
grandavg_L152020 = cell(1,36);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L1520.mat']);
   grandavg_L152020{a} = sourceDiffIntNorm_L1520;
   a = a+1;
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20.mat']);
   grandavg_L152020{a} = sourceDiffIntNorm_L20;
   a = a+1;
end
save -v7.3 grandavg_L152020 grandavg_L152020

%% run statistics over subjects %
clc
clear all
close all
cd F:\2017_localiser
load grandavg_R152020
load grandavg_L152020

cfg=[];
cfg.dim         = grandavg_R152020{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_R152020);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_R152020{:}, grandavg_L1520202020{:});

save -v7.3 RL1520202020_source_stat_p33 stat


%% nifti
cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'RL1520202020_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);

%% run statistics over subjects L15202020 vs L20 %
clc
clear all
close all
load grandavg_L15202020
load grandavg_L20

cfg=[];
cfg.dim         = grandavg_L15202020{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_L15202020);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_L15202020{:}, grandavg_L20{:});

save -v7.3 L1520202020_source_stat_p33 stat


%% nifti
load L1520202020_source_stat_p33

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'L1520202020_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);