cd F:\2017_localiser
clear all
clc
subs = [2:6 8:20]; % [2:6 8:20]
for k = subs
%     close all
    clearvars -except k
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    eval(['load freqT_s' aa '_L15_data'])
    eval(['load freqT_s' aa '_R20_data'])
    %     eval(['dataA = freqT_s' aa '_L15_data']);
    %     eval(['dataB = freqT_s' aa '_R20_data']);
    
    cfg = [];
    cfg.toilim = [0.5 10.5];
    eval(['dataA = ft_redefinetrial(cfg, freqT_s' aa '_L15_data)']);
    cfg = [];
    cfg.toilim = [0.5 10.5];
    eval(['dataB = ft_redefinetrial(cfg, freqT_s' aa '_R20_data)']);
    
    cfg = [];
    cfg.method    = 'mtmfft';
    cfg.output    = 'powandcsd';
    cfg.tapsmofrq = 1;
    cfg.foilim    = [1.8 1.8];
    cfg.keeptrials  = 'yes';
    freqA = ft_freqanalysis(cfg, dataA);
    freqB = ft_freqanalysis(cfg, dataB);
    
    
    load headmodel_meg
    cfg                 = [];
    cfg.grad            = freqA.grad;
    cfg.vol             = headmodel_meg;
    cfg.reducerank      = 2;
    cfg.channel         = {'MEG'};
    cfg.grid.resolution = 0.7;   % use a 3-D grid with a 1 cm resolution
    cfg.grid.unit       = 'cm';
%     cfg.normalize='yes';
    [grid] = ft_prepare_leadfield(cfg);
    
    
    %     dataAll = ft_appenddata([], dataA, dataB);
    %
    %     cfg = [];
    %     cfg.method    = 'mtmfft';
    %     cfg.output    = 'powandcsd';
    %     cfg.tapsmofrq = 1;
    %     cfg.foilim    = [1.3 1.5];
    %     cfg.keeptrials  = 'yes';
    %     freqAll = ft_freqanalysis(cfg, dataAll);
    %
    %     cfg              = [];
    %     cfg.method       = 'dics';
    %     cfg.frequency    = [1.3 1.5];
    %     cfg.grid         = grid;
    %     cfg.vol          = headmodel_meg;
    %     cfg.dics.projectnoise = 'yes';
    %     cfg.dics.lambda       = '0.5%';
    %     cfg.dics.keepfilter   = 'yes';
    %     cfg.dics.realfilter   = 'yes';
    %     %     cfg.rawtrial = 'yes';
    %     cfg.dics.fixedori = 'yes';
    %     sourceAll = ft_sourceanalysis(cfg, freqAll);
    
    cfg              = [];
    cfg.method       = 'dics';
    cfg.frequency    = [1.8 1.8];
    cfg.grid         = grid;
    cfg.vol          = headmodel_meg;
    cfg.dics.projectnoise = 'yes';
    %     cfg.dics.lambda       = '10%';
    %     cfg.rawtrial = 'yes';
    cfg.dics.fixedori = 'yes';
    %     cfg.grid.filter = sourceAll.avg.filter;
    
    sourceA = ft_sourceanalysis(cfg, freqA);
    sourceB = ft_sourceanalysis(cfg, freqB);
    
    sourceA_NAI = sourceA;
    sourceA_NAI.avg.pow = sourceA.avg.pow ./ sourceA.avg.noise;
    sourceB_NAI = sourceB;
    sourceB_NAI.avg.pow = sourceB.avg.pow ./ sourceB.avg.noise;
    
    
    load mri_segmented
    cfg            = [];
    cfg.downsample = 2;
    cfg.parameter  = 'pow';
    sourceAInt  = ft_sourceinterpolate(cfg, sourceA_NAI, mri_segmented);
    sourceBInt  = ft_sourceinterpolate(cfg, sourceB_NAI, mri_segmented);
    
    
    cfg = [];
    cfg.nonlinear     = 'no';
    sourceDiffIntNorm_L15_f = ft_volumenormalise(cfg, sourceAInt);
    sourceDiffIntNorm_R20_f = ft_volumenormalise(cfg, sourceBInt);
    
    save sourceDiffIntNorm_L15_f sourceDiffIntNorm_L15_f
    save sourceDiffIntNorm_R20_f sourceDiffIntNorm_R20_f
    
%             cfg = [];
%     cfg.method        = 'slice';
%     cfg.funparameter  = 'pow';
%     cfg.maskparameter = cfg.funparameter;
%     cfg.funcolorlim   = [1 6];
%     cfg.opacitylim    = [1 6];
%     cfg.opacitymap    = 'rampup';
%     ft_sourceplot(cfg, sourceAInt);
    %
end






%     cfg = [];
%     cfg.parameter    = 'pow';
%     cfg.dim          = sourceA.dim;
%     cfg.method           = 'montecarlo';
%     cfg.statistic        = 'ft_statfun_depsamplesT';
%     cfg.correctm         = 'cluster';
%     cfg.clusteralpha     = 0.05;
% %     cfg.clusterstatistic = 'maxsum';
%     cfg.tail             = 0;
%     cfg.clustertail      = 0;
%     cfg.alpha            = 0.05;
%     cfg.numrandomization = 1000;
%
%     ntrials = numel(sourceA.trial);
%     design  = zeros(2,2*ntrials);
%     design(1,1:ntrials) = 1;
%     design(1,ntrials+1:2*ntrials) = 2;
%     design(2,1:ntrials) = [1:ntrials];
%     design(2,ntrials+1:2*ntrials) = [1:ntrials];
%
%     cfg.design   = design;
%     cfg.ivar     = 1;
%     cfg.uvar     = 2;
%     stat = ft_sourcestatistics(cfg,sourceA,sourceB);