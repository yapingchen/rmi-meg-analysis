close all
clear all

cd 'F:\2017_localiser\s13_cbr'
%% step 1 - read MRI data and get coord info from MEG
mri_orig = ft_read_mri('F:\2017_localiser\s13_cbr\T1\s13.nii');
dataset = 'F:\2017_localiser\s13_cbr\loc_trans_sss.fif';

grad    = ft_read_sens(dataset,'senstype','meg');
% elec    = ft_read_sens(dataset,'senstype','eeg');
shape   = ft_read_headshape(dataset,'unit','cm');

figure;
ft_plot_headshape(shape);
ft_plot_sens(grad, 'style', '*b');
% ft_plot_sens(elec, 'style', '*g');

view([1 0 0])
print -dpng natmeg_dip_geometry1.png

figure;
cfg = [];
ft_sourceplot(cfg, mri_orig);

save mri_orig mri_orig

%% step 2 -  MRI coregister
cfg = [];
cfg.method = 'interactive';
cfg.coordsys = 'neuromag';
[mri_realigned1] = ft_volumerealign(cfg, mri_orig);

save mri_realigned1 mri_realigned1


cfg = [];
cfg.method = 'headshape';
cfg.coordsys = 'neuromag';
cfg.headshape.headshape = shape;
[mri_realigned2] = ft_volumerealign(cfg, mri_realigned1);

save mri_realigned2 mri_realigned2


%% step 3 -  MRI reslice

cfg = [];
cfg.resolution = 1;
cfg.xrange = [-100 100];
cfg.yrange = [-110 140];
cfg.zrange = [-50 150];
mri_resliced = ft_volumereslice(cfg, mri_realigned2);

save mri_resliced mri_resliced

figure
ft_sourceplot([], mri_resliced);
print -dpng natmeg_dip_mri_resliced.png

mri_resliced_cm = ft_convert_units(mri_resliced, 'cm');
save mri_resliced_cm mri_resliced_cm

% step 4 - Construct the MEG volume conduction model

cfg           = [];
cfg.output    = {'brain', 'skull', 'scalp'};
mri_segmented = ft_volumesegment(cfg, mri_resliced);

mri_segmented.anatomy = mri_resliced.anatomy;
save mri_segmented mri_segmented


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.funparameter = 'brain';
ft_sourceplot(cfg, mri_segmented);
print -dpng natmeg_dip_segmented_brain.png

cfg.funparameter = 'skull';
ft_sourceplot(cfg, mri_segmented);
print -dpng natmeg_dip_segmented_skull.png

cfg.funparameter = 'scalp';
ft_sourceplot(cfg, mri_segmented);
print -dpng natmeg_dip_segmented_scalp.png


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.method = 'projectmesh';
cfg.tissue = 'brain';
cfg.numvertices = 3000;
mesh_brain = ft_prepare_mesh(cfg, mri_segmented);


cfg = [];
cfg.method = 'projectmesh';
cfg.tissue = 'skull';
cfg.numvertices = 2000;
mesh_skull = ft_prepare_mesh(cfg, mri_segmented);

cfg = [];
cfg.method = 'projectmesh';
cfg.tissue = 'scalp';
cfg.numvertices = 1000;
mesh_scalp = ft_prepare_mesh(cfg, mri_segmented);

cfg = [];
cfg.method = 'isosurface';
cfg.tissue = 'scalp';
cfg.numvertices = 16000;
highres_scalp = ft_prepare_mesh(cfg, mri_segmented);

save mesh mesh_* highres_scalp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
ft_plot_mesh(mesh_scalp, 'edgecolor', 'none', 'facecolor', 'skin')
material dull
camlight
lighting phong
print -dpng natmeg_dip_scalp.png


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
ft_plot_mesh(highres_scalp, 'edgecolor', 'none', 'facecolor', 'skin')
material dull
camlight
lighting phong
print -dpng natmeg_dip_highres_scalp.png


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg = [];
cfg.method = 'singleshell';
headmodel_meg = ft_prepare_headmodel(cfg, mesh_brain);

headmodel_meg = ft_convert_units(headmodel_meg,'cm');

save headmodel_meg headmodel_meg

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
hold on
ft_plot_headshape(shape);
ft_plot_sens(grad, 'style', 'ob');
ft_plot_sens(elec, 'style', 'og');
ft_plot_vol(headmodel_meg, 'facealpha', 0.5, 'edgecolor', 'none'); % "lighting phong" does not work with opacity
material dull
camlight

view([1 0 0])
print -dpng natmeg_dip_geometry2.png

%% TF data spm 2 ft
D = spm_eeg_load('pbefL20.mat');
freqT_s04_L20_data = D.ftraw(0);

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'MEG'};
chanind = strmatch('neuromag306', D.chantype);

freqT_s04_L20_data.grad = cfg.grad;
freqT_s04_L20_data = ft_preprocessing(cfg, freqT_s04_L20_data);

save freqT_s04_L20_data freqT_s04_L20_data


D = spm_eeg_load('pbefR20.mat');
freqT_s04_R20_data = D.ftraw(0);

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'MEG'};
chanind = strmatch('neuromag306', D.chantype);

freqT_s04_R20_data.grad = cfg.grad;
freqT_s04_R20_data = ft_preprocessing(cfg, freqT_s04_R20_data);

save freqT_s04_R20_data freqT_s04_R20_data

%%
%% TF data spm 2 ft loop
clear all
close all

% subs = [2 3 5:20];
subs = 13:20;
for k = subs
    clearvars -except k
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    
    D = spm_eeg_load('pbefL20.mat');
    eval(['freqT_s' aa '_L20_data = D.ftraw(0)']);
    
    cfg = [];
    cfg.grad = D.sensors('MEG');
    cfg.channel = {'MEG'};
    chanind = strmatch('neuromag306', D.chantype);
    
    eval(['freqT_s' aa '_L20_data.grad = cfg.grad']);
    eval(['freqT_s' aa '_L20_data = ft_preprocessing(cfg, freqT_s' aa '_L20_data)']);
    
    eval(['save freqT_s' aa '_L20_data freqT_s' aa '_L20_data'])
    
    
    D = spm_eeg_load('pbefR20.mat');
    eval(['freqT_s' aa '_R20_data = D.ftraw(0)']);
    
    cfg = [];
    cfg.grad = D.sensors('MEG');
    cfg.channel = {'MEG'};
    chanind = strmatch('neuromag306', D.chantype);
    
    eval(['freqT_s' aa '_R20_data.grad = cfg.grad']);
    eval(['freqT_s' aa '_R20_data = ft_preprocessing(cfg, freqT_s' aa '_R20_data)']);
    
    eval(['save freqT_s' aa '_R20_data freqT_s' aa '_R20_data'])
    
end

%%
%
% load headmodel_meg
% figure;
% ft_plot_sens(freqT_s04_L20_data.grad,'style', '*');
% hold
% ft_plot_vol(headmodel_meg,'alpha', 0.5);


%% Contrast activity to another interval
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Calculating the cross spectral density matrix
%
% close all
% clear all
%
% load freqT_s03_L20_data
% % load data_L_1p5Hz
%
% cfg = [];
% cfg.toilim = [-0.2 0];
% dataPre = ft_redefinetrial(cfg, freqT_s03_L20_data);
%
% cfg.toilim = [0 11];
% dataPost = ft_redefinetrial(cfg, freqT_s03_L20_data);
%
%
% %%
% cfg = [];
% cfg.method    = 'mtmfft';
% cfg.output    = 'powandcsd';
% cfg.tapsmofrq = 0.7;
% cfg.foilim    = [3.3 3.7];
% freqPre = ft_freqanalysis(cfg, dataPre);
%
% cfg = [];
% cfg.method    = 'mtmfft';
% cfg.output    = 'powandcsd';
% cfg.tapsmofrq = 0.7;
% cfg.foilim    = [3.3 3.7];
% freqPost = ft_freqanalysis(cfg, dataPost);
%
% %%
% load headmodel_meg
%
% cfg                 = [];
% cfg.grad            = freqPost.grad;
% cfg.vol             = headmodel_meg;
% cfg.reducerank      = 2;
% cfg.channel         = {'MEG'};
% cfg.grid.resolution = 0.7;   % use a 3-D grid with a 1 cm resolution
% cfg.grid.unit       = 'cm';
% [grid] = ft_prepare_leadfield(cfg);


%% Source Analysis: Contrast activity to another interval
%
% dataAll = ft_appenddata([], dataPre, dataPost);
%
% cfg = [];
% cfg.method    = 'mtmfft';
% cfg.output    = 'powandcsd';
% cfg.tapsmofrq = 4;
% cfg.foilim    = [3.5 3.5];
% freqAll = ft_freqanalysis(cfg, dataAll);
%
% cfg              = [];
% cfg.method       = 'dics';
% cfg.frequency    = 3.5;
% cfg.grid         = grid;
% cfg.vol          = headmodel_meg;
% cfg.dics.projectnoise = 'yes';
% cfg.dics.lambda       = '5%';
% cfg.dics.keepfilter   = 'yes';
% cfg.dics.realfilter   = 'yes';
% sourceAll = ft_sourceanalysis(cfg, freqAll);
%
%
% cfg.grid.filter = sourceAll.avg.filter;
% sourcePre_con  = ft_sourceanalysis(cfg, freqPre );
% sourcePost_con = ft_sourceanalysis(cfg, freqPost);
%
% save sourcePre_con sourcePre_con
% save sourcePost_con sourcePost_con
%
% sourceDiff = sourcePost_con;
% sourceDiff.avg.pow = (sourcePost_con.avg.pow - sourcePre_con.avg.pow) ./ sourcePre_con.avg.pow;
% % sourceDiff.dim = sourcemodel.dim; % for normalised to template space
% % sourceDiff.pos =sourcemodel.pos; % for normalised to template space
% save sourceDiff sourceDiff
%
% load mri_segmented
% cfg            = [];
% cfg.downsample = 2;
% cfg.parameter  = 'avg.pow';
% sourceDiffInt  = ft_sourceinterpolate(cfg, sourceDiff , mri_segmented);
%%

% %sourceDiffInt.mask = sourceDiffInt.pow > sourceDiffInt.pow.*0.5;
% cfg = [];
% cfg.method        = 'ortho';
% cfg.funparameter  = 'avg.pow';
% cfg.maskparameter = 'mask';
% cfg.funcolorlim   = [-1 5];
% cfg.opacitylim    = [-1 5];
%
% ft_sourceplot(cfg, sourceDiffInt);



%% Source Analysis: Contrast activity between conditions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Calculating the cross spectral density matrix
close all
clear all
cd F:\2017_localiser\s02_cbr
load freqT_s02_L20_data
load freqT_s02_R20_data

cfg = [];
cfg.toilim = [0 11];
dataA = ft_redefinetrial(cfg, freqT_s02_L20_data);

cfg.toilim = [0 11];
dataB = ft_redefinetrial(cfg, freqT_s02_R20_data);

% %
cfg = [];
cfg.method    = 'mtmfft';
cfg.output    = 'powandcsd';
cfg.tapsmofrq = 0.7;
cfg.foilim    = [3.3 3.7];
freqA = ft_freqanalysis(cfg, dataA);

cfg = [];
cfg.method    = 'mtmfft';
cfg.output    = 'powandcsd';
cfg.tapsmofrq = 0.7;
cfg.foilim    = [3.3 3.7];
freqB = ft_freqanalysis(cfg, dataB);

% %
load headmodel_meg

cfg                 = [];
cfg.grad            = freqB.grad;
cfg.vol             = headmodel_meg;
cfg.reducerank      = 2;
cfg.channel         = {'MEG'};
cfg.grid.resolution = 0.7;   % use a 3-D grid with a 1 cm resolution
cfg.grid.unit       = 'cm';
[grid] = ft_prepare_leadfield(cfg);


% Source Analysis: Contrast activity btw conditions

dataAll = ft_appenddata([], dataA, dataB);

cfg = [];
cfg.method    = 'mtmfft';
cfg.output    = 'powandcsd';
cfg.tapsmofrq = 0.7;
cfg.foilim    = [3.3 3.7];
freqAll = ft_freqanalysis(cfg, dataAll);

cfg              = [];
cfg.method       = 'dics';
cfg.frequency    = 3.5;
cfg.grid         = grid;
cfg.headmodel         = headmodel_meg;
cfg.dics.projectnoise = 'yes';
cfg.dics.lambda       = '5%';
cfg.dics.keepfilter   = 'yes';
cfg.dics.realfilter   = 'yes';
sourceAll = ft_sourceanalysis(cfg, freqAll);


cfg.grid.filter = sourceAll.avg.filter;
sourceA_con  = ft_sourceanalysis(cfg, freqA);
sourceB_con = ft_sourceanalysis(cfg, freqB);

save sourceA_con sourceA_con
save sourceB_con sourceB_con

sourceDiff2Hz = sourceB_con;
sourceDiff2Hz.avg.pow = (sourceB_con.avg.pow - sourceA_con.avg.pow) ./ sourceA_con.avg.pow;
% sourceDiff.dim = sourcemodel.dim; % for normalised to template space
% sourceDiff.pos =sourcemodel.pos; % for normalised to template space
save sourceDiff2Hz sourceDiff2Hz

load mri_segmented

cfg            = [];
cfg.downsample = 2;
cfg.parameter  = 'avg.pow';
sourceDiffInt2Hz  = ft_sourceinterpolate(cfg, sourceDiff2Hz , mri_segmented);

%  2D plot
%sourceDiffInt.mask = sourceDiffInt.pow > sourceDiffInt.pow.*0.5;
cfg = [];
cfg.method        = 'ortho';
cfg.funparameter  = 'avg.pow';
cfg.maskparameter = 'mask';
cfg.funcolorlim   = [-0.5 0.5];
cfg.opacitylim    = [-0.5 0.5];

ft_sourceplot(cfg, sourceDiffInt2Hz);

saveas(gcf,'Fig_result_LR2_2ndHarmonic')
save sourceDiffInt2Hz  sourceDiffInt2Hz

% 3D plot
cfg = [];
cfg.nonlinear     = 'no';
sourceDiffIntNorm2Hz = ft_volumenormalise(cfg, sourceDiffInt2Hz);

cfg = [];
cfg.method         = 'surface';
cfg.funparameter   = 'avg.pow';
cfg.maskparameter  = cfg.funparameter;
cfg.funcolorlim    = [-0.5 0.5];
cfg.funcolormap    = 'jet';
cfg.opacitylim     = [-0.5 0.5];
cfg.opacitymap     = 'rampup';
cfg.projmethod     = 'nearest';
cfg.surffile       = 'surface_white_both.mat';
cfg.surfdownsample = 10;
ft_sourceplot(cfg, sourceDiffIntNorm2Hz);
view ([90 0])
saveas(gcf,'Fig3D_result_LR2_2ndHarmonic')
save sourceDiffIntNorm2Hz sourceDiffIntNorm2Hz

cfg= [];
cfg.parameter = 'pow';
cfg.filename = 'SResult2Hz';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, sourceDiffIntNorm2Hz)

%% Calculating the cross spectral density matrix loop
close all
clear all
subs = [3 5:20];
for k = subs
    close all
    clearvars -except k
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    
    eval(['load freqT_s' aa '_L20_data'])
    eval(['load freqT_s' aa '_R20_data'])
    
    cfg = [];
    cfg.toilim = [0 11];
    eval(['dataA = ft_redefinetrial(cfg, freqT_s' aa '_L20_data)']);
    
    cfg.toilim = [0 11];
    eval(['dataB = ft_redefinetrial(cfg, freqT_s' aa '_R20_data)']);
    
    % %
    cfg = [];
    cfg.method    = 'mtmfft';
    cfg.output    = 'powandcsd';
    cfg.tapsmofrq = 0.7;
    cfg.foilim    = [3.3 3.7];
    freqA = ft_freqanalysis(cfg, dataA);
    
    cfg = [];
    cfg.method    = 'mtmfft';
    cfg.output    = 'powandcsd';
    cfg.tapsmofrq = 0.7;
    cfg.foilim    = [3.3 3.7];
    freqB = ft_freqanalysis(cfg, dataB);
    
    % %
    load headmodel_meg
    
    cfg                 = [];
    cfg.grad            = freqB.grad;
    cfg.vol             = headmodel_meg;
    cfg.reducerank      = 2;
    cfg.channel         = {'MEG'};
    cfg.grid.resolution = 0.7;   % use a 3-D grid with a 1 cm resolution
    cfg.grid.unit       = 'cm';
    [grid] = ft_prepare_leadfield(cfg);
    
    
    % Source Analysis: Contrast activity btw conditions
    
    dataAll = ft_appenddata([], dataA, dataB);
    
    cfg = [];
    cfg.method    = 'mtmfft';
    cfg.output    = 'powandcsd';
    cfg.tapsmofrq = 0.7;
    cfg.foilim    = [3.3 3.7];
    freqAll = ft_freqanalysis(cfg, dataAll);
    
    cfg              = [];
    cfg.method       = 'dics';
    cfg.frequency    = 3.5;
    cfg.grid         = grid;
    cfg.headmodel         = headmodel_meg;
    cfg.dics.projectnoise = 'yes';
    cfg.dics.lambda       = '5%';
    cfg.dics.keepfilter   = 'yes';
    cfg.dics.realfilter   = 'yes';
    sourceAll = ft_sourceanalysis(cfg, freqAll);
    
    
    cfg.grid.filter = sourceAll.avg.filter;
    sourceA_con  = ft_sourceanalysis(cfg, freqA);
    sourceB_con = ft_sourceanalysis(cfg, freqB);
    
    save sourceA_con sourceA_con
    save sourceB_con sourceB_con
    
    sourceDiff2Hz = sourceB_con;
    sourceDiff2Hz.avg.pow = (sourceB_con.avg.pow - sourceA_con.avg.pow) ./ sourceA_con.avg.pow;
    % sourceDiff.dim = sourcemodel.dim; % for normalised to template space
    % sourceDiff.pos =sourcemodel.pos; % for normalised to template space
    save sourceDiff2Hz sourceDiff2Hz
    
    load mri_segmented
    
    cfg            = [];
    cfg.downsample = 2;
    cfg.parameter  = 'avg.pow';
    sourceDiffInt2Hz  = ft_sourceinterpolate(cfg, sourceDiff2Hz , mri_segmented);
    
    %  2D plot
    %sourceDiffInt.mask = sourceDiffInt.pow > sourceDiffInt.pow.*0.5;
    cfg = [];
    cfg.method        = 'ortho';
    cfg.funparameter  = 'avg.pow';
    cfg.maskparameter = 'mask';c
    cfg.funcolorlim   = [-0.5 0.5];
    cfg.opacitylim    = [-0.5 0.5];
    
    ft_sourceplot(cfg, sourceDiffInt2Hz);
    
    saveas(gcf,'Fig_result_LR2_2ndHarmonic')
    save sourceDiffInt2Hz  sourceDiffInt2Hz
    
    % 3D plot
    cfg = [];
    cfg.nonlinear     = 'no';
    sourceDiffIntNorm2Hz = ft_volumenormalise(cfg, sourceDiffInt2Hz);
    
    cfg = [];
    cfg.method         = 'surface';
    cfg.funparameter   = 'avg.pow';
    cfg.maskparameter  = cfg.funparameter;
    cfg.funcolorlim    = [-0.5 0.5];
    cfg.funcolormap    = 'jet';
    cfg.opacitylim     = [-0.5 0.5];
    cfg.opacitymap     = 'rampup';
    cfg.projmethod     = 'nearest';
    cfg.surffile       = 'surface_white_both.mat';
    cfg.surfdownsample = 10;
    ft_sourceplot(cfg, sourceDiffIntNorm2Hz);
    view ([90 0])
    saveas(gcf,'Fig3D_result_LR2_2ndHarmonic')
    save sourceDiffIntNorm2Hz sourceDiffIntNorm2Hz
    
    cfg= [];
    cfg.parameter = 'pow';
    cfg.filename = 'SResult2Hz';
    cfg.filetype = 'nifti';
    cfg.vmpversion = 2;
    cfg.coordsys = 'spm';
    ft_volumewrite(cfg, sourceDiffIntNorm2Hz)
    
end