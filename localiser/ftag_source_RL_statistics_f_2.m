clear all
close all
grandavg_L15_f_2 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L15_f_2.mat']);
   grandavg_L15_f_2{a} = sourceDiffIntNorm_L15_f_2;
   a = a+1;
end
cd F:\2017_localiser
save -v7.3 grandavg_L15_f_2 grandavg_L15_f_2

clear all
close all
grandavg_R15_f_2 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R15_f_2.mat']);
   grandavg_R15_f_2{a} = sourceDiffIntNorm_R15_f_2;
   a = a+1;
end
cd F:\2017_localiser
save -v7.3 grandavg_R15_f_2 grandavg_R15_f_2


%% run statistics over subjects %
clc
clear all
close all
load grandavg_L20_f_2
load grandavg_R20_f_2

cfg=[];
cfg.dim         = grandavg_R20_f_2{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.05; % 0.33 t>1
cfg.tail        = 0;

nsubj=numel(grandavg_R20_f_2);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_R20_f_2{:}, grandavg_L20_f_2{:});

save -v7.3 RL20_f_2_source_stat_p33 stat


%% nifti
load RL20_f_2_source_stat_p33
% 
cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'RL20_f_2_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
cfg.datatype = 'float';
ft_volumewrite(cfg, stat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

clear all
close all
grandavg_R20 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R20.mat']);
   grandavg_R20{a} = sourceDiffIntNorm_R20;
   a = a+1;
end
save -v7.3 grandavg_R20 grandavg_R20

clear all
close all
grandavg_L20 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20.mat']);
   grandavg_L20{a} = sourceDiffIntNorm_L20;
   a = a+1;
end
save -v7.3 grandavg_L20 grandavg_L20

%% run statistics over subjects %
clc
clear all
close all
load grandavg_L20
load grandavg_R20

cfg=[];
cfg.dim         = grandavg_L20{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_L20);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_L20{:}, grandavg_R20{:});

save -v7.3 LR20_source_stat_p33 stat


%% nifti
load LR20_source_stat_p33

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'LR20_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);
%%
clc
clear all
close all
cd F:\2017_localiser
grandavg_R20_f_220 = cell(1,36);
subs = [2 3 5:20];
a=1;
for k = subs
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R20_f_2.mat']);
    grandavg_R20_f_220{a} = sourceDiffIntNorm_R20_f_2;
    a = a+1;
    eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R20.mat']);
    grandavg_R20_f_220{a} = sourceDiffIntNorm_R20;
    a = a+1;
end
save -v7.3 grandavg_R20_f_220 grandavg_R20_f_220

clear all
close all
grandavg_L20_f_220 = cell(1,36);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20_f_2.mat']);
   grandavg_L20_f_220{a} = sourceDiffIntNorm_L20_f_2;
   a = a+1;
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20.mat']);
   grandavg_L20_f_220{a} = sourceDiffIntNorm_L20;
   a = a+1;
end
save -v7.3 grandavg_L20_f_220 grandavg_L20_f_220

%% run statistics over subjects %
clc
clear all
close all
cd F:\2017_localiser
load grandavg_R20_f_220
load grandavg_L20_f_220

cfg=[];
cfg.dim         = grandavg_R20_f_220{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_R20_f_220);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_R20_f_220{:}, grandavg_L20_f_220{:});

save -v7.3 RL20_f_220_source_stat_p33 stat


%% nifti
cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'RL20_f_220_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);

%% run statistics over subjects L20_f_2 vs L20 %
clc
clear all
close all
load grandavg_L20_f_2
load grandavg_L20

cfg=[];
cfg.dim         = grandavg_L20_f_2{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_L20_f_2);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_L20_f_2{:}, grandavg_L20{:});

save -v7.3 L20_f_220_source_stat_p33 stat


%% nifti
load L20_f_220_source_stat_p33

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'L20_f_220_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);