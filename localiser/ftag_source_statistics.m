clear all
close all
cd F:\2017_localiser
grandavg = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm2Hz.mat']);
   grandavg{a} = sourceDiffIntNorm2Hz;
   a = a+1;
end
save -v7.3 grandavg grandavg

%% run statistics over subjects %
clear all
close all
cd F:\2017_localiser
load grandavg

cfg=[];
cfg.dim         = grandavg{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'indepsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.05; % note that this only implies single-sided testing
cfg.tail        = 0;

nsubj=numel(grandavg);
cfg.design(1,:) = [1:nsubj];
cfg.design(2,:) = [ones(1,nsubj)];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg{:});

save stat_2Hz stat