close all
clear all


%% step 1 - read MRI data and get coord info from MEG
mri_orig = ft_read_mri('d:\toolkit\real_data\IM02\T1\sSbj001_KBC_150129-0002-00001-000192-01.img');
dataset = 'd:\toolkit\real_data\IM02\face_raw_trans_sss.fif';

grad    = ft_read_sens(dataset,'senstype','meg');
elec    = ft_read_sens(dataset,'senstype','eeg');
shape   = ft_read_headshape(dataset,'unit','cm');

figure;
ft_plot_headshape(shape);
ft_plot_sens(grad, 'style', '*b');
ft_plot_sens(elec, 'style', '*g');

view([1 0 0])
print -dpng natmeg_dip_geometry1.png

figure;
cfg = [];
ft_sourceplot(cfg, mri_orig);

save mri_orig mri_orig

%% step 2 -  MRI coregister
cfg = [];
cfg.method = 'interactive';
cfg.coordsys = 'neuromag';
[mri_realigned1] = ft_volumerealign(cfg, mri_orig);

save mri_realigned1 mri_realigned1


cfg = [];
cfg.method = 'headshape';
cfg.headshape = shape;
[mri_realigned2] = ft_volumerealign(cfg, mri_realigned1);

save mri_realigned2 mri_realigned2


%% step 3 -  MRI reslice

cfg = [];
cfg.resolution = 1;
cfg.xrange = [-100 100];
cfg.yrange = [-110 140];
cfg.zrange = [-50 150];
mri_resliced = ft_volumereslice(cfg, mri_realigned2);

save mri_resliced mri_resliced

figure
ft_sourceplot([], mri_resliced);
print -dpng natmeg_dip_mri_resliced.png

mri_resliced_cm = ft_convert_units(mri_resliced, 'cm');
save mri_resliced_cm mri_resliced_cm

%% step 4 - Construct the MEG volume conduction model

cfg           = [];
cfg.output    = {'brain', 'skull', 'scalp'};
mri_segmented = ft_volumesegment(cfg, mri_resliced);

mri_segmented.anatomy = mri_resliced.anatomy;
save mri_segmented mri_segmented


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

cfg = [];
cfg.funparameter = 'brain';
ft_sourceplot(cfg, mri_segmented);
print -dpng natmeg_dip_segmented_brain.png

cfg.funparameter = 'skull';
ft_sourceplot(cfg, mri_segmented);
print -dpng natmeg_dip_segmented_skull.png

cfg.funparameter = 'scalp';
ft_sourceplot(cfg, mri_segmented);
print -dpng natmeg_dip_segmented_scalp.png


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg = [];
cfg.method = 'projectmesh';
cfg.tissue = 'brain';
cfg.numvertices = 3000;
mesh_brain = ft_prepare_mesh(cfg, mri_segmented);


cfg = [];
cfg.method = 'projectmesh';
cfg.tissue = 'skull';
cfg.numvertices = 2000;
mesh_skull = ft_prepare_mesh(cfg, mri_segmented);

cfg = [];
cfg.method = 'projectmesh';
cfg.tissue = 'scalp';
cfg.numvertices = 1000;
mesh_scalp = ft_prepare_mesh(cfg, mri_segmented);

cfg = [];
cfg.method = 'isosurface';
cfg.tissue = 'scalp';
cfg.numvertices = 16000;
highres_scalp = ft_prepare_mesh(cfg, mri_segmented);

save mesh mesh_* highres_scalp
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
ft_plot_mesh(mesh_scalp, 'edgecolor', 'none', 'facecolor', 'skin')
material dull
camlight
lighting phong
print -dpng natmeg_dip_scalp.png


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure
ft_plot_mesh(highres_scalp, 'edgecolor', 'none', 'facecolor', 'skin')
material dull
camlight
lighting phong
print -dpng natmeg_dip_highres_scalp.png


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
cfg = [];
cfg.method = 'singleshell';
headmodel_meg = ft_prepare_headmodel(cfg, mesh_brain);

headmodel_meg = ft_convert_units(headmodel_meg,'cm');

save headmodel_meg headmodel_meg

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
figure;
hold on
ft_plot_headshape(shape);
ft_plot_sens(grad, 'style', 'ob');
ft_plot_sens(elec, 'style', 'og');
ft_plot_vol(headmodel_meg, 'facealpha', 0.5, 'edgecolor', 'none'); % "lighting phong" does not work with opacity
material dull
camlight

view([1 0 0]) 
print -dpng natmeg_dip_geometry2.png

%% checking TF data
close all
clear all

load tf_im02_face_iv_data
load tf_im02_face_up_data
load tf_im02_face_iv_comb
load tf_im02_face_up_comb

cfg = [];
cfg.baseline     = [-0.3 -0.1]; 
cfg.baselinetype = 'absolute'; 
cfg.zlim         = [-10e-24 10e-24];	        
cfg.showlabels   = 'yes';	
cfg.layout       = 'd:\toolkit\fieldtrip\template\layout\neuromag306planar.lay';
figure 
ft_multiplotTFR(cfg, tf_pg);

%% Process the MEG data
clear all

cfg = [];
dataset = 'd:\toolkit\real_data\IM02\face_raw_trans_sss.fif';

cfg.dataset = dataset;
cfg.trialfun                = 'ft_trialfun_general'; % this is the default
cfg.trialdef.eventtype      = 'STI101';
cfg.trialdef.eventvalue     = 31; 
cfg.trialdef.prestim        = 0.5; % in seconds
cfg.trialdef.poststim       = 1; % in seconds
cfg = ft_definetrial(cfg);

cfg.channel    = {'MEG'};
cfg.continuous = 'yes';
cfg.demean     = 'yes';
cfg.baselinewindow  = [-0.2 0];
cfg.lpfilter   = 'yes';                           
cfg.lpfreq     = 100;   
data_face_up = ft_preprocessing(cfg);
save data_face_up data_face_up


clear all

cfg = [];
dataset = 'd:\toolkit\real_data\IM02\face_raw_trans_sss.fif';

cfg.dataset = dataset;
cfg.trialfun                = 'ft_trialfun_general'; % this is the default
cfg.trialdef.eventtype      = 'STI101';
cfg.trialdef.eventvalue     = 32; 
cfg.trialdef.prestim        = 0.5; % in seconds
cfg.trialdef.poststim       = 1; % in seconds
cfg = ft_definetrial(cfg);

cfg.channel    = {'MEG'};
cfg.continuous = 'yes';
cfg.demean     = 'yes';
cfg.baselinewindow  = [-0.2 0];
cfg.lpfilter   = 'yes';                           
cfg.lpfreq     = 100;   
data_face_iv = ft_preprocessing(cfg);
save data_face_iv data_face_iv
clear all

load data_face_up
load data_face_iv

cfg = [];
avg_face_up = ft_timelockanalysis(cfg, data_face_up);
avg_face_iv = ft_timelockanalysis(cfg, data_face_iv);

cfg = [];
cfg.showlabels = 'yes'; 
cfg.fontsize = 6; 
cfg.layout       = 'd:\toolkit\fieldtrip\template\layout\neuromag306planar.lay';
cfg.ylim = [-3e-12 3e-12];
ft_multiplotER(cfg, avg_face_up, avg_face_iv); 


%%

close all
clear all

load data_face_up
load data_face_iv

cfg = [];
cfg.output     = 'pow';
cfg.channel    = 'MEG';
cfg.method     = 'mtmconvol';
cfg.foi        = 1:1:60;
cfg.t_ftimwin  = 5./cfg.foi;
cfg.tapsmofrq  = 0.4 *cfg.foi;
cfg.toi        = -0.5:0.05:1.0;
tf_face_up = ft_freqanalysis(cfg, data_face_up);
tf_face_iv = ft_freqanalysis(cfg, data_face_iv);

save tf_face_up tf_face_up
save tf_face_iv tf_face_iv

load tf_face_up
load tf_face_iv

cfg = [];
cfg.baseline     = [-0.3 -0.1]; 
cfg.baselinetype = 'absolute'; 
cfg.zlim         = [-8e-25 8e-25];	        
cfg.showlabels   = 'yes';	
cfg.layout       = 'd:\toolkit\fieldtrip\template\layout\neuromag306planar.lay';


figure 
ft_multiplotTFR(cfg, tf_face_up);
figure
ft_multiplotTFR(cfg, tf_face_iv);

%%
load headmodel_meg
figure;
ft_plot_sens(data_face_up.grad,'style', '*');
hold
ft_plot_vol(headmodel_meg,'alpha', 0.5);

%% Calculating the cross spectral density matrix
close all
clear all

load data_face_up
%load data_face_iv

cfg = [];                                           
cfg.toilim = [-.3 0];                       
dataPre = ft_redefinetrial(cfg, data_face_up);
   
cfg.toilim = [0.3 0.6];                       
dataPost = ft_redefinetrial(cfg, data_face_up);

%%
cfg = [];
cfg.method    = 'mtmfft';
cfg.output    = 'powandcsd';
cfg.tapsmofrq = 4;
cfg.foilim    = [15 15];
freqPre = ft_freqanalysis(cfg, dataPre);

cfg = [];
cfg.method    = 'mtmfft';
cfg.output    = 'powandcsd';
cfg.tapsmofrq = 4;
cfg.foilim    = [15 15];
freqPost = ft_freqanalysis(cfg, dataPost);

%%
load headmodel_meg

cfg                 = [];
cfg.grad            = freqPost.grad;
cfg.vol             = headmodel_meg;
cfg.reducerank      = 2;
cfg.channel         = {'MEG'};
cfg.grid.resolution = 1;   % use a 3-D grid with a 1 cm resolution
cfg.grid.unit       = 'cm';
[grid] = ft_prepare_leadfield(cfg);

%% Source Analysis: without contrasting condition

cfg              = []; 
cfg.method       = 'dics';
cfg.frequency    = 18;  
cfg.grid         = grid; 
cfg.vol          = headmodel_meg;
cfg.dics.projectnoise = 'yes';
cfg.dics.lambda       = 0;

sourcePost_nocon = ft_sourceanalysis(cfg, freqPost);


save sourcePost_nocon sourcePost_nocon

%%
load mri_resliced
load mri_segmented

% mri = ft_read_mri('d:\toolkit\real_data\IM02\T1\sSbj001_KBC_150129-0002-00001-000192-01.img');
% mri = ft_volumereslice([], mri);

cfg            = [];
cfg.downsample = 2;
cfg.parameter = 'avg.pow';
sourcePostInt_nocon  = ft_sourceinterpolate(cfg, sourcePost_nocon , mri_segmented);

cfg              = [];
cfg.method       = 'slice';
cfg.funparameter = 'avg.pow';
figure
ft_sourceplot(cfg,sourcePostInt_nocon);


sourceNAI = sourcePost_nocon;
sourceNAI.avg.pow = sourcePost_nocon.avg.pow ./ sourcePost_nocon.avg.noise;
 
cfg = [];
cfg.downsample = 2;
cfg.parameter = 'avg.pow';
sourceNAIInt = ft_sourceinterpolate(cfg, sourceNAI , mri_segmented);

cfg = [];
cfg.method        = 'slice';
cfg.funparameter  = 'avg.pow';
cfg.maskparameter = cfg.funparameter;
cfg.funcolorlim   = [4 100];
cfg.opacitylim    = [4 100]; 
cfg.opacitymap    = 'rampup';  
figure
ft_sourceplot(cfg, sourceNAIInt);

%% Source Analysis: Contrast activity to another interval

dataAll = ft_appenddata([], dataPre, dataPost);

cfg = [];
cfg.method    = 'mtmfft';
cfg.output    = 'powandcsd'; 
cfg.tapsmofrq = 4;
cfg.foilim    = [15 15];
freqAll = ft_freqanalysis(cfg, dataAll);

cfg              = [];
cfg.method       = 'dics';
cfg.frequency    = 15;
cfg.grid         = grid;
cfg.vol          = headmodel_meg;
cfg.dics.projectnoise = 'yes';
cfg.dics.lambda       = '5%';
cfg.dics.keepfilter   = 'yes';
cfg.dics.realfilter   = 'yes';
sourceAll = ft_sourceanalysis(cfg, freqAll);


cfg.grid.filter = sourceAll.avg.filter;
sourcePre_con  = ft_sourceanalysis(cfg, freqPre );
sourcePost_con = ft_sourceanalysis(cfg, freqPost);

save sourcePre_con sourcePre_con 
save sourcePost_con sourcePost_con

sourceDiff = sourcePost_con;
sourceDiff.avg.pow = (sourcePost_con.avg.pow - sourcePre_con.avg.pow) ./ sourcePre_con.avg.pow;
% sourceDiff.dim = sourcemodel.dim; % for normalised to template space
% sourceDiff.pos =sourcemodel.pos; % for normalised to template space
save sourceDiff sourceDiff


cfg            = [];
cfg.downsample = 2;
cfg.parameter  = 'avg.pow';
sourceDiffInt  = ft_sourceinterpolate(cfg, sourceDiff , mri_segmented);
%%

%sourceDiffInt.mask = sourceDiffInt.pow > sourceDiffInt.pow.*0.5;
cfg = [];
cfg.method        = 'ortho';
cfg.funparameter  = 'avg.pow';
cfg.maskparameter = 'mask';
cfg.funcolorlim   = [-.5 .5];
cfg.opacitylim    = [-.5 .5]; 
  
ft_sourceplot(cfg, sourceDiffInt);
%%
cfg=[];
cfg.covariance = 'yes';
cfg.covariancewindow = [.1 .2];
avg = ft_timelockanalysis(cfg,data_face_up);

cfg =[];
cfg.method = 'lcmv';
cfg.method           = 'lcmv';
cfg.grid             = grid; % leadfield, which has the grid information
cfg.vol              = headmodel_meg; % volume conduction model (headmodel)
cfg.keepfilter       = 'yes';
cfg.lcmv.fixedori    = 'yes'; % project on axis of most variance using SVD
source_avg           = ft_sourceanalysis(cfg, avg);
%%
cfg = [];                                           
cfg.toilim = [-.3 -.1];                       
dataPre = ft_redefinetrial(cfg, data_face_up);
   
cfg.toilim = [0.1 0.3];                       
dataPost = ft_redefinetrial(cfg, data_face_up);
cfg=[];
cfg.covariance = 'yes';
avgpst = ft_timelockanalysis(cfg,dataPost);
avgpre = ft_timelockanalysis(cfg,dataPre);

cfg =[];
cfg.method = 'lcmv';
cfg.grid             = grid; % leadfield, which has the grid information
cfg.vol              = headmodel_meg; % volume conduction model (headmodel)
% cfg.grid.filter      = source_avg.avg.filter;
source_avgpre           = ft_sourceanalysis(cfg, avgpre);
source_avgpst           = ft_sourceanalysis(cfg, avgpst);
%%

sourceDiff = source_avgpst;
sourceDiff.avg.pow = (source_avgpst.avg.pow - source_avgpre.avg.pow) ./ source_avgpre.avg.pow;

cfg            = [];
cfg.downsample = 2;
cfg.parameter  = 'avg.pow';
sourceDiffInt  = ft_sourceinterpolate(cfg, sourceDiff , mri_segmented);


%%
%sourceDiffInt.mask= sourceDiffInt.pow > sourceDiffInt.pow*0.9;
cfg = [];
cfg.method        = 'ortho';
cfg.funparameter  = 'avg.pow';
% cfg.maskparameter = 'mask';
% cfg.funcolorlim   = [-.5 .5];
% cfg.opacitylim    = [-.5 .5]; 
  
ft_sourceplot(cfg, sourceDiffInt);

%%
load('standard_sourcemodel3d10mm.mat') % for normalised to template space (can be more fine)
cfg = [];
cfg.grid.warpmni   = 'yes';
cfg.grid.template  = sourcemodel;
cfg.grid.nonlinear = 'yes'; % use non-linear normalization
cfg.mri            = mri_segmented;
grid               = ft_prepare_sourcemodel(cfg);
grid = ft_convert_units(grid, 'cm');
% make a figure of the single subject headmodel, and grid positions
figure;
ft_plot_vol(headmodel_meg, 'edgecolor', 'none'); alpha 0.4;
ft_plot_mesh(grid.pos(grid.inside,:));


%%

%% Source Analysis: Contrast activity to another interval

dataAll = ft_appenddata([], dataPre, dataPost);

cfg = [];
cfg.method    = 'mtmfft';
cfg.output    = 'powandcsd'; 
cfg.tapsmofrq = 4;
cfg.foilim    = [15 15];
freqAll = ft_freqanalysis(cfg, dataAll);

cfg              = [];
cfg.method       = 'dics';
cfg.frequency    = 15;
cfg.grid         = grid;
cfg.vol          = headmodel_meg;
cfg.dics.projectnoise = 'yes';
cfg.dics.lambda       = '5%';
cfg.dics.keepfilter   = 'yes';
cfg.dics.realfilter   = 'yes';
sourceAll = ft_sourceanalysis(cfg, freqAll);


cfg.grid.filter = sourceAll.avg.filter;
sourcePre_con  = ft_sourceanalysis(cfg, freqPre );
sourcePost_con = ft_sourceanalysis(cfg, freqPost);

save sourcePre_con sourcePre_con 
save sourcePost_con sourcePost_con

sourceDiff = sourcePost_con;
sourceDiff.avg.pow = (sourcePost_con.avg.pow - sourcePre_con.avg.pow) ./ sourcePre_con.avg.pow;
sourceDiff.dim = sourcemodel.dim; % for normalised to template space
sourceDiff.pos =sourcemodel.pos; % for normalised to template space
save sourceDiff sourceDiff

mri = ft_read_mri('d:\toolkit\fieldtrip\template\anatomy\single_subj_T1.nii');


cfg            = [];
cfg.downsample = 2;
cfg.parameter  = 'avg.pow';
sourceDiffInt  = ft_sourceinterpolate(cfg, sourceDiff , mri);

%%
