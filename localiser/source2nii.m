%% spm
close all
clear all
subs = [3 5:20];
for k = subs
    clearvars -except k
    aa = sprintf('s%02d_cbr',k);
    eval(['cd F:\2017_localiser\' aa]);
    
    load sourceDiffIntNorm_L15
    cfg= [];
    cfg.parameter = 'pow';
    cfg.filename = 'sourceDiffIntNorm_L15';
    cfg.filetype = 'analyze_spm'; %analyze', 'nifti', 'nifti_img', 'analyze_spm', 'mgz','vmp' or 'vmr'                    
    cfg.vmpversion = 2;
    cfg.coordsys = 'spm';
    cfg.datatype = 'float';
    ft_volumewrite(cfg, sourceDiffIntNorm_L15);
    
    load sourceDiffIntNorm_R15
    cfg= [];
    cfg.parameter = 'pow';
    cfg.filename = 'sourceDiffIntNorm_R15';
    cfg.filetype = 'nifti';
    cfg.vmpversion = 2;
    cfg.coordsys = 'spm';
    cfg.datatype = 'float';
    ft_volumewrite(cfg, sourceDiffIntNorm_R15);
    clearvars -except k
   
    load sourceDiffIntNorm_L20
    cfg= [];
    cfg.parameter = 'pow';
    cfg.filename = 'sourceDiffIntNorm_L20';
    cfg.filetype = 'nifti';
    cfg.vmpversion = 2;
    cfg.coordsys = 'spm';
    cfg.datatype = 'float';
    ft_volumewrite(cfg, sourceDiffIntNorm_L20);
    
    load sourceDiffIntNorm_R20
    cfg= [];
    cfg.parameter = 'pow';
    cfg.filename = 'sourceDiffIntNorm_R20';
    cfg.filetype = 'nifti';
    cfg.vmpversion = 2;
    cfg.coordsys = 'spm';
    cfg.datatype = 'float';
    ft_volumewrite(cfg, sourceDiffIntNorm_R20);
    clearvars -except k
    
end