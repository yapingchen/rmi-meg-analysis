spm('defaults', 'eeg');

D = spm_eeg_load('pfpbefL15.mat');
freqT_band_s02_L15_data = D.ftraw(0);

cfg = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'MEG'};
chanind = strmatch('neuromag306', D.chantype);

freqT_band_s02_L15_data.grad = cfg.grad;
freqT_band_s02_L15_data = ft_preprocessing(cfg, freqT_band_s02_L15_data);

save freqT_band_s02_L15_data freqT_band_s02_L15_data

%% loop
clear all
spm('defaults', 'eeg');

for k = [2 3 5:20]
    aa = sprintf('s%02d',k);
    eval(['cd  F:\2017_localiser\' aa '_cbr']);
    
    D = spm_eeg_load('pfpbefR20.mat');
    eval(['freqT_band_2_' aa '_R20_data = D.ftraw(0)']);
    cfg = [];
    cfg.grad = D.sensors('MEG');
    cfg.channel = {'MEG'};
    chanind = strmatch('neuromag306', D.chantype);
    
    eval(['freqT_band_2_' aa '_R20_data.grad = cfg.grad']);
    eval(['freqT_band_2_' aa '_R20_data = ft_preprocessing(cfg, freqT_band_2_' aa '_R20_data)']);
    eval(['save freqT_band_2_' aa '_R20_data freqT_band_2_' aa '_R20_data']);
    
end