clear all
close all
grandavg_R15 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R15.mat']);
   grandavg_R15{a} = sourceDiffIntNorm_R15;
   a = a+1;
end
save -v7.3 grandavg_R15 grandavg_R15

clear all
close all
grandavg_L15 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L15.mat']);
   grandavg_L15{a} = sourceDiffIntNorm_L15;
   a = a+1;
end
save -v7.3 grandavg_L15 grandavg_L15

%% run statistics over subjects %
clc
clear all
close all
load grandavg_L15
load grandavg_R15

cfg=[];
cfg.dim         = grandavg_L15{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_L15);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_L15{:}, grandavg_R15{:});

save -v7.3 LR15_source_stat_p33 stat


%% nifti
load LR15_source_stat_p33

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'LR15_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

clear all
close all
grandavg_R20_Hanning = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R20_Hanning.mat']);
   grandavg_R20_Hanning{a} = sourceDiffIntNorm_R20;
   a = a+1;
end
save -v7.3 grandavg_R20_Hanning grandavg_R20_Hanning

clear all
close all
grandavg_L20_Hanning = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20_Hanning.mat']);
   grandavg_L20_Hanning{a} = sourceDiffIntNorm_L20;
   a = a+1;
end
save -v7.3 grandavg_L20_Hanning grandavg_L20_Hanning

%% run statistics over subjects %
clc
clear all
close all
load grandavg_L20_Hanning
load grandavg_R20_Hanning

cfg=[];
cfg.dim         = grandavg_R20_Hanning{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_R20_Hanning);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_R20_Hanning{:}, grandavg_L20_Hanning{:});

save -v7.3 RL20_source_stat_p33_Hanning stat


% nifti

clear all
close all
load RL20_source_stat_p33_Hanning

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'RL20_source_stat_p33_Hanning';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);
%%
clc
clear all
close all
cd F:\2017_localiser
grandavg_R1520 = cell(1,36);
subs = [2 3 5:20];
a=1;
for k = subs
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R15.mat']);
    grandavg_R1520{a} = sourceDiffIntNorm_R15;
    a = a+1;
    eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R20_Hanning.mat']);
    grandavg_R1520{a} = sourceDiffIntNorm_R20_Hanning;
    a = a+1;
end
save -v7.3 grandavg_R1520 grandavg_R1520

clear all
close all
grandavg_L1520 = cell(1,36);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L15.mat']);
   grandavg_L1520{a} = sourceDiffIntNorm_L15;
   a = a+1;
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20_Hanning.mat']);
   grandavg_L1520{a} = sourceDiffIntNorm_L20_Hanning;
   a = a+1;
end
save -v7.3 grandavg_L1520 grandavg_L1520

%% run statistics over subjects %
clc
clear all
close all
cd F:\2017_localiser
load grandavg_R1520
load grandavg_L1520

cfg=[];
cfg.dim         = grandavg_R1520{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_R1520);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_R1520{:}, grandavg_L1520{:});

save -v7.3 RL1520_source_stat_p33 stat


%% nifti
cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'RL1520_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);

%% run statistics over subjects L15 vs L20 %
clc
clear all
close all
load grandavg_L15
load grandavg_L20_Hanning

cfg=[];
cfg.dim         = grandavg_L15{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_L15);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_L15{:}, grandavg_L20_Hanning{:});

save -v7.3 L1520_source_stat_p33 stat


%% nifti
load L1520_source_stat_p33

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'L1520_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);