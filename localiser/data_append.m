clear all
%%% 1.5 Hz
subs = [2 3 5:20];
for k = subs
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    eval(['load freqT_band_s' aa '_L15_data']);
    eval(['load freqT_band_s' aa '_R15_data']);
    eval(['freqT_band_s' aa '_15_data = ft_appenddata([], freqT_band_s' aa '_L15_data, freqT_band_s' aa '_R15_data)']);
    eval(['save freqT_band_s' aa '_15_data freqT_band_s' aa '_15_data']);
end

clear all
%%% 2.0 Hz
subs = [2 3 5:20];
for k = subs
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    eval(['load freqT_band_s' aa '_L20_data']);
    eval(['load freqT_band_s' aa '_R20_data']);
    eval(['freqT_band_s' aa '_20_data = ft_appenddata([], freqT_band_s' aa '_L20_data, freqT_band_s' aa '_R20_data)']);
    eval(['save freqT_band_s' aa '_20_data freqT_band_s' aa '_20_data']);
end


