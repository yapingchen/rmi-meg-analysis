D = spm_eeg_load('F:\2017_localiser\test\pbefL15.mat');
data = D.ftraw(0);
data.fsample = 250;
cfg             = [];
cfg.grad = D.sensors('MEG');
cfg.channel = {'neuromag306'};
chanind = strmatch('neuromag306', D.chantype);

% https://github.com/OHBA-analysis/osl-core/blob/master/HCP/HCP_beamform.m
% cfg.polyremoval = 'yes';
% cfg.polyorder   = 2;
% data            = ft_preprocessing(cfg,data);
data.trial      = {cat(2, data.trial{:})};
data.time       = {(1:length([data.time{:}]))./data.fsample}; % not real time, due to bad trials having been removed
data.trialinfo  = [1];


data.hdr       = ft_read_header('F:\2017_localiser\test\pbefL15.mat');
D2             = spm_eeg_ft2spm(data, 'filename');

D2 = fiducials(D2,D.fiducials);
% D2 = conditions(D2, [], D.conditions);
% D2 = events(D2, [], D.events);
D2 = type(D2, D.type);
D2 = timeonset(D2, D.timeonset);

% channel match
% https://www.jiscmail.ac.uk/cgi-bin/webadmin?A3=ind1110&L=SPM&E=7bit&P=2899752&B=--------------080400010807090100080700&T=text%2Fhtml;%20charset=ISO-8859-1&pending=
All_Channel_Categories = unique(D.chantype);
[non, Num_Of_Unique_Chan_Categories] = size(All_Channel_Categories);

for Channel_Category_Counter = 1: Num_Of_Unique_Chan_Categories
%     disp([' '])
%     disp([' '])
%     disp([' '])
    %..display the name of the current channel category
    display(All_Channel_Categories(Channel_Category_Counter))
    
    %...find all the channels (that is, their labels) that belong to
    %this category in the original spm object (original== before
    %conversion)... and store them in the temp variable
    %"Channel_Names_Of_This_Category"
    
    Channel_Names_Of_This_Category = []; %this will each time contain the label names (i.e. 'MLP52'... of those channels that belong to the current category (i.e. MEGGRAD)
    Channel_Names_Of_This_Category = D.chanlabels(find(strcmp(D.chantype, All_Channel_Categories(Channel_Category_Counter))==1));
    
    %...count how many they are...
    [non, Num_Of_Channels_In_Current_Category] = size(Channel_Names_Of_This_Category);
    
    %..and for each of these channels...
    for Channel_In_Current_Category_Counter = 1:Num_Of_Channels_In_Current_Category
        
        %get its label name from the "Channel_Names_Of_This_Category" variable
        Current_Channel_Name = Channel_Names_Of_This_Category(Channel_In_Current_Category_Counter);
        
        %..find its ID  in the new object according to its name
        ID_Of_Current_ChanLabel_In_New_Object = find(strcmp(D2.chanlabels, Current_Channel_Name)==1);
        
        %...and set its chantype to be the one of the category it
        %actually belongs in the original D object
        D2 = chantype(D2, ID_Of_Current_ChanLabel_In_New_Object, All_Channel_Categories(Channel_Category_Counter));
        
        %      pause
    end
    
    
    %%note: the internal for loop could be avoided if the code followed the other direction,
    % starting from the label of the channel in the new spm object and then going back to
    % the original object to get its chantype, thus creating a list of chantypes that would
    %correspond to the order of the channels in the new file, and doing only
    %one single chantype assignment (which is slow) at the end
    
end

D2.save;

