
clear all
% close all
load F:\2017_localiser\s02_cbr\freqT_band_2_s02_R20_data
data = freqT_band_2_s02_R20_data;
cfg = [];
cfg.method = 'mtmfft';
cfg.taper = 'hanning';
cfg.output = 'powandcsd'; % for only one channel 'powandcr...'
cfg.foilim = [0.1 5];
freq = ft_freqanalysis(cfg, data);
% save freq freq

% plot(freq.freq, mean(freq.powspctrm));
%legend(freq.label)
shadedErrorBar(freq.freq, mean(freq.powspctrm), std(freq.powspctrm), '-r');

set(gca,'xlim',[0 5],'xtick',0:1:5)
% set(gca,'xlim',[0 100],'xtick',0:10:100)
%
% set(gca,'ylim',[0 2000],'ytick',0:500:2000)
%
% set(gca,'ylim',[0 20])

xlabel('Frequency (Hz)')
ylabel('Power')
title('band_2_s02')
% saveas (gcf, 's02_freq_pow_R2Hz')
% title('1 Hz Face')
% title('1.5 Hz House')
% title('2 Hz Object')
% title('2.5 Hz Gabor')
%
%
% title('1 Hz White Square')
% title('1.5 Hz White Square')
% title('2 Hz White Square')
% title('2.5 Hz White Square')

%%
close all
clear all
subs = [2 3 5:20];
for k = subs
    close all
    clearvars -except k
    aa = sprintf('%02d',k);
    eval(['load F:\2017_localiser\s' aa '_cbr\freqT_s' aa '_R20_data'])
    eval(['data = freqT_s' aa '_R20_data']);
    cfg = [];
    cfg.method = 'mtmfft';
    cfg.taper = 'hanning';
    cfg.output = 'powandcsd'; % for only one channel 'powandcr...'
    cfg.foilim = [1 5];
%     cfg.tapsmofrq = 0.5;
    freq = ft_freqanalysis(cfg, data);
    
    plot(freq.freq, freq.powspctrm);
    set(gca,'xlim',[1 5],'xtick',0:1:5)
    xlabel('Frequency (Hz)')
    ylabel('Power')
    
%     eval(['saveas (gcf, ''s' aa '_freq_pow_R2Hz'')'])
        
end

%% multiplot
close all
clear all
figure;
subs = [2 3 5:20];
m=1;
for k = subs
    subplot(6,3,m); 
    aa = sprintf('%02d',k);
    eval(['load F:\2017_localiser\s' aa '_cbr\freqT_band_2_s' aa '_R15_data'])
    eval(['data = freqT_band_2_s' aa '_R15_data']);
    cfg = [];
    cfg.method = 'mtmfft';
    cfg.taper = 'hanning';
    cfg.output = 'powandcsd'; % for only one channel 'powandcr...'
    cfg.foilim = [1 5];
%     cfg.tapsmofrq = 1;
    freq = ft_freqanalysis(cfg, data);
    
    plot(freq.freq, freq.powspctrm);
    set(gca,'xlim',[1 5],'xtick',0:1:5)
    xlabel('Frequency (Hz)')
m = m+1;
        
end

%% single sub 1.5 Hz & 2.Hz (1.38 Hz & 1.76 Hz)

clear all
close all
load F:\2017_localiser\s06_cbr\freqT_band_2_s06_L15_data
data = freqT_band_2_s06_L15_data;
cfg = [];
cfg.method = 'mtmfft';
cfg.taper = 'hanning';
cfg.output = 'powandcsd'; % powandcsd for only one channel 'powandcr...'
cfg.foilim = [1 2.5];
% cfg.channel = {'MEG2032','MEG2033','MEG2342','MEG2343','MEG2332','MEG2333','MEG2132','MEG2133',...
%     'MEG2312','MEG2313','MEG2322','MEG2323','MEG2512','MEG2513','MEG2542','MEG2543',...
%     'MEG2432','MEG2433','MEG2522','MEG2523','MEG2532','MEG2533','MEG2112','MEG2113','MEG2122','MEG2123',...
%     'MEG2042','MEG2043','MEG1922','MEG1923','MEG1932','MEG1933','MEG2142','MEG2143',...
%     'MEG1912','MEG1913','MEG1942','MEG1943','MEG1732','MEG1733','MEG1742','MEG1743',...
%     'MEG1642','MEG1643','MEG1722','MEG1723','MEG1712','MEG1713'};
freq15 = ft_freqanalysis(cfg, data);

load F:\2017_localiser\s06_cbr\freqT_band_2_s06_L20_data
data = freqT_band_2_s06_L20_data;
cfg = [];
cfg.method = 'mtmfft';
cfg.taper = 'hanning';
cfg.output = 'powandcsd'; % for only one channel 'powandcr...'
cfg.foilim = [1 2.5];
% cfg.channel = {'MEG2032','MEG2033','MEG2342','MEG2343','MEG2332','MEG2333','MEG2132','MEG2133',...
%     'MEG2312','MEG2313','MEG2322','MEG2323','MEG2512','MEG2513','MEG2542','MEG2543',...
%     'MEG2432','MEG2433','MEG2522','MEG2523','MEG2532','MEG2533','MEG2112','MEG2113','MEG2122','MEG2123',...
%     'MEG2042','MEG2043','MEG1922','MEG1923','MEG1932','MEG1933','MEG2142','MEG2143',...
%     'MEG1912','MEG1913','MEG1942','MEG1943','MEG1732','MEG1733','MEG1742','MEG1743',...
%     'MEG1642','MEG1643','MEG1722','MEG1723','MEG1712','MEG1713'};
freq20 = ft_freqanalysis(cfg, data);


figure
shadedErrorBar(freq15.freq, mean(freq15.powspctrm), std(freq15.powspctrm), '-r');
% hold on
figure
shadedErrorBar(freq20.freq, mean(freq20.powspctrm), std(freq20.powspctrm), '-b');
% hold off

% figure
% plot(freq15.freq, mean(freq15.powspctrm));
% figure
% plot(freq20.freq, mean(freq20.powspctrm));
% 
set(gca,'xlim',[1 2.5],'xtick',1:0.5:2.5)
set(gca,'ylim',[0 6e-25],'ytick',0:1e-25:6e-25)

%% between subs 1.5 Hz & 2.Hz

close all
clear all
subs = [2 3 5:20];
m=1;

for k = subs
    clearvars -except k m pow_15Hz_data
    aa = sprintf('%02d',k);
    eval(['load F:\2017_localiser\s' aa '_cbr\freqT_s' aa '_15_data'])
    eval(['data = freqT_s' aa '_15_data']);
    cfg = [];
    cfg.method = 'mtmfft';
    cfg.taper = 'hanning';
    cfg.output = 'powandcsd'; % powandcsd for only one channel 'powandcr...'
    cfg.foilim = [1 5];
    cfg.channel = {'MEG2022','MEG2023','MEG2032','MEG2033','MEG2312','MEG2313','MEG2322','MEG2323','MEG2512','MEG2513','MEG2532','MEG2533','MEG2342','MEG2343','MEG2332','MEG2333','MEG2542','MEG2543','MEG2112','MEG2113','MEG2122','MEG2123',...
        'MEG2012','MEG2013','MEG2042','MEG2043','MEG1912','MEG1913','MEG1922','MEG1923','MEG1932','MEG1933','MEG1942','MEG1943','MEG1712','MEG1713','MEG1732','MEG1733','MEG1742','MEG1743'};
    freq15 = ft_freqanalysis(cfg, data);
    pow_15Hz_data(m,:)=mean(freq15.powspctrm);
    m = m+1;
end  
subs = [2 3 5:20];
n=1;
for k = subs
    clearvars -except k n pow_15Hz_data pow_20Hz_data freq15
    aa = sprintf('%02d',k);
    eval(['load F:\2017_localiser\s' aa '_cbr\freqT_s' aa '_20_data'])
    eval(['data = freqT_s' aa '_20_data']);
    cfg = [];
    cfg.method = 'mtmfft';
    cfg.taper = 'hanning';
    cfg.output = 'powandcsd'; % powandcsd for only one channel 'powandcr...'
    cfg.foilim = [1 5];
    cfg.channel = {'MEG2022','MEG2023','MEG2032','MEG2033','MEG2312','MEG2313','MEG2322','MEG2323','MEG2512','MEG2513','MEG2532','MEG2533','MEG2342','MEG2343','MEG2332','MEG2333','MEG2542','MEG2543','MEG2112','MEG2113','MEG2122','MEG2123',...
        'MEG2012','MEG2013','MEG2042','MEG2043','MEG1912','MEG1913','MEG1922','MEG1923','MEG1932','MEG1933','MEG1942','MEG1943','MEG1712','MEG1713','MEG1732','MEG1733','MEG1742','MEG1743'};
    freq20 = ft_freqanalysis(cfg, data);
    pow_20Hz_data(n,:)=mean(freq20.powspctrm);
    n = n+1;
end  

% figure
hold on
shadedErrorBar(freq15.freq, mean(pow_15Hz_data), std(pow_15Hz_data), '-r',1);

shadedErrorBar(freq20.freq, mean(pow_20Hz_data), std(pow_20Hz_data), '-b',1);

hold off
set(gca,'ylim',[-1e-23 1e-23]);
% plot(freq20.freq, mean(pow_20Hz_data));










