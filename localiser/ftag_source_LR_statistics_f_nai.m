clear all
close all
grandavg_R15_f = cell(1,18);
subs = [2:6 8:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_R15_f.mat']);
   grandavg_R15_f{a} = sourceAIntNorm;
   a = a+1;
end
cd F:\2017_localiser
save -v7.3 grandavg_R15_f grandavg_R15_f

clear all
close all
grandavg_L20_f = cell(1,18);
subs = [2:6 8:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20_f.mat']);
   grandavg_L20_f{a} = sourceBIntNorm;
   a = a+1;
end
cd F:\2017_localiser
save -v7.3 grandavg_L20_f grandavg_L20_f


% run statistics over subjects %
clc
clear all
close all
load grandavg_R15_f
load grandavg_L20_f

cfg=[];
cfg.dim         = grandavg_R15_f{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.05; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_R15_f);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_R15_f{:}, grandavg_L20_f{:});

% save -v7.3 R15L20_f_source_stat stat


% %% nifti
% cd F:\2017_localiser
% load R15L20_f_source_stat

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'R15L20_f_source_stat_c';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
cfg.datatype = 'float';
ft_volumewrite(cfg, stat);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%

clear all
close all
grandavg_L20 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20.mat']);
   grandavg_L20{a} = sourceDiffIntNorm_L20;
   a = a+1;
end
save -v7.3 grandavg_L20 grandavg_L20

clear all
close all
grandavg_L20 = cell(1,18);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20.mat']);
   grandavg_L20{a} = sourceDiffIntNorm_L20;
   a = a+1;
end
save -v7.3 grandavg_L20 grandavg_L20

%% run statistics over subjects %
clc
clear all
close all
load grandavg_L20
load grandavg_L20

cfg=[];
cfg.dim         = grandavg_L20{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_L20);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_L20{:}, grandavg_L20{:});

save -v7.3 LL20_source_stat_p33 stat


%% nifti
load LL20_source_stat_p33

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'LL20_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);
%%
clc
clear all
close all
cd F:\2017_localiser
grandavg_L20_f20 = cell(1,36);
subs = [2 3 5:20];
a=1;
for k = subs
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    eval(['load F:\2017_localiser\s' aa '_cbr\sourceBIntNorm.mat']);
    grandavg_L20_f20{a} = sourceBIntNorm;
    a = a+1;
    eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20.mat']);
    grandavg_L20_f20{a} = sourceDiffIntNorm_L20;
    a = a+1;
end
save -v7.3 grandavg_L20_f20 grandavg_L20_f20

clear all
close all
grandavg_R15_f20 = cell(1,36);
subs = [2 3 5:20];
a=1;
for k = subs
   aa = sprintf('%02d',k);
   eval(['cd F:\2017_localiser\s' aa '_cbr']);
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceAIntNorm.mat']);
   grandavg_R15_f20{a} = sourceAIntNorm;
   a = a+1;
   eval(['load F:\2017_localiser\s' aa '_cbr\sourceDiffIntNorm_L20.mat']);
   grandavg_R15_f20{a} = sourceDiffIntNorm_L20;
   a = a+1;
end
save -v7.3 grandavg_R15_f20 grandavg_R15_f20

%% run statistics over subjects %
clc
clear all
close all
cd F:\2017_localiser
load grandavg_L20_f20
load grandavg_R15_f20

cfg=[];
cfg.dim         = grandavg_L20_f20{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_L20_f20);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_L20_f20{:}, grandavg_R15_f20{:});

save -v7.3 RR15_f20_source_stat_p33 stat


%% nifti
cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'RR15_f20_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);

%% run statistics over subjects R15_f vs L20 %
clc
clear all
close all
load grandavg_R15_f
load grandavg_L20

cfg=[];
cfg.dim         = grandavg_R15_f{1}.dim; 
cfg.method      = 'montecarlo';
cfg.statistic   = 'ft_statfun_depsamplesT';
cfg.parameter   = 'pow';
cfg.correctm    = 'cluster';
cfg.numrandomization = 1000;
cfg.alpha       = 0.33; % t>1
cfg.tail        = 0;

nsubj=numel(grandavg_R15_f);
cfg.design(1,:) = [1:nsubj 1:nsubj];
cfg.design(2,:) = [ones(1,nsubj) ones(1,nsubj)*2];
cfg.uvar        = 1; % row of design matrix that contains unit variable (in this case: subjects)
cfg.ivar        = 2; % row of design matrix that contains independent variable (the conditions)

stat = ft_sourcestatistics(cfg, grandavg_R15_f{:}, grandavg_L20{:});

save -v7.3 R15_f20_source_stat_p33 stat


%% nifti
load R15_f20_source_stat_p33

cfg= [];
cfg.parameter = 'stat';
cfg.filename = 'R15_f20_source_stat_p33';
cfg.filetype = 'nifti';
cfg.vmpversion = 2;
cfg.coordsys = 'spm';
ft_volumewrite(cfg, stat);