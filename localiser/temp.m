spm('defaults', 'eeg');

S = [];
S.D = 'F:\2017_localiser\s02_cbr\pbefL15.mat';
S.filter.band = 'bandpass';
S.filter.type = 'butterworth';
S.filter.order = 5;
S.filter.dir = 'twopass';
S.filter.PHz = [0.5
                40];
D = spm_eeg_filter(S);


