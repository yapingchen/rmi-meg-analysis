%% Contrast activity to another interval
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculating the cross spectral density matrix
% L15
close all
clear all
subs = [2 3 5:20];
for k = subs
    close all
    clearvars -except k
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    
    eval(['load freqT_s' aa '_L15_data'])
    
    cfg = [];
    cfg.toilim = [-1 0];
    eval(['dataPre = ft_redefinetrial(cfg, freqT_s' aa '_L15_data)']);
    
    cfg.toilim = [0 12];
    eval(['dataPost = ft_redefinetrial(cfg, freqT_s' aa '_L15_data)']);
    
    %
    cfg = [];
    cfg.method    = 'mtmfft';
    cfg.output    = 'powandcsd';
    cfg.tapsmofrq = 1;
    cfg.foilim    = [2.6 2.9];
    freqPre = ft_freqanalysis(cfg, dataPre);
    
    cfg = [];
    cfg.method    = 'mtmfft';
    cfg.output    = 'powandcsd';
    cfg.tapsmofrq = 1;
    cfg.foilim    = [2.6 2.9];
    freqPost = ft_freqanalysis(cfg, dataPost);
    
    load headmodel_meg  
    cfg                 = [];
    cfg.grad            = freqPost.grad;
    cfg.vol             = headmodel_meg;
    cfg.reducerank      = 2;
    cfg.channel         = {'MEG'};
    cfg.grid.resolution = 0.7;   % use a 3-D grid with a 1 cm resolution
    cfg.grid.unit       = 'cm';
    [grid] = ft_prepare_leadfield(cfg);
    
    
    % Source Analysis: Contrast activity to another interval
    
    dataAll = ft_appenddata([], dataPre, dataPost);
    
    cfg = [];
    cfg.method    = 'mtmfft';
    cfg.output    = 'powandcsd';
    cfg.tapsmofrq = 1;
    cfg.foilim    = [2.6 2.9];
    freqAll = ft_freqanalysis(cfg, dataAll);
    
    cfg              = [];
    cfg.method       = 'dics';
    cfg.frequency    = 2.73;
    cfg.grid         = grid;
    cfg.vol          = headmodel_meg;
    cfg.dics.projectnoise = 'yes';
    cfg.dics.lambda       = '5%';
    cfg.dics.keepfilter   = 'yes';
    cfg.dics.realfilter   = 'yes';
    sourceAll = ft_sourceanalysis(cfg, freqAll);
    
    
    cfg.grid.filter = sourceAll.avg.filter;
    sourcePre_con  = ft_sourceanalysis(cfg, freqPre );
    sourcePost_con = ft_sourceanalysis(cfg, freqPost);
    
%     save sourcePre_con sourcePre_con
%     save sourcePost_con sourcePost_con
    
    sourceDiff = sourcePost_con;
    sourceDiff.avg.pow = (sourcePost_con.avg.pow - sourcePre_con.avg.pow) ./ sourcePre_con.avg.pow;  
    
    load mri_segmented
    cfg            = [];
    cfg.downsample = 2;
    cfg.parameter  = 'avg.pow';
    sourceDiffInt  = ft_sourceinterpolate(cfg, sourceDiff, mri_segmented);
    
    cfg = [];
    cfg.nonlinear     = 'no';
    sourceDiffIntNorm_L15 = ft_volumenormalise(cfg, sourceDiffInt);
    
    save sourceDiffIntNorm_L15 sourceDiffIntNorm_L15
end

%%
% R15
close all
clear all
subs = [2 3 5:20];
for k = subs
    close all
    clearvars -except k
    aa = sprintf('%02d',k);
    eval(['cd F:\2017_localiser\s' aa '_cbr']);
    
    eval(['load freqT_s' aa '_R15_data'])
    
    cfg = [];
    cfg.toilim = [-1 0];
    eval(['dataPre = ft_redefinetrial(cfg, freqT_s' aa '_R15_data)']);
    
    cfg.toilim = [0 12];
    eval(['dataPost = ft_redefinetrial(cfg, freqT_s' aa '_R15_data)']);
    
    %
    cfg = [];
    cfg.method    = 'mtmfft';
    cfg.output    = 'powandcsd';
    cfg.tapsmofrq = 1;
    cfg.foilim    = [2.6 2.9];
    freqPre = ft_freqanalysis(cfg, dataPre);
    
    cfg = [];
    cfg.method    = 'mtmfft';
    cfg.output    = 'powandcsd';
    cfg.tapsmofrq = 1;
    cfg.foilim    = [2.6 2.9];
    freqPost = ft_freqanalysis(cfg, dataPost);
    
    load headmodel_meg  
    cfg                 = [];
    cfg.grad            = freqPost.grad;
    cfg.vol             = headmodel_meg;
    cfg.reducerank      = 2;
    cfg.channel         = {'MEG'};
    cfg.grid.resolution = 0.7;   % use a 3-D grid with a 1 cm resolution
    cfg.grid.unit       = 'cm';
    [grid] = ft_prepare_leadfield(cfg);
    
    
    % Source Analysis: Contrast activity to another interval
    
    dataAll = ft_appenddata([], dataPre, dataPost);
    
    cfg = [];
    cfg.method    = 'mtmfft';
    cfg.output    = 'powandcsd';
    cfg.tapsmofrq = 1;
    cfg.foilim    = [2.6 2.9];
    freqAll = ft_freqanalysis(cfg, dataAll);
    
    cfg              = [];
    cfg.method       = 'dics';
    cfg.frequency    = 2.73;
    cfg.grid         = grid;
    cfg.vol          = headmodel_meg;
    cfg.dics.projectnoise = 'yes';
    cfg.dics.lambda       = '5%';
    cfg.dics.keepfilter   = 'yes';
    cfg.dics.realfilter   = 'yes';
    sourceAll = ft_sourceanalysis(cfg, freqAll);
    
    
    cfg.grid.filter = sourceAll.avg.filter;
    sourcePre_con  = ft_sourceanalysis(cfg, freqPre );
    sourcePost_con = ft_sourceanalysis(cfg, freqPost);
    
%     save sourcePre_con sourcePre_con
%     save sourcePost_con sourcePost_con
    
    sourceDiff = sourcePost_con;
    sourceDiff.avg.pow = (sourcePost_con.avg.pow - sourcePre_con.avg.pow) ./ sourcePre_con.avg.pow;  
    
    load mri_segmented
    cfg            = [];
    cfg.downsample = 2;
    cfg.parameter  = 'avg.pow';
    sourceDiffInt  = ft_sourceinterpolate(cfg, sourceDiff, mri_segmented);
    
    cfg = [];
    cfg.nonlinear     = 'no';
    sourceDiffIntNorm_R15 = ft_volumenormalise(cfg, sourceDiffInt);
    
    save sourceDiffIntNorm_R15 sourceDiffIntNorm_R15
end